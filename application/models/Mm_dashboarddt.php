<?php
class Mm_dashboarddt extends CI_Model {
    
    
    private $table;
    private $table02;
    private $table03;

    public function __construct() {
        parent::__construct();
        $this->table = "sales"; 

    }  
    function data_sales($periode){
        $query = "SELECT tahun,c.Nmcust,b.nmbarang,v.keterangan,v.varian,z.size,d.nmchassis,h.chassis,s.periode,SUM(s.qty)qty,SUM(s.UTotal)total FROM sales s
                    INNER JOIN customers c ON c.idCust=s.idCust
                    INNER JOIN barang b ON s.kdBarang=b.kdBarang
                    INNER JOIN varian v ON v.id=s.varian_id
                    INNER JOIN size z ON z.id=s.size_id
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    GROUP BY s.varian_id,tahun" ;
        return $this->db->query($query);
    }     
    // function data_salesqty(){
    //     $query = "SELECT  IFNULL(SUBSTRING_INDEX(tgl_sales, '-', 1) , 'TOTAL') AS periodee,
    //                 SUM( IF( kat_size = 1, qty, 0) ) AS Big,
    //                 SUM( IF( kat_size = 2, qty, 0) ) AS Small,
    //                 SUM( IF( kat_size = 3, qty, 0) ) AS Medium, 
    //                 SUM( qty ) AS total 
    //             FROM sales
    //             GROUP BY periodee" ;
    //     return $this->db->query($query);
    // }  
        function data_salesqtyperiode(){
        $year=date("Y");
        $query = "SELECT m.tahun periodee,IFNULL(SUM(t.qty), 0) qty FROM m_tahun m
            LEFT JOIN sales t ON t.tahun=m.tahun AND kategori_id='dt'
                    LEFT JOIN varian v ON v.id=t.varian_id AND  v.id >= 10 
                    WHERE m.tahun <= $year
                    GROUP BY m.tahun 
                    ORDER BY m.tahun ASC" ;
        return $this->db->query($query);
    }  
     function data_salesqtyperiodetotal(){
        $query = "SELECT t.tahun periodee,sum(t.qty) qty FROM sales t   
                    group by t.tahun 
                    ORDER BY t.tahun ASC" ;
        return $this->db->query($query);
    }  
        function data_salesqty(){
        $year=date("Y");
          $query = "SELECT m.tahun periodee,c.keterangan katsize,IFNULL(SUM(s.qty), 0) qty ,IFNULL(SUM(s.Utotal), 0)qtyamount 
                    FROM categorysize c  
                     LEFT JOIN m_tahunkat m ON m.kat_size=c.id 
                     LEFT JOIN sales s ON  m.tahun=s.tahun AND m.kat_size=s.kat_size AND s.kategori_id='dt'
                     WHERE m.tahun <=$year
                     GROUP BY m.tahun,c.id
                     ORDER BY m.tahun,c.seqno ASC" ;
        return $this->db->query($query);
    }  
    function data_salesqtytipe(){
        $year=date("Y");
        $query = "SELECT m.tahun periodee,c.tipe katsize,IFNULL(SUM(s.qty), 0) qty  
                    FROM tipe c  
                     LEFT JOIN m_tahuntipe m ON m.id_tipe=c.id
                     LEFT JOIN sales s ON s.tahun=m.tahun AND m.id_tipe=s.id_tipe AND s.kategori_id='dt'
                     WHERE m.tahun <=$year
                     GROUP BY m.tahun,c.id
                     ORDER BY m.tahun,c.id ASC" ;
        return $this->db->query($query);
    }   
    function data_salesqtyVarian($idvarian){
        $year=date("Y");
        $query = "   SELECT m.tahun periodee,c.keterangan katsize,IFNULL(SUM(s.qty), 0) qty ,IFNULL(SUM(s.Utotal), 0)qtyamount 
                    FROM categorysize c  
                     LEFT JOIN m_tahunkat m ON m.kat_size=c.id 
                     LEFT JOIN sales s ON  m.tahun=s.tahun AND m.kat_size=s.kat_size AND s.varian_id=$idvarian
                     LEFT JOIN varian v ON v.id=s.varian_id 
                     WHERE m.tahun <=$year 
                     GROUP BY m.tahun,c.id
                     ORDER BY m.tahun,c.seqno ASC " ;
        return $this->db->query($query);
    }  
 
    function data_chassisdetail(){
        $query = "SELECT c.chassis,s.tahun,SUM(s.qty)qty FROM chassis c
                        LEFT JOIN chassis_detail d ON d.idchassis=c.id
                        LEFT JOIN sales s ON s.chassis_id=d.id
                        GROUP BY c.chassis,s.tahun  ORDER BY s.tahun,c.chassis ASC" ;
                            return $this->db->query($query);
    }   

    function checkData(){
        $query = "SELECT * FROM tempdatachassis_dt" ;
        return $this->db->query($query)->num_rows();  
      
    }
    function checkData2(){
        $query = "SELECT * FROM transaksi_chassis_dt" ;
        return $this->db->query($query)->num_rows();  
      
    }
    function droptemp(){  
        $query = "DELETE from tempdatachassis_dt" ;  
            return $this->db->query($query);
    } 


    function droptempcalc(){ 
       $query = "DELETE from transaksi_chassis_dt" ;  
            return $this->db->query($query);
    } 


    function calculateyear($i){ 
  
      $query=" INSERT INTO transaksi_chassis_dt (tahun, idchassis,qty,totalV,STATUS) 
               SELECT $i,c.id,IFNULL(SUM(s.qty), 0),IFNULL(SUM(s.Utotal), 0),0 FROM chassis c 
              INNER JOIN chassis_detail d ON c.id=d.idchassis 
              LEFT JOIN sales s ON d.id=s.chassis_id AND s.tahun=$i AND s.kategori_id='dt'
               GROUP BY c.chassis";   
               return $this->db->query($query);
    }
    function updatecalculateyear($i){   
      $query=" UPDATE  transaksi_chassis_dt a
                INNER JOIN
                (SELECT   ROW_NUMBER() OVER (
                        ORDER BY qty DESC
                    ) seq ,tahun,qty,totalV,idchassis
                                    FROM    transaksi_chassis_dt 
                                    WHERE tahun=$i
                                    ORDER BY qty DESC LIMIT 5
               ) b ON  b.tahun = a.tahun AND b.qty=a.qty AND b.totalV=a.totalV AND a.idchassis=b.idchassis
        SET     a.status = (SELECT MAX(STATUS) FROM transaksi_chassis_dt)+ b.seq 
        LIMIT 5";   
        return $this->db->query($query);
    }
    function data_chassisyear($tahun,$limit){ 
        $query = "INSERT INTO tempdatachassis_dt(tahun,idchassis,qty,totalV,STATUS) SELECT tahun,idchassis,qty,totalV,status from transaksi_chassis_dt where tahun=$tahun order by status DESC limit $limit" ;  
                            return $this->db->query($query);
    } 

    function data_chassisyearblank($tahun,$blank){  
        $query = "INSERT into tempdatachassis_dt(tahun,idchassis,qty,totalV,STATUS) SELECT tahun,idchassis,0,0,0  from transaksi_chassis_dt where tahun=$tahun order by status ASC limit $blank" ;  
                            return $this->db->query($query);
    } 
    function data_saleschassisup(){
        $query = " SELECT c.status,d.chassis,c.tahun,c.qty,c.totalV FROM tempdatachassis_dt c
                    INNER JOIN chassis d ON d.id=c.idchassis 
                   GROUP BY d.chassis,c.tahun
                    ORDER BY c.tahun,c.qty ASC" ;
                            return $this->db->query($query);
    } 

    function data_saleschassis(){
        $query = " SELECT d.seqno,c.status,d.chassis,c.tahun,c.qty,c.totalV FROM tempdatachassis_dt c
                    INNER JOIN chassis d ON d.id=c.idchassis 
                    WHERE d.seqno >0
                   GROUP BY d.chassis,c.tahun
                    ORDER BY c.tahun,d.seqno,c.qty ASC" ;
                            return $this->db->query($query);
    } 
    // function data_saleschassis(){
    //     $query = "SELECT t.tahun,SUM(s.qty) qty FROM m_tahun t
    //                 LEFT JOIN sales s ON t.tahun=s.tahun 
    //                 LEFT JOIN chassis_detail d ON s.chassis_id=d.id
    //                 LEFT JOIN chassis c ON d.idchassis=c.id 
    //                 GROUP BY c.chassis,t.tahun 
    //                 ORDER BY t.tahun,c.chassis ASC" ;
    //                         return $this->db->query($query);
    // }
    function data_salesqtyVarianfull(){
        $query = " SELECT t.tahun, CASE varian_id 
                     WHEN 1 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ1,
                     CASE varian_id 
                     WHEN 2 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ2,
                     CASE varian_id 
                     WHEN 3 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ3,
                     CASE varian_id 
                     WHEN 4 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ4,
                     CASE varian_id 
                     WHEN 5 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ5,
                     CASE varian_id 
                     WHEN 7 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ7,
                     CASE varian_id 
                     WHEN 8 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ8,
                     CASE varian_id 
                     WHEN 9 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQA,
                     CASE varian_id 
                     WHEN 10 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalCT,
                     CASE varian_id 
                     WHEN 11 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalwb FROM m_tahun t
                    LEFT OUTER JOIN sales s ON s.tahun=t.tahun
                    GROUP BY t.tahun,varian_id
                     ORDER BY tahun ASC" ;
        return $this->db->query($query);
    }  
      // function data_salesqty(){
      //   $query = "SELECT s.kat_size as katsize,COALESCE(c.keterangan, '0') ket,SUM(s.qty) qty,s.tahun periodee FROM categorysize c
      //               LEFT JOIN sales s ON c.id=s.kat_size
      //               GROUP BY size
      //               ORDER BY kat_size,tahun ASC" ;
      //     return $this->db->query($query);
      //   }
        function data_salescategory(){
        $query = "SELECT id,keterangan FROM categorysize ORDER BY seqno  ASC" ;
          return $this->db->query($query);
        }  
    function getendingyear() {
        $query = " SELECT tahun FROM m_tahun ORDER BY tahun ASC LIMIT 1";
        return $this->db->query($query);   
    }

    function getcountchassis() {
        $query = " SELECT count(chassis) chassis FROM chassis";
        return $this->db->query($query);   
    }

    function chassis(){
        $query = "SELECT h.chassis,SUM(s.qty) qty,SUM(s.Utotal) amount FROM sales s
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    GROUP BY h.chassis" ;
                            return $this->db->query($query);
    } 

    function chassisdetail(){
        $query = "SELECT h.chassis,d.nmchassis,SUM(s.qty) qty,SUM(s.Utotal) amount FROM sales s
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    GROUP BY d.nmchassis" ;
                            return $this->db->query($query);
    } 
    function getdatavarian($datavarian) {
        $query = " SELECT id FROM varian where varian like '%$datavarian%'";
        return $this->db->query($query);   
    }
}
