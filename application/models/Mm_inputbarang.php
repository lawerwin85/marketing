<?php

/* membuat class dengan nama Magama_model*/
class Mm_inputbarang extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "barang"; 

    }

     /* mendapatkan semua data dan hasilnya sebuah array */
    public function getAll() {
        return $this->db->get($this->table)->result_array();
    }
 
    function getGridData() {
        $query = "SELECT c.id,c.nmBarang,d.nmChassis,d.id as idchassis,h.chassis  FROM barang c
                    LEFT OUTER JOIN chassis_detail d ON d.id=c.chassis_d
                    LEFT OUTER JOIN chassis h on h.id=d.idchassis
                    where c.status=0 order by c.crtDt";
        return $this->db->query($query);      
    }
    function getGridDatadetail($id) {
        $query = "SELECT d.id,d.nmChassis FROM chassis c
                    INNER JOIN chassis_detail d ON d.idchassis=c.id
                    where d.idchassis='$id'";
        return $this->db->query($query);      
    }

    function getby_id($id) {
        $query = "SELECT s.*,h.id as chassis_h,d.*,k.* FROM barang s 
                    INNER JOIN varian v ON v.id=s.varian_id
                    INNER JOIN size z ON z.id=s.size_id
                    INNER JOIN kategori  k ON k.id=s.kategori_id
                    INNER JOIN chassis_detail d ON d.id=s.chassis_d
                    INNER JOIN chassis h ON h.id=d.idchassis
                    LEFT JOIN categorysize g ON g.id=s.kat_size
                    LEFT JOIN tipe t ON t.id=s.id_tipe
                    where s.id='$id'";
        return $this->db->query($query);   
    }
    function insert($record) {
        $this->db->insert($this->table, $record);
    } 
    function update($id, $record) {
        $this->db->where("id", $id);
        $this->db->update($this->table, $record);
    }  
    function delete($id) {
        $query = "UPDATE barang set status=2 where id='$id'";
        return $this->db->query($query);  
    }

}
