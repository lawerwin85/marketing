<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Inputbarang extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_Inputbarang', 'table01'); 
        $this->load->model('Mm_Inputsales', 'table02'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('Inputbarang/grid'),
            "url_add" => site_url('Inputbarang/addBarang'),
            "url_edit" => site_url('Inputbarang/edit'),
            "url_delete" => site_url('Inputbarang/remove'),
        );
        $this->load->view('vm_inputbarang/home', $data); 
        // $this->load->view('vm_inputbarang/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        echo json_encode(array(
            "data" => $this->table01->getGridData()->result()
        ));
    }  
    function addBarang() {
        $data['title'] = 'Add - Input Barang';   
        $data['chassisdetail'] = '';    
        $data['nmchassisdetail'] = '';        
        $data['namaUnit'] ='';     
        $data['readset']='';   
        $data['disabled']=''; 
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis']; 
            $e++;
        }  
        $data['url_post'] = site_url('Inputbarang/addpost');   
        $data['url_gridchassis'] = site_url('Inputsales/gridchassis');  
        $data['url_getchassis'] = site_url('Inputsales/getchassis'); 
        $data['url_index'] = site_url('Inputbarang'); 
        $data['id'] = 0; 

        $this->load->view('vm_inputbarang/form', $data); 
    }    
    public function addpost() { 
            $namaUnit = $this->input->post('namaUnit');  
            $chassisdetail = $this->input->post('chassisdetail'); 
            $kdBarang=date("YmdHs"); 
            $record = array(
                "chassis_d" => $chassisdetail,
                "nmBarang" => $namaUnit, 
                "kdBarang" => $kdBarang, 
                "crtBy" => 'ep',
                "updBy" => 'ep' 
            );
  
        $this->table01->insert($record); 
         $valid = 'true';
         $message = 'data insert';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi edit ini akan mensetting nilai-nilai di form ketika mengklik tombol edit */

    function edit($id) {
        $row = $this->table01->getby_id($id)->row();  
        $data['nmchassisdetail'] = $row->nmChassis;    
        $data['chassisdetail'] =$row->idchassis;       
        $data['namaUnit'] =$row->nmBarang;     
        $idchassis=$row->idheader;
        $data['readset']="readonly='readonly'";
        $data['disabled']='disabled'; 
        // var_dump($idchassis);
        // die();
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis'];
            if ($idchassis == $rowchassis['id']) {
                $data['default']['chassis'][$e]['DISABLED'] = "DISABLED";
            } 
            $e++;
        } 
        $data['url_post'] = site_url('Inputbarang/editpost'); 
        $data['url_gridchassis'] = site_url('Inputsales/gridchassis'); 
        $data['url_getchassis'] = site_url('Inputsales/getchassis'); 
        $data['url_index'] = site_url('Inputbarang'); 
        $data['id'] = $id; 
        $this->load->view('vm_inputbarang/form', $data); 
    }

    public function gridchassis() {
         $id = $this->input->post('id'); 
        echo json_encode(array(
            "data" => $this->table02->getGridDatachassis($id)->result()
        ));
    } 

    function editpost() {
        $id = $this->input->post('id');
        $namaUnit = $this->input->post('namaUnit');  
        $chassisdetail = $this->input->post('chassisdetail'); 
            // var_dump($periode);
            // die();
            $record = array(
                "chassis_d" => $chassisdetail,
                "nmBarang" => $namaUnit, 
                "crtBy" => 'ep',
                "updBy" => 'ep' 
            );
        $this->table01->update($id,$record);   
         $valid = 'true';
         $message = 'data update';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi untuk delete data */

    public function remove() {
        $id = $this->input->post('id');
        $this->table01->delete($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

}
