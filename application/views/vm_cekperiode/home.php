 <div class="container"> 
        <div class="card">
            <div class="header">
                <h2>List Periode</h2>
            </div>
            <div class="body">
                <div class="list-group">
                   <div class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Master Tahun</h5>
                        <big><button <?php echo $buttonm_tahun; ?>="<?php echo $buttonm_tahun; ?>" type="button" onClick="ParamFunc.mtahun(<?php echo $thisyear; ?>)" id="m_tahun" class="btn btn-primary">Update</button></big>
                        </div>
                        <p class="mb-1"><?php echo $m_tahun; ?></p>
                    </div>
                   <div   class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Master Kategori per Tahun</h5>
                       <big><button <?php echo $buttonm_tahunkat; ?>="<?php echo $buttonm_tahunkat; ?>" type="button" onClick="ParamFunc.mtahunkat(<?php echo $thisyear; ?>)" id="m_tahun" class="btn btn-primary">Update</button></big>
                        </div>
                        <p class="mb-1"><?php echo $m_tahunkat; ?></p>
                    </div>
                   <div   class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Master Tipe per Tahun</h5>
                        <big><button <?php echo $buttonm_tahuntipe; ?>="<?php echo $buttonm_tahuntipe; ?>" type="button" onClick="ParamFunc.mtahuntipe(<?php echo $thisyear; ?>)" id="m_tahun" class="btn btn-primary">Update</button></big>
                        </div>
                        <p class="mb-1"><?php echo $m_tahuntipe; ?></p>
                    </div>
                   <div   class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Periode Transaksi Sales  </h5>
                        <big><button <?php echo $button_transQty; ?>="<?php echo $button_transQty; ?>" type="button" onClick="ParamFunc.transaksi_salesqty(<?php echo $thisyear; ?>)" id="m_tahun" class="btn btn-primary">Update</button></big>
                        </div>
                        <p class="mb-1"><?php echo $transQty; ?></p>
                    </div> 
                   <div  class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Periode Transaksi Sales Tipe</h5>
                        <big><button <?php echo $button_transtipe; ?>="<?php echo $button_transtipe; ?>" type="button" onClick="ParamFunc.transaksi_tipe(<?php echo $thisyear; ?>)" id="m_tahun" class="btn btn-primary">Update</button></big>
                        </div>
                        <p class="mb-1"><?php echo $transtipe; ?></p>
                    </div>
                </div>
            </div>
        </div> 
</div>
<script type="text/javascript">
        var ParamFunc = { 
        mtahun: function (id) {
            
              var jawab = confirm("Apakah anda yakin untuk Update ini !");
                if (jawab === true) {
        //            kita set hapus false untuk mencegah duplicate request
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        $.post('<?php echo $url_mtahun ?>', {id: id},
                        function (data) {
                           location.reload(true);
                        }); 
                    }
                } else {
                    return false;
                }
         },
         mtahunkat: function (id) {
            
              var jawab = confirm("Apakah anda yakin untuk Update ini !");
                if (jawab === true) {
        //            kita set hapus false untuk mencegah duplicate request
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        $.post('<?php echo $url_mtahunkat ?>', {id: id},
                        function (data) {
                           location.reload(true);
                        }); 
                    }
                } else {
                    return false;
                }
         },
         mtahuntipe: function (id) {
            
              var jawab = confirm("Apakah anda yakin untuk Update ini !");
                if (jawab === true) {
        //            kita set hapus false untuk mencegah duplicate request
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        $.post('<?php echo $url_mtahuntipe ?>', {id: id},
                        function (data) {
                           location.reload(true);
                        }); 
                    }
                } else {
                    return false;
                }
         },
         transaksi_salesqty: function (id) {
            
              var jawab = confirm("Apakah anda yakin untuk Update ini !");
                if (jawab === true) {
        //            kita set hapus false untuk mencegah duplicate request
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        $.post('<?php echo $url_transaksi_sales ?>', {id: id},
                        function (data) {
                           location.reload(true);
                        }); 
                    }
                } else {
                    return false;
                }
         },
         transaksi_tipe: function (id) {
            
              var jawab = confirm("Apakah anda yakin untuk Update ini !");
                if (jawab === true) {
        //            kita set hapus false untuk mencegah duplicate request
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        $.post('<?php echo $url_transaksi_tipe ?>', {id: id},
                        function (data) {
                           location.reload(true);
                        }); 
                    }
                } else {
                    return false;
                }
         }
    }
</script>