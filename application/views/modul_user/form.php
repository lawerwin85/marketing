<div class="col-md-12">
    <div class="panel panel-white" data-sortable-id="form-stuff-2">
        <div class="panel-heading">

            <h4 class="panel-title">Form <?php echo $title ?></h4>
        </div>
        <div class="panel-body">


            <form id="demo-form2" data-parsley-validate class="form-horizontal">
                <div class="col-sm-6">

                    <input type="hidden" id="vbr" value="<?php echo $id; ?>" />

                    <div class="form-group">
                        <label class="control-label col-md-3" for="g_name">Group name</label>
                        <div class="col-md-9">
                            <input type="text" id="g_name"  name="g_name" class="form-control" placeholder="Group name"
                                   value="<?php echo (isset($default['g_name'])) ? $default['g_name'] : ''; ?>"/>
                            <span id="err_g_name"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" for="g_desc">Group desc.</label>
                        <div class="col-md-9">
                            <input type="text" id="g_desc"  name="g_desc" class="form-control" placeholder="Group description"
                                   value="<?php echo (isset($default['g_desc'])) ? $default['g_desc'] : ''; ?>"/>
                            <span id="err_g_desc"></span>
                        </div>
                    </div>

                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="reset" id="cancel" class="btn btn-warning">Cancel</button>
                        <button type="submit" id ="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#tanggal").datepicker({miniViewMode:1,format:'yyyy-mm-dd'});
        var buttonsave, buttoncancel, urlpost, urlindex, content;


        buttonsave = $('#submit');
        buttoncancel = $('#cancel');
        urlpost = '<?php echo $url_post; ?>';
        urlindex = '<?php echo $url_index; ?>';
        content = $("#contentdata");


        buttonsave.click(
            function ()
            {

                $.ajax(
                    {
                        type: "POST",
                        url: urlpost,
                        dataType: "json",
                        data: {
                            id: $("#vbr").val(),
                            g_name: $("#g_name").val(),
                            g_desc: $("#g_desc").val(),
                        },
                        cache: false,
                        success:
                            function (data, text)
                            {
                                if (data.hasil == 'true') {
                                    content.fadeOut("slow", "linear");
                                    content.load(urlindex);
                                    content.fadeIn("slow");
                                } else {
                                    $("#err_g_name").html(data.err_g_name).fadeIn('slow');
                                    $("#err_g_desc").html(data.err_g_desc).fadeIn('slow');
                                }
                            },
                        error: function (request, status, error) {
                            alert(request.responseText + " " + status + " " + error);
                        }
                    });
                return false;

            });

        buttoncancel.click(
            function ()
            {
                content.fadeOut("slow", "linear");
                content.load(urlindex);
                content.fadeIn("slow");

            });




    });

</script>