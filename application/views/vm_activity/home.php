 <div class="container"> 
         <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>List Log Activity<small>
                                <select id="user" class="form-control select2" name="user">
                                    <option value="admin">--Pilih User--</option>
                                       <?php print_r($default['user']); foreach ($default['user'] as $row) { ?>
                                                
                                                <option data-id="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                                        <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                                    <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                            <?php } ?>
                                    </select> </small>
                                </h2> <br> 

                         <button onclick="loadTable();"  type="button" id ="submit" class="btn btn-success">Cari</button>  
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table  id="datatable-ajax" class="table table-hover js-basic-example dataTable table-custom">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th style="width: 50px">No</th>
                                            <th style="width: 250px">UserName</th> 
                                            <th style="width: 250px">User Group</th>
                                            <th style="width: 250px">Tipe</th>
                                            <th style="width: 250px">Keterangan</th>
                                            <th style="width: 300px">Last Login</th> 
                                        </tr>
                                    </thead> 
                                    <tbody>
                                         
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
        </div>
  <script>
   var logactivty = {
        table: undefined
    };
    $(document).ready(function () {
        /* start datatable */
        var handleDataTableDefault = function () {
            if ($("#datatable-ajax").length!==0) {
        var nameUser=$("#user").val();
               logactivty.table = $("#datatable-ajax").DataTable({
                    "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
                        var oSettings = this.fnSettings();
                        var iTotalRecords = oSettings.fnRecordsDisplay();
                        $("#total").val(iTotalRecords);
                        $("td:first", nRow).html(iDisplayIndex +1);
                       return nRow;
                    },
                    "ajax": {
                        "url": '<?php echo $url_grid; ?>',
                        "type": 'POST',
                        "data" : {
                            "user" :nameUser 
                        },
                    },
                    "columns": [
                        {"data": "log_tipe"},
                        {"data": "log_user"},
                        {"data": "log_group"},  
                        {"data": "tipe"},  
                        {"data": "log_desc"},
                        {"data": "log_time"} 
                    ]
                });
            }
        };
        TableManageDefault = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableDefault();
                }
            };
        }();
        TableManageDefault.init();
    });
    /* end datatable */
    /* end datatable */
   function loadTable() { 
   var nameUser = $('#user').val(); 
    if (nameUser==''){
        alert("User harap diisi");
    }
    else{
        $('#datatable-ajax').dataTable().fnDestroy();
        if ( $.fn.dataTable.isDataTable( '#datatable-ajax' ) ) {
            $('#datatable-ajax').DataTable().ajax.reload();
               
        }

        else {
            TableManageDefault.init();
        }
    }
    


    }


   
</script>