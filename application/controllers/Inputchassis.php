<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Inputchassis extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_inputchassis', 'table01'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('Inputchassis/grid'),
            "url_add" => site_url('Inputchassis/addChassis'),
            "url_edit" => site_url('Inputchassis/editdetail'),
            "url_delete" => site_url('Inputchassis/remove'),
        );
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='View Chassis Table'; 
        $log_trans='ChassisTable'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("view", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->load->view('vm_inputchassis/home', $data); 
        // $this->load->view('vm_inputchassis/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        echo json_encode(array(
            "data" => $this->table01->getGridData()->result()
        ));
    }
    function editdetail($id) {
  
        $data = array(
            "base" => base_url(), 
            "url_griddetail" => site_url('Inputchassis/griddetail'),
            "url_add" => site_url('Inputchassis/addChassisdetil'),
            "url_delete" => site_url('Inputchassis/remove'),
        );
        $data['id'] = $id; 
        $this->load->view('vm_inputchassis/homedetail', $data); 
    }
    public function griddetail() {
         $id = $this->input->post('id'); 
        echo json_encode(array(
            "data" => $this->table01->getGridDatadetail($id)->result()
        ));
    }
    function addChassis() {
        $data['title'] = 'Add - Input Chassis';   
        $data['chassisdetail'] = '';   
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis']; 
            $e++;
        }  
        $data['url_post'] = site_url('Inputchassis/addpost');  
        $data['url_getchassis'] = site_url('Inputchassis/getchassis'); 
        $data['url_index'] = site_url('Inputchassis'); 
        $data['id'] = 0; 

        $this->load->view('vm_inputchassis/form', $data); 
    }
    function addChassisdetil($id) {
        $data['title'] = 'Add - Input Chassis';   
        $data['chassisdetail'] = '';   
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis'];
            if ($id == $rowchassis['id']) {
                $data['default']['chassis'][$e]['selected'] = "SELECTED";
            } 
            $e++;
        }  
        $data['url_post'] = site_url('Inputchassis/addpost');  
        $data['url_getchassis'] = site_url('Inputchassis/getchassis'); 
        $data['url_index'] = site_url('Inputchassis'); 
        $data['id'] = 0; 

        $this->load->view('vm_inputchassis/form', $data); 
    }    
    public function addpost() { 
        $user = $this->session->userdata('ses_userName'); 
            $chassisdetail = $this->input->post('chassisdetail');  
            $chassis = $this->input->post('chassis'); 
            // var_dump($periode);
            // die();
            $record = array(
                "nmChassis" => $chassisdetail,
                "idchassis" => $chassis, 
                "crtBy" => $user,
                "updBy" => $user 
            );
  
        $this->table01->insert($record); 
         $valid = 'true';
         $message = 'data insert';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Add Chassis Table'; 
        $log_trans='Add'.$chassisdetail; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("add", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi edit ini akan mensetting nilai-nilai di form ketika mengklik tombol edit */

    function edit($id) {
        $row = $this->table01->getby_id($id)->row();   
        $data['chassisdetail'] =$row->nmChassis;      
        $idchassis=$row->idchassis;     
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis'];
            if ($idchassis == $rowchassis['id']) {
                $data['default']['chassis'][$e]['selected'] = "SELECTED";
            } 
            $e++;
        } 
        $data['url_post'] = site_url('Inputchassis/editpost');  
        $data['url_index'] = site_url('Inputchassis'); 
        $data['id'] = $id; 
        $this->load->view('vm_inputchassis/form', $data); 
    }

    /* fungsi untuk post data ketika melakukan edit data, fungsi ini akan masuk ke database */

    function editpost() {
        $user = $this->session->userdata('ses_userName'); 
        $id = $this->input->post('id');
        $chassisdetail = $this->input->post('chassisdetail');
        $chassis = $this->input->post('chassis'); 
            // var_dump($periode);
            // die();
        $datetime=date("Y-m-d h:i:s");
            $record = array(
                "nmChassis" => $chassisdetail,
                "idchassis" => $chassis, 
                "UpdDt" => $datetime,
                "updBy" => $user  
            );
        $this->table01->update($id,$record);   
         $valid = 'true';
         $message = 'data update';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Edit Chassis Table'; 
        $log_trans='Edit'.$chassisdetail; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("edit", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi untuk delete data */

    public function remove() {
        $id = $this->input->post('id');
        $row = $this->table01->getby_id($id)->row();   
        $chassisdetail =$row->nmChassis;   
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Hapus Chassis Table'; 
        $log_trans='Hapus'.$chassisdetail; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("hapus", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->table01->delete($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

}
