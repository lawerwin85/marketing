<?php

/* membuat class dengan nama Magama_model*/
class Mm_array extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table
     LEFT JOIN $this->table02 b ON b.id = a.id
                 LEFT JOIN $this->table03 c ON c.id = a.id */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "customers"; 
        $this->table1 = "barang"; 
        $this->table2 = "varian"; 
        $this->table3 = "size"; 
        $this->table4 = "chassis"; 
        $this->table6 = "chassis_detail"; 
        $this->table5 = "kategori"; 
        $this->table7 = "categorysize"; 
        $this->table8 = "tipe"; 
        $this->table9 = "m_tahun"; 
        $this->tableuser = "users"; 
    }
      
    public function data_tahun() {
        $query = "SELECT * from m_tahun order by tahun ASC";
        return $this->db->query($query)->result_array();     
    }
    public function data_group() {
        $query = "SELECT * from user_group ";
        return $this->db->query($query)->result_array();     
    }
    public function data_cust(){
        return $this->db->get($this->table)->result_array();
    }
     public function data_user(){
        return $this->db->get($this->tableuser)->result_array();
    }
     public function data_tipe(){
        return $this->db->get($this->table8)->result_array();
    }
    public function data_barang(){
        return $this->db->get($this->table1)->result_array();
    }
    public function data_varian(){
        return $this->db->get($this->table2)->result_array();
    }
    public function data_size(){
        return $this->db->get($this->table3)->result_array();
    }
    public function data_chassis(){
        return $this->db->get($this->table4)->result_array();
    }
    public function data_chassisdtl(){
        return $this->db->get($this->table6)->result_array();
    }
    // public function data_kategori(){
    //     return $this->db->get($this->table5)->result_array();
    // }
    public function data_kategori() {
        $query = "SELECT * from kategori order by seqno ASC";
        return $this->db->query($query)->result_array();     
    }
    public function data_kategorisize(){
        return $this->db->get($this->table7)->result_array();
    }

    function getGridDataQr($varian) {
        $query = "SELECT * from varian
                    where id='$varian'";
        return $this->db->query($query);      
    }
    function getGridDataSize($size) {
        $query = "SELECT * from size where id='$size'";
        return $this->db->query($query);      
    } 
     
}
