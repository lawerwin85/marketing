<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Gantipass extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_Gantipass', 'table01'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),   
            "url_index" => site_url('Gantipass'),
            "url_post" => site_url('Gantipass/addpost')  
        );
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Update Password'; 
        $log_trans='UpdatePass'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("edit", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->load->view('vm_gantipass/home', $data); 
        // $this->load->view('vm_Gantipass/confirm_delete', $data);
    }
   
 
    public function addpost() { 

            $userId = $this->session->userdata('ses_user_id'); 
            $user = $this->session->userdata('ses_userName'); 
            $passwordlama = $this->input->post('passwordlama');  
            $passwordbaru = $this->input->post('passwordbaru');  
            $passcheck = md5($user . $passwordlama);
            $passbaru = md5($user . $passwordbaru);
            $checkdata = $this->table01->cek_password($user,$passcheck);
            $datetime=date("Y-m-d h:i:s");
            // var_dump($checkdata);
            // var_dump($passbaru);
            // die();
            if($checkdata > 0){
                $valid=true;
                $message='Password Berhasil Dirubah';
                $record = array( 
                    "userPassword" => $passbaru,  
                    "UpdateTime" => $datetime,
                    "UpdateBy" => $userId
                );
                $this->table01->updatepass($user,$record);
            }
            else{
                $valid=false;
                $message='Password Tidak sesuai';  
            } 
  
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );   
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }
 
}
