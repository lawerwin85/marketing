var urlchassiskat = urlpost+'Dash/gridsaleschassiskategori';
$(function () {
      function graphbar(callback){ 
              
                $.ajax({
                    url: urlchassiskat, 
                    dataType: 'json', 
                    type:'POST',  
                    data: {
                        periode: '2019', 
                    },
                    success: function(data){    
                        var chassis=data.chasis;  
                        var chassisdetail=data.chassisdetail;  
                        callback({dataheader:chassis,datadetail:chassisdetail}); 
                    } 
                }); 
        } 
        graphbar(function(res){
            Array.prototype.contains = function(v) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === v) return true;
            }
            return false;
        };
        Array.prototype.unique = function() {
            var arr = [];
            for(var i = 0; i < this.length; i++) {
                if(!arr.includes(this[i])) {
                    arr.push(this[i]);
                }
            }
            return arr; 
        }
          var list=res.dataheader; 
            var listdetail=res.datadetail;   
            var groups = {};   
            var periodebar=[];
            var groupsdetail = {};
            for (var i = 0; i < list.length; i++) {   
                        var listnilai=list[i].qty;
                        var y =Number(listnilai);
                        var groupName2 = list[i].chassis; 
                        var drilldown= list[i].chassis; 
                        if (!groups[groupName2]) {
                            groups[groupName2] = [];
                          }   

                        
                          groups[groupName2].push({y:y,name:groupName2,colorByPoint: true,drilldown:drilldown});  
            }
                        myArray = [];
                        for (var groupName2 in groups) { 
                          myArray.push({name: groupName2, data: groups[groupName2]});
                             
                         }
            for (var i = 0; i < listdetail.length; i++) {   
                        var listnilai=listdetail[i].qty;
                        var y =Number(listnilai);
                        var groupNamedetail = listdetail[i].nmchassis; 
                        var name= listdetail[i].chassis; 
                        var id= listdetail[i].chassis; 
                        if (!groupsdetail[name]) {
                            groupsdetail[name] = [];
                          }      

                        
                          groupsdetail[name].push([groupNamedetail,y]);  
            } 
                        myArray2 = [];
                        for (var name in groupsdetail) { 
                          myArray2.push({id: name,name: name, data: groupsdetail[name]});
                            //console.log(groupsdetail[groupNamedetail]);
                        }  
                         //console.log(myArray);
                        //  console.log(myArray2);
                        // myArray.push({
                        //     type: 'spline',
                        //     showInLegend: false,  
                        //     data: groups[groupName2],
                        //     name: curvetrend,
                        //     marker: {
                        //         lineWidth: 2,
                        //         lineColor: Highcharts.getOptions().colors[3],
                        //         fillColor: 'white'
                        //     }
                        // }); 
    
    var defaultTitle = "Dashboard by Chassis Kategori";
    var drilldownTitle = "Dashboard by Chassis Kategori ";
    
    // Create the chart
    var chart = new Highcharts.Chart({
        chart: {
            type: 'column',
            renderTo: 'containerclickkategori',
            events: {
                drilldown: function(e) {
                    chart.setTitle({ text: drilldownTitle + e.point.name });
                },
                drillup: function(e) {
                    chart.setTitle({ text: defaultTitle });
                }
            }
        },
        title: {
            text: defaultTitle
        },
        xAxis: {
            type: 'category'
        }, 
        yAxis: {
            title: {
                text: 'QTY'
            }

        },  
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                pointWidth:40,
                borderWidth: 1,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.0f}'
                }
            }
        }, 
        tooltip: {
            headerFormat: '<span style="font-size:18px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'
        },  
        series: myArray,
        drilldown: { series: myArray2 }
    })
 });

});