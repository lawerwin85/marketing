 <div class="main_menu" >
        <nav class="navbar navbar-expand-lg">
            <div class="container">

                <div class="navbar-collapse align-items-center collapse" id="navbar">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown mega-menu active">
                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="icon-speedometer"></i> <span> Dasboard Panel</span></a>
                            <div class="dropdown-menu mega-main padding-0 animated fadeIn">
                                <div class="row">
                                    <div class="col-lg-2 col-lg-auto col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled"> 
                                                <li><a href="#DashoverAll"><b>Dashboard OverAll</b></a></li> 
                                                <li><a href="#Dashproduct"><b>Dashboard</b></a></li> 
                                            </ul> 
                                        </div>
                                    </div>  
                                    <div class="col-lg-2 col-lg-auto col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled">
                                                <li><label>Dashboard by QTY</label></li>
                                                <li><a href="#Dash">Dashboard Wing Box</a></li>
                                                <li><a href="#Dashutl">Dashboard Utility</a></li> 
                                                <li><a href="#Dashmx">Dashboard Mixer</a></li> 
                                                <li><a href="#Dashdt">Dashboard Dump Truck</a></li> 
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-lg-auto col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled">
                                                <li><label>Dashboard by Amount</label></li>
                                                <li><a href="#Dashamount">Dashboard Wing Box</a></li>
                                                <li><a href="#Dashutlamount">Dashboard Utility</a></li> 
                                                <li><a href="#Dashmxamount">Dashboard Mixer</a></li> 
                                                <li><a href="#Dashdtamount">Dashboard Dump Truck</a></li> 
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-4 hidden-sm">
                                        <div class="img-box" style="background-image: url(<?php echo $base; ?>assets/images/menu-img/1.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown mega-menu active">
                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="icon-list"></i><span> Master Panel</span></a>
                            <div class="dropdown-menu mega-main padding-0 animated fadeIn">
                                <div class="row"> 
                                    <div class="col-lg-2 col-lg-auto col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled"> 
                                                <li><a href="#Inputsales">Input Sales</a></li> 
                                                <li><a href="#Inputchassis">Input Chassis</a></li>
                                                <li><a href="#Inputbarang">Input Produk</a></li> 
                                                <li><a href="#Inputcust">Input Customers</a></li> 
                                            </ul> 
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-lg-auto col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled"> 
                                                <li><a href="#Inputsize">Input Size</a></li> 
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-4 hidden-sm">
                                        <div class="img-box" style="background-image: url(<?php echo $base; ?>assets/images/menu-img/1.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php $group= $this->session->userdata('ses_aktorgroup');   
                            if ($group=='admin'){
                        ?>
                        <li class="nav-item dropdown mega-menu">
                            <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i> <span> Settings</span></a>
                            <div class="dropdown-menu mega-main padding-0 animated fadeIn">
                                <div class="row">
                                    <div class="col-lg-6 col-md-4 hidden-sm">
                                        <div class="img-box" style="background-image: url(<?php echo $base; ?>assets/images/menu-img/7.jpg)"></div>
                                    </div>
                                    <div class="col-lg-2 col-lg-auto col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled"> 
                                                <li><a href="#User"><b>Setting User</b></a></li> 
                                                <li><a href="#Logactivty"><b>Data Log Activity</b></a></li> 
                                                <li><a href="#Cekperiode"><b>Cek Periode</b></a></li>
                                            </ul> 
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </li>
                    <?php };?>
                        <li class="nav-item dropdown mega-menu active">
                            <a href="#GantiPass" class="nav-link"><i class="icon-key"></i><span> Ganti Password</span></a> 
                        </li>
                        <li class="nav-item dropdown mega-menu">
                            <!-- <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="icon-grid"></i> <span>Application</span></a>
                            <div class="dropdown-menu mega-main padding-0 animated fadeIn">
                                <div class="row">
                                    <div class="col-lg-2 col-lg-auto col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled">
                                                <li><label>Map</label></li>
                                                <li><a href="map-google.html">Google Map</a></li>
                                                <li><a href="map-yandex.html">Yandex Map</a></li>
                                                <li><a href="map-jvectormap.html">jVector Map</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="mega-list">
                                        	<ul class="list-unstyled">
                                                <li><label>Widgets</label></li>
                                                <li><a href="widgets-statistics.html">Statistics</a></li>
                                                <li><a href="widgets-data.html">Data</a></li>
                                                <li><a href="widgets-chart.html">Chart</a></li>
                                                <li><a href="widgets-weather.html">Weather</a></li>
                                                <li><a href="widgets-social.html">Social</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled">
                                                <li><label>Chart</label></li>
                                                <li><a href="chart-morris.html">Morris</a> </li>
                                                <li><a href="chart-flot.html">Flot</a> </li>
                                                <li><a href="chart-chartjs.html">ChartJS</a> </li>                                    
                                                <li><a href="chart-jquery-knob.html">Jquery Knob</a> </li>
                                                <li><a href="chart-sparkline.html">Sparkline Chart</a></li>
                                                <li><a href="chart-peity.html">Peity</a></li>
                                                <li><a href="chart-c3.html">C3 Charts</a></li>
                                                <li><a href="chart-gauges.html">Gauges</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-4 hidden-sm">
                                        <div class="img-box" style="background-image: url(<?php echo $base; ?>assets/images/menu-img/1.jpg)"></div>
                                    </div>
                                </div>
                            </div> -->
                        </li>
                        <li class="nav-item dropdown mega-menu">
                          <!--   <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="icon-docs"></i> <span>Pages</span></a>
                            <div class="dropdown-menu mega-main padding-0 animated fadeIn">
                                <div class="row">
                                    <div class="col-lg-6 col-md-4 hidden-sm">
                                        <div class="img-box" style="background-image: url(<?php echo $base; ?>assets/images/menu-img/7.jpg)"></div>
                                    </div>
                                    <div class="col-lg-2 col-lg-auto col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled">
                                                <li><label>Pages</label></li>
                                                <li><a href="page-blank.html">Blank Page</a> </li>
                                                <li><a href="page-profile2.html">Profile</a></li>
                                                <li><a href="page-gallery.html">Image Gallery</a> </li>
                                                <li><a href="page-gallery2.html">Image Gallery</a> </li>
                                                <li><a href="page-timeline.html">Timeline</a></li>
                                                <li><a href="page-timeline-h.html">Horizontal Timeline</a></li>
                                                <li><a href="page-pricing.html">Pricing</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled">
                                                <li><label>Pages</label></li>
                                                <li><a href="page-invoices.html">Invoices</a></li>
                                                <li><a href="page-invoices2.html">Invoices</a></li>
                                                <li><a href="page-search-results.html">Search Results</a></li>
                                                <li><a href="page-helper-class.html">Helper Classes</a></li>
                                                <li><a href="page-maintenance.html">Maintenance</a></li>
                                                <li><a href="page-testimonials.html">Testimonials</a></li>
                                                <li><a href="page-faq.html">FAQ</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4">
                                        <div class="mega-list">
                                            <ul class="list-unstyled">
                                                <li><label>Tables</label></li>
                                                <li><a href="table-basic.html">Tables Example</a> </li>
                                                <li><a href="table-normal.html">Normal Tables</a> </li>
                                                <li><a href="table-jquery-datatable.html">Jquery Datatables</a> </li>
                                                <li><a href="table-editable.html">Editable Tables</a> </li>
                                                <li><a href="table-color.html">Tables Color</a> </li>
                                                <li><a href="table-filter.html">Table Filter</a></li>
                                                <li><a href="table-dragger.html">Table dragger</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </li>
                        <li class="nav-item dropdown">
                           <!--  <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="icon-lock"></i> <span>Authentication</span></a>
                            <ul class="dropdown-menu animated bounceIn">                                    
                                <li><a href="page-login.html">Login</a></li>
                                <li><a href="page-register.html">Register</a></li>
                                <li><a href="page-lockscreen.html">Lockscreen</a></li>
                                <li><a href="page-forgot-password.html">Forgot Password</a></li>
                                <li><a href="page-404.html">Page 404</a></li>
                                <li><a href="page-403.html">Page 403</a></li>
                                <li><a href="page-500.html">Page 500</a></li>
                                <li><a href="page-503.html">Page 503</a></li>
                            </ul>
                        </li> -->
                        <li class="nav-item dropdown">
                           <!--  <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="icon-pencil"></i> <span>Forms</span></a>
                            <ul class="dropdown-menu animated bounceIn right-side">
                                <li><a href="forms-validation.html">Form Validation</a></li>
                                <li><a href="forms-advanced.html">Advanced Elements</a></li>
                                <li><a href="forms-basic.html">Basic Elements</a></li>
                                <li><a href="forms-wizard.html">Form Wizard</a></li>
                                <li><a href="forms-dragdropupload.html">Drag &amp; Drop Upload</a></li>
                                <li><a href="forms-cropping.html">Image Cropping</a></li>
                                <li><a href="forms-summernote.html">Summernote</a></li>
                                <li><a href="forms-editors.html">CKEditor</a></li>
                                <li><a href="forms-markdown.html">Markdown</a></li>
                            </ul> -->
                        </li>
                        <li class="nav-item dropdown">
                           <!--  <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="icon-diamond"></i> <span>UI Elements</span></a>
                            <div class="dropdown-menu animated bounceIn right-side colmun2-menu">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <ul class="list-unstyled">
                                            <li><a href="ui-typography.html">Typography</a></li>
                                            <li><a href="ui-tabs.html">Tabs</a></li>
                                            <li><a href="ui-buttons.html">Buttons</a></li>
                                            <li><a href="ui-bootstrap.html">Bootstrap UI</a></li>
                                            <li><a href="ui-icons.html">Icons</a></li>
                                            <li><a href="ui-notifications.html">Notifications</a></li>
                                            <li><a href="ui-colors.html">Colors</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <ul class="list-unstyled">
                                            <li><a href="ui-dialogs.html">Dialogs</a></li>                                    
                                            <li><a href="ui-list-group.html">List Group</a></li>
                                            <li><a href="ui-media-object.html">Media Object</a></li>
                                            <li><a href="ui-modals.html">Modals</a></li>
                                            <li><a href="ui-nestable.html">Nestable</a></li>
                                            <li><a href="ui-progressbars.html">Progress Bars</a></li>
                                            <li><a href="ui-range-sliders.html">Range Sliders</a></li>
                                            <li><a href="ui-treeview.html">Treeview</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->
                        </li>
                       <!--  <li class="navbar-right" style="margin-right: 300px;">
                            <a href="<?php echo site_url('Login/logout') ?>" class="nav-link"><i class="icon-login"></i> <span> Logout</span></a>

                        </li> -->
                    </ul>
                </div>

            </div>
        </nav>
    </div> 