<?php
class Mm_dashboardall extends CI_Model {
    
    
    private $table;
    private $table02;
    private $table03;

    public function __construct() {
        parent::__construct();
        $this->table = "sales"; 

    }  
    function data_sales($periode){
        $query = "SELECT tahun,c.Nmcust,b.nmbarang,v.keterangan,v.varian,z.size,d.nmchassis,h.chassis,s.periode,SUM(s.qty)qty,SUM(s.UTotal)total FROM sales s
                    INNER JOIN customers c ON c.idCust=s.idCust
                    INNER JOIN barang b ON s.kdBarang=b.kdBarang
                    INNER JOIN varian v ON v.id=s.varian_id
                    INNER JOIN size z ON z.id=s.size_id
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    GROUP BY s.varian_id,tahun" ;
        return $this->db->query($query);
    }     
    // function data_salesqty(){
    //     $query = "SELECT  IFNULL(SUBSTRING_INDEX(tgl_sales, '-', 1) , 'TOTAL') AS periodee,
    //                 SUM( IF( kat_size = 1, qty, 0) ) AS Big,
    //                 SUM( IF( kat_size = 2, qty, 0) ) AS Small,
    //                 SUM( IF( kat_size = 3, qty, 0) ) AS Medium, 
    //                 SUM( qty ) AS total 
    //             FROM sales
    //             GROUP BY periodee" ;
    //     return $this->db->query($query);
    // }  
     function data_salesqtyperiode(){
        $query = "SELECT t.tahun periodee,sum(t.qty) qty FROM sales t 
                    inner join varian v on v.id=t.varian_id 
                    group by t.tahun 
                    ORDER BY t.tahun ASC" ;
        return $this->db->query($query);
    }  
    function data_salesqtyperiodeamount(){
        $query = "SELECT t.tahun periodee,sum(t.UTotal)/1000 qty FROM sales t 
                    inner join varian v on v.id=t.varian_id 
                    group by t.tahun 
                    ORDER BY t.tahun ASC" ;
        return $this->db->query($query);
    }  
     function data_salesqtyperiodetotal(){
        $query = "SELECT t.tahun periodee,sum(t.qty) qty FROM sales t   
                    group by t.tahun 
                    ORDER BY t.tahun ASC" ;
        return $this->db->query($query);
    }  
        function data_salesqty(){
        $query = "SELECT t.tahun periodee,t.qtyTotalCategory qty,s.keterangan katsize FROM transaksi_sales t
                    LEFT OUTER JOIN categorysize s ON s.id=t.id_category  
                    ORDER BY t.tahun,s.seqno ASC" ;
        return $this->db->query($query);
    }  
    function data_salesqtytipe(){
        $query = "SELECT t.tahun periodee,t.qtyTotalTipe qty,s.tipe katsize FROM transaksi_salestipe t
                    LEFT OUTER JOIN tipe s ON s.id=t.id_tipe
                    ORDER BY t.tahun ASC" ;
        return $this->db->query($query);
    }   
    function data_salesqtyVarian($datavarian){
        $query = "SELECT t.tahun periodee,$datavarian qty,s.keterangan katsize FROM transaksi_sales t
                    LEFT OUTER JOIN categorysize s ON s.id=t.id_category  
                    ORDER BY t.tahun,s.seqno  ASC" ;
        return $this->db->query($query);
    }  
 
    function data_chassisdetail(){
        $query = "SELECT c.chassis,s.tahun,SUM(s.qty)qty FROM chassis c
                        LEFT JOIN chassis_detail d ON d.idchassis=c.id
                        LEFT JOIN sales s ON s.chassis_id=d.id
                        GROUP BY c.chassis,s.tahun  ORDER BY s.tahun,c.chassis ASC" ;
                            return $this->db->query($query);
    } 

    function data_saleschassis(){
        $query = " SELECT d.chassis,c.tahun,c.qty,c.totalV FROM transaksi_chassis c
                    INNER JOIN chassis d ON d.id=c.idchassis
                    ORDER BY c.tahun,c.qty ASC" ;
                            return $this->db->query($query);
    } 
    // function data_saleschassis(){
    //     $query = "SELECT t.tahun,SUM(s.qty) qty FROM m_tahun t
    //                 LEFT JOIN sales s ON t.tahun=s.tahun 
    //                 LEFT JOIN chassis_detail d ON s.chassis_id=d.id
    //                 LEFT JOIN chassis c ON d.idchassis=c.id 
    //                 GROUP BY c.chassis,t.tahun 
    //                 ORDER BY t.tahun,c.chassis ASC" ;
    //                         return $this->db->query($query);
    // }
    function data_salesqtyVarianfull(){
        $query = " SELECT t.tahun, CASE varian_id 
                     WHEN 1 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ1,
                     CASE varian_id 
                     WHEN 2 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ2,
                     CASE varian_id 
                     WHEN 3 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ3,
                     CASE varian_id 
                     WHEN 4 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ4,
                     CASE varian_id 
                     WHEN 5 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ5,
                     CASE varian_id 
                     WHEN 7 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ7,
                     CASE varian_id 
                     WHEN 8 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ8,
                     CASE varian_id 
                     WHEN 9 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQA,
                     CASE varian_id 
                     WHEN 10 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalCT,
                     CASE varian_id 
                     WHEN 11 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalwb FROM m_tahun t
                    LEFT OUTER JOIN sales s ON s.tahun=t.tahun
                    GROUP BY t.tahun,varian_id
                     ORDER BY tahun ASC" ;
        return $this->db->query($query);
    }  
      // function data_salesqty(){
      //   $query = "SELECT s.kat_size as katsize,COALESCE(c.keterangan, '0') ket,SUM(s.qty) qty,s.tahun periodee FROM categorysize c
      //               LEFT JOIN sales s ON c.id=s.kat_size
      //               GROUP BY size
      //               ORDER BY kat_size,tahun ASC" ;
      //     return $this->db->query($query);
      //   }
        function data_salescategory(){
        $query = "SELECT id,keterangan FROM categorysize ORDER BY seqno  ASC" ;
          return $this->db->query($query);
        }  

}
