var urlcust = urlpost+'Dash/gridcustperkat'; 

function displayVals(multipleValues) {
 // var singleValues = $( "#single" ).val();
  var multipleValues = $( "#tahun" ).val() || '2012';
  // When using jQuery 3: 
  getfunction(multipleValues);
}
 
$( "select" ).change( displayVals );
displayVals();    
function getfunction(year){   

var tahun=year;
var kat=$( "#kategori" ).val();
if (kat=='wb'){
    var jenis='WING BOX';
}
else if(kat=='ut'){
    var jenis ='UTILITY';
}
else if(kat=='mx'){
    var jenis ='MIXER';
}
else{
    var jenis='DUMP TRUCK';
}
        function graphbar(callback){  
            
                $.ajax({
                    url: urlcust, 
                    dataType: 'json', 
                    type:'POST',  
                    data: {
                        periode: tahun, 
                        kat: kat, 
                    },
                    success: function(data){    
                        var datacust=data.nilaissalescust;  
                        var cust=data.cust;  
                        callback({databarcust:datacust,custd:cust}); 
                    } 
                }); 
        } 
        graphbar(function(res){
            Array.prototype.contains = function(v) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === v) return true;
            }
            return false;
        };

        Array.prototype.unique = function() {
            var arr = [];
            for(var i = 0; i < this.length; i++) {
                if(!arr.includes(this[i])) {
                    arr.push(this[i]);
                }
            }
            return arr; 
        } 
            var listbarcust=res.databarcust; 
            var listcust=res.custd; 
            var periodebar=[];    
            var groups = {};   
 
            for (var i = 0; i < listbarcust.length; i++) {   
                        var listnilai=listbarcust[i].totalV;
                        var y =Number(listnilai);
                        var groupName2 = listbarcust[i].chassis;  
                        if (!groups[groupName2]) {
                            groups[groupName2] = [];
                          }   

                        
                          groups[groupName2].push({y:y,name:groupName2});  
            }
                        myArray = [];
                        for (var groupName2 in groups) { 
                          myArray.push({type: 'column',name: groupName2, data: groups[groupName2]});
                             
                         }
             
                        
        Highcharts.setOptions({
            lang: {
                decimalPoint: ',',
                thousandsSep: '.'
            }
        });
 
                Highcharts.chart('containerKatbuyer', {
                
                title: {
                    text: 'CUSTOMERS TOP 15 '+jenis,
                    enabled:false
                },
                exporting: {
                            enabled:false
                        },
                subtitle: {
                    text:null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    type: 'category' 
                },
                yAxis: {
                    min: 0,
                    title: {
                        text:'AMOUNT / K'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td align="right" style="padding:0"><b>{point.y:,.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: { 
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        pointWidth:22,
                        dataLabels: {
                          enabled: false,
                          formatter: function() {
                            if (this.y > 0) {
                              return this.y;
                            }
                          }
                        },
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                   //alert('Category: ' + this.category + ', value: ' + this.series.name);  
                                  window.open ('#Reportdash?category=cust&tahun='+tahun+'&kat='+this.series.name+'&varian=all','_blank'); 
                                }
                            }
                        }
                    }
                },
                series:myArray
            });
        });    
}