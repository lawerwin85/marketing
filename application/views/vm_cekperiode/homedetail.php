 <div class="container"> 
         <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <input type="hidden" value="<?php echo $id;?>" id="id">
                            <h2>List Periode</h2>                            
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table  id="datatable-ajax" class="table table-hover js-basic-example dataTable table-custom">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>No</th>
                                            <th>Varian</th> 
                                            <th>Size</th> 
                                            <th>Total</th> 
                                            <th>Field Data</th> 
                                            <th>Action</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                         
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
        </div>
  <script>
   var InputSales = {
        table: undefined
    };
    $(document).ready(function () {
        /* start datatable */
        var id=$("#id").val();
        var handleDataTableDefault = function () {
            if ($("#datatable-ajax").length!==0) {
               InputSales.table = $("#datatable-ajax").DataTable({
                    "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
                        var oSettings = this.fnSettings();
                        var iTotalRecords = oSettings.fnRecordsDisplay();
                        $("#total").val(iTotalRecords);
                        $("td:first", nRow).html(iDisplayIndex +1);
                       return nRow;
                    },
                    "ajax": {
                        "url": '<?php echo $url_griddetail; ?>',
                        "type": 'POST',
                        "data" : {
                            "id" :id 
                        },
                    },
                    "columns": [
                        {"data": "kat_size"}, 
                        {"data": "varian"}, 
                        {"data": "keterangan"},
                        {"data": "qty"}, 
                        {"data": "field"}, 
                        {
                            "data": "id", "width": "100px", "sClass": "left",
                            "bSortable": false,
                            "mRender": function (data, type, row) {
                                var btn = "";
                               
                                btn = btn + " <button onClick='ParamFunc.editdata(tahun="  + id+ ",kat="  + row.kat_size + ",vard="+ row.varian_id +")' class='btn btn-xs btn-icon btn-circle btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit Data' type='button'><i class='fa fa-edit'></i>Proses Calculation </button>";
 
                                
                                btn = btn + "</div>";
                                return btn;
                            }
                        }
                    ]
                });
            }
        };
        TableManageDefault = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableDefault();
                }
            };
        }();
        TableManageDefault.init();
    });
    /* end datatable */
 

        var urlpost = '<?php echo $url_post; ?>';

    /*start edit, delete  function */
    var ParamFunc = {

        editdata: function (tahun,kat,vard) {
            $.ajax(
                {
                    type: "POST",
                    url: urlpost,
                    dataType: "json",
                    data: {
                        tahun: tahun,
                        kategori: kat,
                        varian: vard
                    },
                    cache: false,
                    success:
                            function (data, text)
                            {
                                if (data.hasil == 'true') {
                                    
                                        InputSales.table.ajax.reload();
                                } else {
                                    alert(data.msg);

                                }
                            },
                    error: function (request, status, error) {
                        alert(request.responseText + " " + status + " " + error);
                    }
                });
        },
        deletedata: function (id) {
              var jawab = confirm("Apakah anda yakin untuk menghapus !");
                if (jawab === true) {
        //            kita set hapus false untuk mencegah duplicate request
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        $.post('<?php echo $url_delete ?>', {id: id},
                        function (data) {
                           InputSales.table.ajax.reload();
                        });
                        hapus = false;
                    }
                } else {
                    return false;
                }
        }
    }
    /*end edit, delete  function */

    //start delete data
    // $("#confirmDelete a[action=delete]").click(function () {
    //     var url;
    //     url = '<?php echo $url_delete ?>';
    //     $.ajax({
    //         url: url,
    //         type: "post",
    //         dataType: "json",
    //         cache: false,
    //         data: {
    //             idpost: $("#confirmDelete input[name=idpost]").val()
    //         },
    //         success: function (data) {
    //            $('#confirmDelete').modal('hide');
    //             Cm_Kelas.table.ajax.reload();
    //         }
    //     });
    // });
    //end delete data

    /*start add function */
 
    /*end add function */

   

</script>