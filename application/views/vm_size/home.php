 <div class="container"> 
         <div class="col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>List Size<small><button onclick="adddata()"  class="btn btn-primary">Input Size</button></small></h2>                            
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table width="80%" id="datatable-ajax" class="table table-hover js-basic-example dataTable table-custom">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th width="10">No</th>
                                            <th  width="80">Size</th>  
                                            <th>Action</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                         
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
        </div>
  <script>
   var Inputsize = {
        table: undefined
    };
    $(document).ready(function () {
        /* start datatable */
        var handleDataTableDefault = function () {
            if ($("#datatable-ajax").length!==0) {
               Inputsize.table = $("#datatable-ajax").DataTable({
                    "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
                        var oSettings = this.fnSettings();
                        var iTotalRecords = oSettings.fnRecordsDisplay();
                        $("#total").val(iTotalRecords);
                        $("td:first", nRow).html(iDisplayIndex +1);
                       return nRow;
                    },
                    "ajax": {
                        "url": '<?php echo $url_grid; ?>',
                        "type": 'POST',
                    },
                    "columns": [
                        {"data": "id"},
                        {"data": "size"},  
                        {
                            "data": "id", "width": "20px", "sClass": "left",
                            "bSortable": false,
                            "mRender": function (data, type, row) {
                                var btn = "";
                               
                                btn = btn + " <button onClick='ParamFunc.editdata(" + row.id + ")' class='btn btn-xs btn-icon btn-circle btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit Data' type='button'><i class='fa fa-edit'></i> </button>"; 
                                btn = btn + " <button onClick='ParamFunc.deletedata(" + row.id + ")' class='btn btn-xs btn-icon btn-circle btn-danger' data-toggle='tooltip' data-placement='bottom' title='Hapus Data' type='button'><i class='fa fa-trash'></i> </button>"; 
                                
                                btn = btn + "</div>";
                                return btn;
                            }
                        }
                    ]
                });
            }
        };
        TableManageDefault = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableDefault();
                }
            };
        }();
        TableManageDefault.init();
    });
    /* end datatable */



    /*start edit, delete  function */
    var ParamFunc = {
        editdata: function (id) {
            window.top.location.href = 'index.php#Inputsize/edit/'+id; 
        }, 
        deletedata: function (id) {
              var jawab = confirm("Apakah anda yakin untuk menghapus !");
                if (jawab === true) {
        //            kita set hapus false untuk mencegah duplicate request
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        $.post('<?php echo $url_delete ?>', {id: id},
                        function (data) {
                           Inputsize.table.ajax.reload();
                        });
                        hapus = false;
                    }
                } else {
                    return false;
                }
        } 
    }
    /*end edit, delete  function */

    //start delete data
    // $("#confirmDelete a[action=delete]").click(function () {
    //     var url;
    //     url = '<?php echo $url_delete ?>';
    //     $.ajax({
    //         url: url,
    //         type: "post",
    //         dataType: "json",
    //         cache: false,
    //         data: {
    //             idpost: $("#confirmDelete input[name=idpost]").val()
    //         },
    //         success: function (data) {
    //            $('#confirmDelete').modal('hide');
    //             Cm_Kelas.table.ajax.reload();
    //         }
    //     });
    // });
    //end delete data

    /*start add function */
    function adddata() {
        window.top.location.href = 'index.php#Inputsize/addSize'; 
    }
    /*end add function */

   

</script>