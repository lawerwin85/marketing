<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Logactivty extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_logactivity', 'table01'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('Logactivty/grid'), 
        );
        $resuluser= $this->array->data_user();
        $e = 0;
        foreach ($resuluser as $rowuser) {              
            $data['default']['user'][$e]['value'] = $rowuser['userName'];
            $data['default']['user'][$e]['display'] = $rowuser['userName']; 
            $e++;
        }  
        $user = $this->session->userdata('ses_userName'); 
         $usergroup= $this->session->userdata('ses_aktor'); 
         $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='View Log Table'; 
        $log_trans='LogTable'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("view", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->load->view('vm_activity/home', $data); 
        // $this->load->view('vm_User/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        $user = $this->input->post('user');  
        echo json_encode(array(
            "data" => $this->table01->getGridData($user)->result()
        ));
    }  
  

}
