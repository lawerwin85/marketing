<?php

/* membuat class dengan nama Magama_model*/
class Mm_inputsales extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "sales"; 

    }

     /* mendapatkan semua data dan hasilnya sebuah array */
    public function getAll() {
        return $this->db->get($this->table)->result_array();
    }
 
    function getGridData() {
        $query = "SELECT s.*,b.nmBarang,c.NmCust2,DATE_FORMAT(s.tgl_sales,'%d/%m/%Y') as tglsales,CONCAT( FORMAT(s.unit_price, 2)) as unitprice,CONCAT( FORMAT(s.UTotal, 2)) as total from sales s 
        INNER JOIN barang b on b.kdBarang=s.kdBarang
        INNER JOIN customers c on c.idCust=s.idCust order by s.crtdt DESC";
        return $this->db->query($query);      
    }
    function getGridDatachassis($id) {
        $query = "SELECT * from chassis_detail where idchassis='$id'";
        return $this->db->query($query);      
    }
    function getGridDatabarang($id) {
        $query = "SELECT b.* from barang b where b.chassis_d='$id' and status=0";
        return $this->db->query($query);      
    }
    function insert($record) {
        $this->db->insert($this->table, $record);
    }

    function getchassisby_id($id) {
        $query = "SELECT * from chassis_detail where id='$id'";
        return $this->db->query($query);   
    }

    function getby_id($id) {
        $query = "SELECT s.*,h.id as chassis_h,d.*,b.* ,k.*,s.kategori_id FROM sales s
                    INNER JOIN customers c ON c.idCust=s.idCust
                    INNER JOIN barang b ON s.kdBarang=b.kdBarang
                    INNER JOIN varian v ON v.id=s.varian_id
                    INNER JOIN size z ON z.id=s.size_id
                    INNER JOIN kategori  k ON k.id=s.kategori_id
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    LEFT JOIN categorysize g ON g.id=s.kat_size
                    LEFT JOIN tipe t ON t.id=s.id_tipe
                    where s.id='$id'";
        return $this->db->query($query);   
    }
    function getby_idvarian($varian) {
        $query = "SELECT * from varian where id='$varian'";
        return $this->db->query($query);   
    }
    function getby_idsize($size) {
        $query = "SELECT * from size where id='$size'";
        return $this->db->query($query);   
    }
    function getbarangby_id($id) {
        $query = "SELECT s.*,h.id as chassis_h,d.*,k.*,v.varian,g.keterangan,z.size,t.tipe FROM barang s 
                    INNER JOIN varian v ON v.id=s.varian_id
                    INNER JOIN size z ON z.id=s.size_id
                    INNER JOIN kategori  k ON k.id=s.kategori_id
                    INNER JOIN chassis_detail d ON d.id=s.chassis_d
                    INNER JOIN chassis h ON h.id=d.idchassis
                    LEFT JOIN categorysize g ON g.id=s.kat_size
                    LEFT JOIN tipe t ON t.id=s.id_tipe
                    where s.id='$id'";
        return $this->db->query($query);   
    }
     
    function update($id, $record) {
        $this->db->where("id", $id);
        $this->db->update($this->table, $record);
    }
  //ddd  
  function updatetransaksi($tahun,$ckatageri,$qtyTotal,$qty) {
        $query = "UPDATE transaksi_sales set $qtyTotal=$qtyTotal+$qty,qtyTotalCategory=qtyTotalCategory+$qty where tahun='$tahun' and id_category='$ckatageri'";
        return $this->db->query($query);  
    }
//eee 
   function updatetransaksiamount($tahun,$ckatageri,$qtyTotal,$qtyamount) {
        $query = "UPDATE transaksi_salesamount set $qtyTotal=$qtyTotal+$qtyamount,qtyTotalCategory=qtyTotalCategory+$qtyamount where tahun='$tahun' and id_category='$ckatageri'";
        return $this->db->query($query);  
    }
     
    function updatetransaksiremove($tahun,$ckatageri,$qtyTotal,$qty) {
        $query = "UPDATE transaksi_sales set $qtyTotal=$qtyTotal-$qty,qtyTotalCategory=qtyTotalCategory-$qty where tahun='$tahun' and id_category='$ckatageri'";
        return $this->db->query($query);  
    }
    function updatetransaksitiperemove($tahun,$tipe,$qtyTotal,$qty) {
        $query = "UPDATE transaksi_salestipe set qtyTotalTipe= qtyTotalTipe-$qty where tahun='$tahun' and id_tipe='$tipe'";
        return $this->db->query($query);  
    }

    function updatetransaksidetail($tahun,$ckatageri,$qtyTotal,$field,$qty,$qty2) {
        $query = "UPDATE transaksi_sales set $qtyTotal=$qtyTotal+$qty,$field=$field-$qty2,qtyTotalCategory=(qtyTotalCategory-$qty2)+$qty where tahun='$tahun' and id_category='$ckatageri'";
        return $this->db->query($query);  
    }
    // function updatetransaksitipe($tahun,$tipe,$qtyamount,$qtyTotal) {
    //     $query = "UPDATE transaksi_salestipe set qtyTotalamount= qtyTotalamount+$qtyamount where tahun='$tahun' and id_tipe='$tipe'";
    //     return $this->db->query($query);  
    // }
  //eee
    function updatetransaksitipe($tahun,$tipe,$qty,$qtyamount) {
        $query = "UPDATE transaksi_salestipe set qtyTotalTipe= qtyTotalTipe+$qty,qtyTotalamount=qtyTotalamount+$qtyamount where tahun='$tahun' and id_tipe='$tipe'";
        return $this->db->query($query);  
    }
    function delete($id) {
        $this->db->delete($this->table, array("id" => $id)
        );
    }

}
