<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Calculation extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_Calculation', 'table01'); 
        $this->load->model('Mm_Inputsales', 'table02'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('Calculation/grid'),
            "url_add" => site_url('Calculation/addChassis'),
            "url_edit" => site_url('Calculation/editdetail'),
            "url_delete" => site_url('Calculation/remove'),
        );
        $this->load->view('vm_calculation/home', $data); 
        // $this->load->view('vm_calculation/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        echo json_encode(array(
            "data" => $this->table01->getGridData()->result()
        ));
    }
    function editdetail($id) {
  
        $data = array(
            "base" => base_url(), 
            "url_griddetail" => site_url('Calculation/griddetail'),
            "url_post" => site_url('Calculation/postsales'),
            "url_add" => site_url('Calculation/addChassisdetil'),
            "url_delete" => site_url('Calculation/remove'),
        );
        $data['id'] = $id; 
        $this->load->view('vm_calculation/homedetail', $data); 
    }
    public function griddetail() {
         $id = $this->input->post('id'); 
        echo json_encode(array(
            "data" => $this->table01->getGridDatadetail($id)->result()
        ));
    }
    function addChassis() {
        $data['title'] = 'Add - Input Chassis';   
        $data['chassisdetail'] = '';   
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis']; 
            $e++;
        }  
        $data['url_post'] = site_url('Calculation/addpost');  
        $data['url_getchassis'] = site_url('Calculation/getchassis'); 
        $data['url_index'] = site_url('Calculation'); 
        $data['id'] = 0; 

        $this->load->view('vm_calculation/form', $data); 
    }
    function addChassisdetil($id) {
        $data['title'] = 'Add - Input Chassis';   
        $data['chassisdetail'] = '';   
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis'];
            if ($id == $rowchassis['id']) {
                $data['default']['chassis'][$e]['selected'] = "SELECTED";
            } 
            $e++;
        }  
        $data['url_post'] = site_url('Calculation/addpost');  
        $data['url_getchassis'] = site_url('Calculation/getchassis'); 
        $data['url_index'] = site_url('Calculation'); 
        $data['id'] = 0; 

        $this->load->view('vm_calculation/form', $data); 
    }    
    public function postsales() { 
            $varian = $this->input->post('varian');  
            $ckatageri = $this->input->post('kategori'); 
            $tahun = $this->input->post('tahun'); 
            // var_dump($periode);
            // die();
            $record = array(
                "ckatageri" => $ckatageri,
                "varian" => $varian, 
                "tahun" => $tahun,
                "crtBy" => 'ep',
                "updBy" => 'ep' 
            );

        $row = $this->table01->getby_id($tahun,$ckatageri,$varian)->row();  
        $qtyTotal=$row->field; 
        $qty=$row->qty;
        $qtyamount=$row->qtyamount;
        $tipe=$row->id_tipe;  
        // var_dump($qtyTotal);
        // die();
        $this->table02->updatetransaksi($tahun,$ckatageri,$qtyTotal,$qty); 
        //$this->table02->updatetransaksi($tahun,$ckatageri,$qtyTotal,$qtyamount); 
        //$this->table02->updatetransaksitipe($tahun,$tipe,$qty,$qtyTotal);
        // $this->table02->updatetransaksitipe($tahun,$tipe,$qtyamount,$qtyTotal);
        $this->table01->updatesalesstatus($tahun,$ckatageri,$varian);
      
        //$this->table01->insert($record); 
         $valid = 'true';
         $message = 'data insert';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi edit ini akan mensetting nilai-nilai di form ketika mengklik tombol edit */

    function edit($id) {
        $row = $this->table01->getby_id($id)->row();   
        $data['chassisdetail'] =$row->nmChassis;      
        $idchassis=$row->idchassis;     
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis'];
            if ($idchassis == $rowchassis['id']) {
                $data['default']['chassis'][$e]['selected'] = "SELECTED";
            } 
            $e++;
        } 
        $data['url_post'] = site_url('Calculation/editpost');  
        $data['url_index'] = site_url('Calculation'); 
        $data['id'] = $id; 
        $this->load->view('vm_calculation/form', $data); 
    }

    /* fungsi untuk post data ketika melakukan edit data, fungsi ini akan masuk ke database */

    function editpost() {
        $id = $this->input->post('id');
        $chassisdetail = $this->input->post('chassisdetail');
        $chassis = $this->input->post('chassis'); 
            // var_dump($periode);
            // die();
            $record = array(
                "nmChassis" => $chassisdetail,
                "idchassis" => $chassis, 
                "crtBy" => 'ep',
                "updBy" => 'ep' 
            );
        $this->table01->update($id,$record);   
         $valid = 'true';
         $message = 'data update';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi untuk delete data */

    public function remove() {
        $id = $this->input->post('id');
        $this->table01->delete($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

}
