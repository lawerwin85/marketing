<?php

/* membuat class dengan nama Magama_model*/
class Userlogin_model extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "userlogin"; //setting di dalam $this->table dalah nama tabel sesuai di databasenya
    }

     /* mendapatkan semua data dan hasilnya sebuah array */
    public function getAll() {
        return $this->db->get($this->table)->result_array();
    }

    function checkData($param){
        $this->db->where("username",$param);
        $result= $this->db->get($this->table)->num_rows();  
        return $result;
    }
    
    function getGridData() {
        $query = "
                 SELECT a.*                        
                 FROM $this->table a   
                 ";
        return $this->db->query($query);      
    }
  
    function insert($record) {
        $this->db->insert($this->table, $record);
    }

    function getby_id($id) {
        $this->db->where("username", $id);
        return $this->db->get($this->table);
    }

    function update($id, $record) {
        $this->db->where("username", $id);
        $this->db->update($this->table, $record);
    }

    function delete($id) {
        $this->db->delete($this->table, array("username" => $id)
        );
    }

}
