var urlallamount = urlpost+'DashoverAll/gridsalesqtyperiodeamount'; 
  $(function () { 
        function graphbar(callback){ 
              
                $.ajax({
                    url: urlallamount, 
                    dataType: 'json', 
                    type:'POST', 
                    data: {
                        periode: '2019', 
                    },
                    success: function(data){    
                        var datas=data.nilaissales;   
                        var datatotal=data.totalsales;    
                        callback({databar:datas,qtysales:datatotal}); 
                    } 
                }); 
        } 
        graphbar(function(res){
            Array.prototype.contains = function(v) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === v) return true;
            }
            return false;
        };

        Array.prototype.unique = function() {
            var arr = [];
            for(var i = 0; i < this.length; i++) {
                if(!arr.includes(this[i])) {
                    arr.push(this[i]);
                }
            }
            return arr; 
        } 
            var list=res.databar; 
            var listtotal=res.qtysales; 
           console.log(list);
            var periodebar=[];   
            var kategori=[];
            var groups = {}; 
            
            for (var i = 0; i < list.length; i++) {   
                  var periode =list[i].periodee;
                  var nmkatsize=list[i].periodee;
                  var groupName = "Overall SALES TREND by AMOUNT";
                    var listnilai=list[i].qty;
                    var nilai =Number(listnilai);
                 periodebar.push(periode);
                 kategori.push({name:nmkatsize}); 
                 if (!groups[groupName]) {
                            groups[groupName] = [];
                          }
                          groups[groupName].push(nilai);  
            }  
                        myArray = [];
                        for (var groupName in groups) { 
                          myArray.push({type: 'column',name: groupName, data: groups[groupName]});
                     console.log(groups[groupName]);
                        }     
                        myArray.push({
                            type: 'spline',
                            showInLegend: false,  
                            data: groups[groupName],
                            name: curvetrend,
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[3],
                                fillColor: 'white'
                            }
                        });
        var uniques = periodebar.unique(); 
        uniques.sort();  
        uniques.sort();  
        Highcharts.setOptions({
            lang: {
                decimalPoint: ',',
                thousandsSep: '.'
            }
        });
    var chart, merge = Highcharts.merge;
 var perShapeGradient = {
            x1: 0,
            y1: 0,
            x2: 1,
            y2: 0
        };
        var colors = Highcharts.getOptions().colors;
        colors = [{
            linearGradient: perShapeGradient,
            stops: [
                [0, 'rgb(247, 111, 111)'],
                [1, 'rgb(220, 54, 54)']
                ]
            }, {
            linearGradient: merge(perShapeGradient),
            stops: [
                [0, 'rgb(120, 202, 248)'],
                [1, 'rgb(46, 150, 208)']
                ]
            }, {
            linearGradient: merge(perShapeGradient),
            stops: [
                [0, 'rgb(136, 219, 5)'],
                [1, 'rgb(112, 180, 5)']
                ]}, 
        ];
                      //  console.log(myArray);
                Highcharts.chart('containerFullperiodeamount', {
                
                title: {
                    text: 'Overall SALES BASED ON AMOUNT ',
                    enabled:false
                },
                exporting: {
                            enabled:false
                        },
                subtitle: {
                    text:null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: uniques 
                },
                yAxis: {
                    min: 1,
                    title: {
                        text: 'AMOUNT / K'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td align="right" style="padding:0"><b>{point.y:,.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                   //alert('Category: ' + this.category + ', value: ' + this.series.name);  
                                  window.open ('index.php#Reportdash?category=overall&tahun='+this.category+'&kat=overall'+'&varian=none','_blank');  
                                }
                            }
                        },
                   dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        } 
                    }
                    }
                },
                series:myArray
            });
        });    
})