<!doctype html>
<html lang="en">


<!-- Mirrored from www.wrraptheme.com/templates/lucid/hr/html/h-menu/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 May 2019 04:26:44 GMT -->
<head>
<title>:: Digital Marketing :: MTI</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Lucid Bootstrap 4x Admin Template">
<meta name="author" content="WrapTheme, design by: ThemeMakker.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/select2/select2.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/chartist/css/chartist.min.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/toastr/toastr.min.css"> 
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/parsleyjs/css/parsley.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">


<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<!-- MAIN CSS -->
<link rel="stylesheet" href="<?php echo $base; ?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/css/color_skins.css">
</head>
<body class="theme-orange">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"> </div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->

<div id="wrapper">

 
	<?php $this->load->view('pages/topnav');?>  
     
	<?php $this->load->view('pages/rightbar');?>
    
	<?php $this->load->view('pages/main_menu');?>
	<div id="main-content">
		<?php $this->load->view('pages/content');?>
	</div>
   

    
</div>

<!-- Javascript -->
 <script src="<?php echo $base; ?>assets/bundles/libscripts.bundle.js"></script>
<script src="<?php echo $base; ?>assets/bundles/vendorscripts.bundle.js"></script>
<!--  
 <script src="<?php echo $base; ?>assets/js/jquery.min.js"></script>
 -->
<script src="<?php echo $base; ?>assets/vendor/toastr/toastr.js"></script>
<script src="<?php echo $base; ?>assets/bundles/chartist.bundle.js"></script>
<script src="<?php echo $base; ?>assets/bundles/knob.bundle.js"></script> <!-- Jquery Knob-->

<script src="<?php echo $base; ?>assets/vendor/select2/select2.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
 
<script src="<?php echo $base; ?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script> 
<script src="<?php echo $base; ?>assets/vendor/parsleyjs/js/parsley.min.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="<?php echo $base; ?>assets/bundles/mainscripts.bundle.js"></script>
<script src="<?php echo $base; ?>assets/bundles/datatablescripts.bundle.js"></script>
<script src="<?php echo $base; ?>assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="<?php echo $base; ?>assets/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="<?php echo $base; ?>assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="<?php echo $base; ?>assets/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="<?php echo $base; ?>assets/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>

 
<script src="https://cdn.rawgit.com/igorescobar/jQuery-Mask-Plugin/1ef022ab/dist/jquery.mask.min.js"></script> 
        <script type="text/javascript">
             /*mulai setting url default */
                    var ROOT = {
                        'site_url': '<?php echo site_url(); ?>',
                        'base_url': '<?php echo base_url(); ?>',

                    };
           
                    function ToController(controller, title,callback) {

                        var content, url;
                        content = $('#contentdata');
                        url = ROOT.site_url + '/' + controller;
                       // content.fadeIn("fast", "linear");
                       // / geturl(controller);
                        content.load(url);
                        content.fadeIn("slow");
                        return false;
                        url.empty(); 
                          callback(controller);
                    }

                    function load_form(url, title) {
                        var content;
                        content = $("#contentdata");
                        content.fadeOut("slow", "linear");
                        content.load(url);
                        content.fadeIn("slow");
                    }

                    $(window).on('hashchange', function(e){
                       var origEvent = e.originalEvent;
                       var newURL = origEvent.newURL.split('#');
                       //if(newURL.length>1){
                            var toUrl = newURL[1];
                            if(!toUrl) toUrl='Dash';
                            //if(toUrl!=''){
                                ToController(toUrl);
                            //}
                       //} 
                        console.log('Going to: ' + origEvent.newURL + ' from: ' + origEvent.oldURL);
                    });

                    (function(){
                        var url = window.location.href;
                        var newURL = url.split('#');
                        var toUrl = newURL[1];
                        if(!toUrl) toUrl='Dash';
                        ToController(toUrl);
                            
                    })();
var urlpost = ROOT.site_url+'/';
 

// var x = geturl(); 
// console.log(x); 
//var urlpost = 'http://kubekubek.com/digitalDashMti/index.php/';
var curvetrend = 'Trend';
var QTY = 'QTY';
var Values = 'Values';
        </script>    
</body>

<!-- Mirrored from www.wrraptheme.com/templates/lucid/hr/html/h-menu/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 May 2019 04:27:29 GMT -->
</html>
