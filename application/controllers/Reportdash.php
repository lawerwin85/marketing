<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Reportdash extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_reportdash', 'table01'); 
        $this->load->model('Mm_inputsales', 'table02'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('Reportdash/grid'),
            "url_add" => site_url('Reportdash/addBarang'),
            "url_edit" => site_url('Reportdash/edit'),
            "url_delete" => site_url('Reportdash/remove'),
        );
        $this->load->view('vm_reportdash/home', $data); 
        // $this->load->view('vm_reportdash/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
         $category = $this->input->post('category'); 
         $tahun = $this->input->post('tahun'); 
         $kat = $this->input->post('kat'); 
         $varian = $this->input->post('varian'); 
          
        echo json_encode(array(
            "data" => $this->table01->getGridData($category,$tahun,$kat,$varian)->result()
        ));
    }  
 
 
    public function gridchassis() {
         $id = $this->input->post('id'); 
        echo json_encode(array(
            "data" => $this->table02->getGridDatachassis($id)->result()
        ));
    } 
 
 

}
