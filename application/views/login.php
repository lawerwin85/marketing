<!doctype html>
<html lang="en">


<!-- Mirrored from www.wrraptheme.com/templates/lucid/hr/html/h-menu/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 May 2019 04:28:21 GMT -->
<head>
<title>:: Dashboard Login ::</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Lucid Bootstrap 4x Admin Template">
<meta name="author" content="WrapTheme, design by: ThemeMakker.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/vendor/sweetalert/sweetalert.css">
<!-- MAIN CSS -->
<link rel="stylesheet" href="<?php echo $base; ?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo $base; ?>assets/css/color_skins.css">
<style type="text/css">
    body{background:url(<?php echo $base; ?>assets/images/auth_bg.jpg) no-repeat top left fixed};
</style>
</head>

<body class="theme-orange">
    <!-- WRAPPER -->
    <div id="wrapper">
        <div class="vertical-align-wrap">
            <div class="vertical-align-middle auth-main">
                <div class="auth-box">
                    <div class="top"> 
                    </div>
                    <div class="card">
                        <div class="header">
                            <p class="lead">Login to your account</p>
                        </div>
                        <div class="body">
                            <form autocomplete="off" class="form-auth-small">
                                <div class="form-group">
                                    <label for="signin-email" class="control-label sr-only">Email</label>
                                    <input type="text" class="form-control" id="username">
                                </div>
                                <div class="form-group">
                                    <label for="signin-password" class="control-label sr-only">Password</label>
                                    <input type="password" class="form-control" id="password">
                                </div> 
                                <button type="submit" id="login" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                                <div class="bottom"> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <script src="<?php echo $base; ?>assets/js/jquery.min.js" type="text/javascript"></script>  
    <script src="<?php echo $base; ?>assets/vendor/sweetalert/sweetalert.min.js"></script>
   <script type="text/javascript">
    var url_post; 
    url_post = '<?php echo $url_post; ?>'; //setting url post, variable di dapat dari controller  
 

    $(document).ready(function ()
    {
 
 
        $('#login').click(
                function ()
                {
                    $.ajax(
                            {
                                type: "POST", //type ajax adalah post
                                url: url_post, // ketik di klik, akan mengirim data ke url postnya
                                dataType: "json", //data yang di kirim berupa json
                                data: {
                                    username: $("#username").val(), //username: adalah sebuah properties, $("#username") adalah sebuah object dari id input username
                                    password: $("#password").val() //password: adalah sebuah properties, $("#password") adalah sebuah object dari id input password

                                },
                                cache: false,
                                success:
                                        function (data, text)
                                        {

                                            if (data.hasil == true) {
//                                                alert(data.msg); //ketika sukses maka akan masuk ke halaman dashboard di controller dashboard
                                                //toastr.info(data.msg);
                                                setTimeout(function () {
                                                    window.location = data.redirecto;
                                                },1000);
                                            } else {
                                                  swal(" ",data.msg  , "error");
                                                    setTimeout(function () {
                                                        window.location = data.redirecto;
                                                    },2000);
                                                // $("#err_username").html(data.err_username).fadeIn('slow');
                                                // $("#err_password").html(data.err_password).fadeIn('slow');
                                                // if (data.err_loginas == null) {
                                                //     console.log(data.msg); 
                                                //     setTimeout(function () {
                                                //         window.location = data.redirecto;
                                                //     },1000);
                                                // }

                                            }
                                        },
                                error: function (request, status, error) {
                                    alert(request.responseText + " " + status + " " + error);
                                }
                            });
                    return false;


                }); 
    });
 
</script>    
</body>

<!-- Mirrored from www.wrraptheme.com/templates/lucid/hr/html/h-menu/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 May 2019 04:28:21 GMT -->
</html>
