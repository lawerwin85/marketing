<?php

defined('BASEPATH') OR exit('No direct script access allowed');


/* class dengan nama userdata */

class userdata extends CI_Controller {
    

    function __construct() {
        parent::__construct();
        $this->load->model('Userlogin_model', 'user01');
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller userdata di akses */

    public function index() {

        /* setting array key untuk di home.php agar urlnya dinamis, maka 
         * ketika copy home.php hanya mengubah parameternya di controller saja
         */
        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('userdata/grid'),
            "url_add" => site_url('userdata/add'),
            "url_edit" => site_url('userdata/edit'),
            "url_delete" => site_url('userdata/remove'),
        );
        $this->load->view('v_userdata/home', $data); 
        $this->load->view('v_userdata/confirm_delete', $data); 
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        echo json_encode(array(
            "data" => $this->user01->getGridData()->result()
        ));
    }

    
    function add() {
        $data['title'] = 'Add - user01'; 
        $data['default']['username'] = '';
        $data['default']['password'] = '';
        $data['default']['fullname'] = '';
        $data['default']['email'] = ''; 
        $data['url_post'] = site_url('userdata/addpost'); 
        $data['url_index'] = site_url('userdata');
        $data['id'] = 0; 

        $this->load->view('v_userdata/form', $data); 
    }

    /* fungsi untuk post data ketika melakukan add data, fungsi ini akan masuk ke database */

    public function addpost() {
         $this->form_validation->set_rules('username', 'postuser', 'required');
         $this->form_validation->set_rules('password', 'postpass', 'required');
         $this->form_validation->set_rules('fullname', 'postname', 'required');
        $this->form_validation->set_rules('email', 'postemail', 'required'); 

        if ($this->form_validation->run() == TRUE) { 
            $user = $this->input->post('postuser');
             $pass = $this->input->post('postpass');
              $name = $this->input->post('postname');
                $email = $this->input->post('postemail'); 

            /* membuat record sebuah array, array ini akan masuk ke database */
            $record = array(
                "username" => $user,
                "password" => md5($pass),
                "fullname" => $name,
                "email" => $email,
                
            );

            $checkdata = $this->user01->checkdata($user); 
            if ($checkdata > 0) {

                $valid = 'false';
                $message = 'data already exist';
                $err_user = "UserName userdata sudah ada";
            } else { /* jika data belum ada,maka berhasil di simpan */
                $this->user01->insert($record);
                $valid = 'true';
                $message = "Insert data, success";
                $err_user = null;
            }

            /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
            $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid,
                "err_user" => $err_user,
                "err_pass" => null,
                "err_fullname" => null,
                 "err_email" => null,
            );
        } else {
            /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
            $jsonmsg = array(
                "msg" => 'Insert Data Failed',
                "hasil" => 'false',
                 "err_user" => form_error('UserName'),
                "err_pass" => form_error('password'),
                 "err_fullname" => form_error('fullname'),
                  "err_email" => form_error('email'),
                
            );
        }

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi edit ini akan mensetting nilai-nilai di form ketika mengklik tombol edit */

    function edit($id) {
        $row = $this->user01->getby_id($id)->row();
        $data['title'] = 'Edit - user01';
        $data['default']['nip'] = $row->nip;
        $data['default']['nama'] = $row->nama; 

        $data['url_post'] = site_url('userdata/editpost');
        $data['url_index'] = site_url('userdata'); 
        $data['id'] = $id; 
        $this->load->view('v_userdata/form', $data); 
    }

    /* fungsi untuk post data ketika melakukan edit data, fungsi ini akan masuk ke database */

    function editpost() {
        $this->form_validation->set_rules('nip', 'NIP', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');

        if ($this->form_validation->run() == TRUE) { 
            $id = $this->input->post('idpost');
            $nip = $this->input->post('nip');
            $nama = $this->input->post('nama');

            /* membuat record sebuah array, array ini akan masuk ke database */
            $record = array(
                "nip" => $nip,
                "nama" => $nama,
            );

            $this->user01->update($id, $record);

            /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
            $jsonmsg = array(
                "msg" => 'Update Data Success',
                "hasil" => 'true',
                "err_user" => null,
                "err_pass" => null,
            );
        } else {
            /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
            $jsonmsg = array(
                "msg" => 'Update Data Failed',
                "hasil" => 'false',
                "err_user" => form_error('nip'),
                "err_pass" => form_error('nama'),
            );
        }
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi untuk delete data */

    public function remove() {
        $id = $this->input->post('postuser');
        $this->user01->delete($id); 

        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

}
