<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Inputcust extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_inputcust', 'table01'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('Inputcust/grid'),
            "url_add" => site_url('Inputcust/addCust'),
            "url_edit" => site_url('Inputcust/edit'),
            "url_delete" => site_url('Inputcust/remove'),
        );
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='View Cust Table'; 
        $log_trans='CustTable'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("view", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->load->view('vm_inputcust/home', $data); 
        // $this->load->view('vm_inputcust/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        echo json_encode(array(
            "data" => $this->table01->getGridData()->result()
        ));
    }
    function editdetail($idCust) {
  
        $data = array(
            "base" => base_url(), 
            "url_griddetail" => site_url('Inputcust/griddetail'),
            "url_add" => site_url('Inputcust/addChassisdetil'),
            "url_delete" => site_url('Inputcust/remove'),
        );
        $data['id'] = $id; 
        $this->load->view('vm_inputcust/for', $data); 
    }
    public function griddetail() {
         $id = $this->input->post('id'); 
        echo json_encode(array(
            "data" => $this->table01->getGridDatadetail($id)->result()
        ));
    }
    function addCust() {

        $year=date("Yms");
        $data['title'] = 'Add - Input Customers';   
        $data['kdcust'] = $year;      
        $data['customer'] = '';     
        $resulchassis= $this->array->data_chassis(); 


        $data['url_post'] = site_url('Inputcust/addpost');  
        $data['url_getchassis'] = site_url('Inputcust/getchassis'); 
        $data['url_index'] = site_url('Inputcust'); 
        $data['id'] = 0; 

        $this->load->view('vm_inputcust/form', $data); 
    }
 
    public function addpost() { 

        $user = $this->session->userdata('ses_userName'); 
            $kdcust = $this->input->post('kdcust');  
            $customer = $this->input->post('customer'); 
            // var_dump($kdcust);
            // die();
            $record = array(
                "idCust" => $kdcust,
                "NmCust" => $customer, 
                "NmCust2" => $customer, 
                "crtBy" => $user,
                "updBy" => $user
            );
  
        $this->table01->insert($record); 
         $valid = 'true';
         $message = 'data insert';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Add Cust Table'; 
        $log_trans='Add'.$kdcust; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("add", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi edit ini akan mensetting nilai-nilai di form ketika mengklik tombol edit */

    function edit($id) {
        $row = $this->table01->getby_id($id)->row();   
        $data['kdcust'] = $row->idCust;      
        $data['customer'] = $row->NmCust;   

        $data['url_post'] = site_url('Inputcust/editpost');  
        $data['url_index'] = site_url('Inputcust'); 
        $data['id'] = $id; 
        $this->load->view('vm_inputcust/form', $data); 
    }

    /* fungsi untuk post data ketika melakukan edit data, fungsi ini akan masuk ke database */

    function editpost() {
     
        $user = $this->session->userdata('ses_userName'); 
            $kdcust = $this->input->post('kdcust');  
            $customer = $this->input->post('customer'); 
            // var_dump($periode);
            // die();

        $datetime=date("Y-m-d h:i:s");
           $record = array( 
                "NmCust" => $customer, 
                "NmCust2" => $customer, 
                "UpdDt" => $datetime, 
                "updBy" => $user 
            );
        $this->table01->update($kdcust,$record);   
         $valid = 'true';
         $message = 'data update';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Edit Cust Table'; 
        $log_trans='Edit'.$kdcust; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("edit", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi untuk delete data */

    public function remove() {
        $id = $this->input->post('id');
        $row = $this->table01->getby_id($id)->row();   
        $kdcust= $row->idCust;      
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Hapus Cust Table'; 
        $log_trans='Hapus'.$kdcust; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("hapus", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->table01->delete($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

}
