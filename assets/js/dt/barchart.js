var url2 = urlpost+'Dashdt/gridsalesqty';
 $(function () { 
        function graphbar(callback){ 
              
                $.ajax({
                    url: url2, 
                    dataType: 'json', 
                    type:'POST', 
                    data: {
                        periode: '2019', 
                    },
                    success: function(data){    
                        var datas=data.nilaissales;    
                        callback({databarqty:datas}); 
                    } 
                }); 
        } 
        graphbar(function(res){
            Array.prototype.contains = function(v) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === v) return true;
            }
            return false;
        };

        Array.prototype.unique = function() {
            var arr = [];
            for(var i = 0; i < this.length; i++) {
                if(!arr.includes(this[i])) {
                    arr.push(this[i]);
                }
            }
            return arr; 
        } 
            var list=res.databarqty; 
            // console.log(list);
            var periodebar=[];   
            var kategori=[];
            var groups = {}; 
            for (var i = 0; i < list.length; i++) {   
                  var periode =list[i].periodee;
                  var nmkatsize=list[i].katsize;
                  var groupName = list[i].katsize;
                        var listnilai=list[i].qty;
                        var nilai =Number(listnilai);
                 periodebar.push(periode);
                 kategori.push({name:nmkatsize}); 
                 if (!groups[groupName]) {
                            groups[groupName] = [];
                          }
                          groups[groupName].push(nilai);  
            }  
                        myArray = [];
                        for (var groupName in groups) { 
                          myArray.push({type: 'column',name: groupName, data: groups[groupName]});
                      //  console.log(groups[groupName]);
                        }     
                        myArray.push({
                            type: 'spline',
                            showInLegend: false,  
                            data: groups[groupName],
                            name: curvetrend,
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[3],
                                fillColor: 'white'
                            }
                        });
        var uniques = periodebar.unique(); 
        uniques.sort();  

                      //  console.log(myArray);
                Highcharts.chart('containerkat', {
                
                title: {
                    text: 'CATEGORY SIZE BASED ON QTY ',
                    enabled:false
                },
                exporting: {
                            enabled:false
                        },
                subtitle: {
                    text:null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: uniques 
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: QTY
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                   //alert('Category: ' + this.category + ', value: ' + this.series.name);  
                                  window.open ('index.php#Reportdash?category=category&tahun='+this.category+'&kat='+this.series.name+'&varian=none','_blank'); 
                                }
                            }
                        }
                    }
                },
                series:myArray
            });
        });    
})