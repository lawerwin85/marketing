<?php
class Mm_dashboard extends CI_Model {
    
    
    private $table;
    private $table02;
    private $table03;

    public function __construct() {
        parent::__construct();
        $this->table = "sales"; 

    }  
    function data_sales($periode){
        $query = "SELECT tahun,c.Nmcust,b.nmbarang,v.keterangan,v.varian,z.size,d.nmchassis,h.chassis,s.periode,SUM(s.qty)qty,SUM(s.UTotal)total FROM sales s
                    INNER JOIN customers c ON c.idCust=s.idCust
                    INNER JOIN barang b ON s.kdBarang=b.kdBarang
                    INNER JOIN varian v ON v.id=s.varian_id
                    INNER JOIN size z ON z.id=s.size_id
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    GROUP BY s.varian_id,tahun" ;
        return $this->db->query($query);
    }     
    // function data_salesqty(){
    //     $query = "SELECT  IFNULL(SUBSTRING_INDEX(tgl_sales, '-', 1) , 'TOTAL') AS periodee,
    //                 SUM( IF( kat_size = 1, qty, 0) ) AS Big,
    //                 SUM( IF( kat_size = 2, qty, 0) ) AS Small,
    //                 SUM( IF( kat_size = 3, qty, 0) ) AS Medium, 
    //                 SUM( qty ) AS total 
    //             FROM sales
    //             GROUP BY periodee" ;
    //     return $this->db->query($query);
    // }  
        function data_salesqtyperiode(){
        $query = "SELECT t.tahun periodee,sum(t.qty) qty FROM sales t 
                    inner join varian v on v.id=t.varian_id
                    where v.id < 10
                    group by t.tahun 
                    ORDER BY t.tahun ASC" ;
        return $this->db->query($query);
    }  
     function data_salesqtyperiodetotal(){
        $query = "SELECT t.tahun periodee,sum(t.qty) qty FROM sales t   
                    group by t.tahun 
                    ORDER BY t.tahun ASC" ;
        return $this->db->query($query);
    }  
        function data_salesqty(){
        $query = "SELECT t.tahun periodee,t.qtyTotalCategory-t.qtytotalwb-t.qtyTotalCT qty,s.keterangan katsize FROM transaksi_sales t
                    LEFT OUTER JOIN categorysize s ON s.id=t.id_category  
                    ORDER BY t.tahun,s.seqno ASC" ;
        return $this->db->query($query);
    }  
    function data_salesqtytipe(){
        $query = "SELECT t.tahun periodee,t.qtyTotalTipe qty,s.tipe katsize FROM transaksi_salestipe t
                    LEFT OUTER JOIN tipe s ON s.id=t.id_tipe
                    ORDER BY t.tahun,s.id ASC" ;
        return $this->db->query($query);
    }   
    function data_salesqtyVarian($datavarian){
        $query = "SELECT t.tahun periodee,$datavarian qty,s.keterangan katsize FROM transaksi_sales t
                    LEFT OUTER JOIN categorysize s ON s.id=t.id_category  
                    ORDER BY t.tahun,s.seqno  ASC" ;
        return $this->db->query($query);
    }  
 
    function data_chassisdetail(){
        $query = "SELECT c.chassis,s.tahun,SUM(s.qty)qty FROM chassis c
                        LEFT JOIN chassis_detail d ON d.idchassis=c.id
                        LEFT JOIN sales s ON s.chassis_id=d.id
                        GROUP BY c.chassis,s.tahun  ORDER BY s.tahun,c.chassis ASC" ;
                            return $this->db->query($query);
    }   
    function data_saleschassisup(){
        $query = " SELECT c.status,d.chassis,c.tahun,c.qty,c.totalV FROM tempdatachassis c
                    INNER JOIN chassis d ON d.id=c.idchassis 
                   GROUP BY d.chassis,c.tahun
                    ORDER BY c.tahun,c.qty ASC" ;
                            return $this->db->query($query);
    } 
    function data_salescustup($periode){
        $query = "SELECT c.status,d.Nmcust chassis,c.tahun,c.qty,c.totalV,d.idCust  FROM tempdatacust c
                    INNER JOIN customers d ON d.idCust=c.idCust 
                    where tahun=$periode
                   GROUP BY d.idCust,c.tahun
                    ORDER BY c.tahun,c.status,c.qty DESC" ;
                            return $this->db->query($query);
    } 
    function data_katsalesCustup($periode,$kat){
        $query = "SELECT d.Nmcust chassis,IFNULL(c.tahun, $periode) tahun,IFNULL(SUM(c.qty), 0) qty,IFNULL(SUM(c.UTotal)/1000, 0) totalV,d.idCust  FROM customers d
                    LEFT JOIN sales c ON d.idCust=c.idCust and tahun=$periode AND c.kategori_id='$kat'
                   GROUP BY d.idCust,c.tahun
                    ORDER BY c.tahun,SUM(c.qty) DESC" ;
                            return $this->db->query($query);
    } 

    function data_saleschassis(){
        $query = " SELECT d.seqno,c.status,d.chassis,c.tahun,c.qty,c.totalV FROM tempdatachassis c
                    INNER JOIN chassis d ON d.id=c.idchassis 
                    WHERE d.seqno >0
                   GROUP BY d.chassis,c.tahun
                    ORDER BY c.tahun,d.seqno,c.qty ASC" ;
                            return $this->db->query($query);
    } 

    function data_katsalesCust($periode,$kat){
        $query = "SELECT d.Nmcust chassis,IFNULL(s.tahun, $periode) tahun,IFNULL(SUM(s.qty), 0) qty,IFNULL(SUM(s.UTotal)/1000, 0) totalV,d.idCust  FROM customers d 
                    LEFT JOIN sales s ON d.idCust=s.idCust and s.tahun=$periode AND s.kategori_id='$kat'
                   GROUP BY d.idCust,s.tahun
                    ORDER BY SUM(s.qty) DESC LIMIT 15" ;
                            return $this->db->query($query);
    } 

    function data_salescust($periode){
        $query = "SELECT c.status,d.Nmcust chassis,c.tahun,c.qty,c.totalV,d.idCust  FROM tempdatacust c
                    INNER JOIN customers d ON d.idCust=c.idCust
                    where tahun=$periode
                   GROUP BY d.idCust,c.tahun
                    ORDER BY c.qty DESC limit 15" ;
                            return $this->db->query($query);
    } 


    function data_salescustupfull(){
        $query = "SELECT d.Nmcust chassis, SUM(c.qty)qty FROM sales c
                    INNER JOIN customers d ON d.idCust=c.idCust  
                   GROUP BY d.idCust 
                    ORDER BY SUM(c.qty) DESC " ;
                            return $this->db->query($query);
    } 
    function data_salescustful(){
        $query = "SELECT d.Nmcust chassis, SUM(c.qty)qty,d.idCust  FROM sales c
                    INNER JOIN customers d ON d.idCust=c.idCust  
                   GROUP BY d.idCust 
                    ORDER BY SUM(c.qty) DESC LIMIT 15" ;
                            return $this->db->query($query);
    } 

    function droptemp(){
        $query = "DELETE from tempdatachassis" ;
            return $this->db->query($query);
    } 


    function checkData(){
        $query = "SELECT * FROM tempdatachassis" ;
        return $this->db->query($query)->num_rows();  
      
    }
   
    function data_chassisyear($tahun,$limit){
        $query = "INSERT INTO tempdatachassis(tahun,idchassis,qty,totalV,STATUS) SELECT * from transaksi_chassis where tahun=$tahun order by qty DESC limit $limit" ;
        $query2 = "INSERT INTO tempdatacust(tahun,idCust,qty,totalV,STATUS) SELECT * from transaksi_cust where tahun=$tahun order by qty DESC limit 15 " ;
        $this->db->query($query2);
                            return $this->db->query($query);
    } 

    function data_chassisyearblank($tahun,$blank){$query = "DELETE from tempdatachassis" ;
        $query = "INSERT into tempdatachassis(tahun,idchassis,qty,totalV,STATUS) SELECT tahun,idchassis,0,0,0  from transaksi_chassis where tahun=$tahun order by qty ASC limit $blank" ;
        $query2 = "INSERT into tempdatacust(tahun,idCust,qty,totalV,STATUS) SELECT tahun,idCust,0,0,0  from transaksi_cust where tahun=$tahun order by qty ASC limit 7" ;
       $this->db->query($query2);
                            return $this->db->query($query);
    } 
    // function data_saleschassis(){
    //     $query = "SELECT t.tahun,SUM(s.qty) qty FROM m_tahun t
    //                 LEFT JOIN sales s ON t.tahun=s.tahun 
    //                 LEFT JOIN chassis_detail d ON s.chassis_id=d.id
    //                 LEFT JOIN chassis c ON d.idchassis=c.id 
    //                 GROUP BY c.chassis,t.tahun 
    //                 ORDER BY t.tahun,c.chassis ASC" ;
    //                         return $this->db->query($query);
    // }
    function data_salesqtyVarianfull(){
        $query = " SELECT t.tahun, CASE varian_id 
                     WHEN 1 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ1,
                     CASE varian_id 
                     WHEN 2 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ2,
                     CASE varian_id 
                     WHEN 3 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ3,
                     CASE varian_id 
                     WHEN 4 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ4,
                     CASE varian_id 
                     WHEN 5 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ5,
                     CASE varian_id 
                     WHEN 7 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ7,
                     CASE varian_id 
                     WHEN 8 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQ8,
                     CASE varian_id 
                     WHEN 9 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalQA,
                     CASE varian_id 
                     WHEN 10 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalCT,
                     CASE varian_id 
                     WHEN 11 THEN SUM(qty)
                     ELSE 0 END AS qtyTotalwb FROM m_tahun t
                    LEFT OUTER JOIN sales s ON s.tahun=t.tahun
                    GROUP BY t.tahun,varian_id
                     ORDER BY tahun ASC" ;
        return $this->db->query($query);
    }  
      // function data_salesqty(){
      //   $query = "SELECT s.kat_size as katsize,COALESCE(c.keterangan, '0') ket,SUM(s.qty) qty,s.tahun periodee FROM categorysize c
      //               LEFT JOIN sales s ON c.id=s.kat_size
      //               GROUP BY size
      //               ORDER BY kat_size,tahun ASC" ;
      //     return $this->db->query($query);
      //   }
        function data_salescategory(){
        $query = "SELECT id,keterangan FROM categorysize ORDER BY seqno  ASC" ;
          return $this->db->query($query);
        }  
    function getendingyear() {
        $query = " SELECT tahun FROM m_tahun ORDER BY tahun ASC LIMIT 1";
        return $this->db->query($query);   
    }

    function getcountchassis() {
        $query = " SELECT count(chassis) chassis FROM chassis";
        return $this->db->query($query);   
    }

    function chassis(){
        $query = "SELECT h.chassis,SUM(s.qty) qty,SUM(s.Utotal) amount FROM sales s
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    GROUP BY h.chassis" ;
                            return $this->db->query($query);
    } 

    function chassisdetail(){
        $query = "SELECT h.chassis,d.nmchassis,SUM(s.qty) qty,SUM(s.Utotal) amount FROM sales s
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    GROUP BY d.nmchassis" ;
                            return $this->db->query($query);
    } 
                  
    function chassisdetailkategori(){
        $query = "SELECT h.chassis ,d.nmchassis ff ,k.kategori nmchassis,SUM(s.qty) qty,SUM(s.Utotal) amount FROM sales s
                    INNER JOIN kategori k ON k.id=s.kategori_id 
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    GROUP BY k.id,h.chassis
                    ORDER BY k.seqno ASC" ;
                            return $this->db->query($query);
    } 
}
