<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Inputbarang extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_Inputbarang', 'table01'); 
        $this->load->model('Mm_Inputsales', 'table02'); 
        $this->load->model('Mm_inputchassis', 'table03'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('Inputbarang/grid'),
            "url_add" => site_url('Inputbarang/addBarang'),
            "url_edit" => site_url('Inputbarang/edit'),
            "url_delete" => site_url('Inputbarang/remove'),
        );
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='View barang Table'; 
        $log_trans='BarangTable'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("view", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->load->view('vm_inputbarang/home', $data); 
        // $this->load->view('vm_inputbarang/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        echo json_encode(array(
            "data" => $this->table01->getGridData()->result()
        ));
    }  
    function addBarang() {
        $data['title'] = 'Add - Input Produk'; 
        $data['tanggal'] = ''; 
        $data['nmchassisdetail'] = ''; 
        $data['chassisdetail'] = ''; 
        $data['namaUnitshow'] = ''; 
        $data['namaUnit'] = '';  
        $data['field'] = '';     
        $data['readset']='';   
        $data['disabled']=''; 
        $resulunit = $this->array->data_barang();
        $e = 0;
        foreach ($resulunit as $rowunit) {              
            $data['default']['namaUnit'][$e]['value'] = $rowunit['kdBarang'];
            $data['default']['namaUnit'][$e]['display'] = $rowunit['nmBarang']; 
            $e++;
        } 
        $resulvarian = $this->array->data_varian();
        $e = 0;
        foreach ($resulvarian as $rowvarian) {              
            $data['default']['varian'][$e]['value'] = $rowvarian['id'];
            $data['default']['varian'][$e]['display'] = $rowvarian['varian']; 
            $e++;
        }  
        $resulsize = $this->array->data_size();
        $e = 0;
        foreach ($resulsize as $rowsize) {              
            $data['default']['size'][$e]['value'] = $rowsize['id'];
            $data['default']['size'][$e]['display'] = $rowsize['size']; 
            $e++;
        } 
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis']; 
            $e++;
        } 
        $resulchassisdetail= $this->array->data_chassisdtl();
        $e = 0;
        foreach ($resulchassisdetail as $rowchassisdtl) {              
            $data['default']['chassisdetail'][$e]['value'] = $rowchassisdtl['id'];
            $data['default']['chassisdetail'][$e]['display'] = $rowchassisdtl['nmChassis']; 
            $e++;
        }
        $resulkategori = $this->array->data_kategori();
        $e = 0;
        foreach ($resulkategori as $rowkategori) {              
            $data['default']['kategori'][$e]['value'] = $rowkategori['id'];
            $data['default']['kategori'][$e]['display'] = $rowkategori['kategori']; 
            $e++;
        } 
        $resultipe= $this->array->data_tipe();
        $e = 0;
        foreach ($resultipe as $rowtipe) {              
            $data['default']['tipe'][$e]['value'] = $rowtipe['id'];
            $data['default']['tipe'][$e]['display'] = $rowtipe['tipe'];  
            $e++;
        } 
        $resulkategorisize = $this->array->data_kategorisize();
        $e = 0;
        foreach ($resulkategorisize as $rowkategorisize) {              
            $data['default']['ckatageri'][$e]['value'] = $rowkategorisize['id'];
            $data['default']['ckatageri'][$e]['display'] = $rowkategorisize['keterangan']; 
            $e++;
        }   
        $data['url_post'] = site_url('Inputbarang/addpost');   
        $data['url_gridchassis'] = site_url('Inputsales/gridchassis');  
        $data['url_getchassis'] = site_url('Inputsales/getchassis');
        $data['url_gridbarang'] = site_url('Inputsales/gridbarang');  
        $data['url_getbarang'] = site_url('Inputsales/getbarang'); 
        $data['url_index'] = site_url('Inputbarang'); 
        $data['id'] = 0; 

        $this->load->view('vm_inputbarang/form', $data); 
    }    
    public function addpost() { 

            $user = $this->session->userdata('ses_userName'); 
            $kdBarang=date("YmdHs"); 
           // $namaUnit = $this->input->post('namaUnitshow');  
            $kategori = $this->input->post('kategori');   
            $chassisdetail = $this->input->post('chassisdetail'); 
            $varian = $this->input->post('varian');  
            $size = $this->input->post('size');
            $ckatageri = $this->input->post('ckatageri'); 
            $tipe = $this->input->post('tipe');  
            $row = $this->table03->getGridDatachasiis($chassisdetail)->row();   
            $row2 = $this->array->getGridDataQr($varian)->row();   
            $row3 = $this->array->getGridDataSize($size)->row();   
            $g=$row->chassis; 
            $gg=$row->nmChassis; 
            $var=$row2->varian; 
            $sz=$row3->size;  
            $spasi=" "; 
            $namaUnit=$g.$spasi.$gg.$spasi.$var.$spasi.$sz;
            // var_dump();
            // die();
            $record = array( 
                "kdBarang" => $kdBarang, 
                "nmBarang" => $namaUnit, 
                "kategori_id" => $kategori,
                "crtBy" => $user,
                "updBy" => $user,
                "chassis_d" => $chassisdetail,
                "varian_id" => $varian,  
                "size_id" => $size, 
                "kat_size" => $ckatageri,
                "id_tipe" => $tipe,   
                "status" =>0
            ); 
        $this->table01->insert($record);  
         $valid = 'true';
         $message = 'data insert';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Add barang Table'; 
        $log_trans='Add'.$kdBarang; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("add", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg); 
    }

    /* fungsi edit ini akan mensetting nilai-nilai di form ketika mengklik tombol edit */

    function edit($id) {
        $row = $this->table01->getby_id($id)->row();     
        $data['nmchassisdetail'] =$row->nmChassis;  
        $data['chassisdetail'] =$row->chassis_d;  
        //$data['namaUnitshow'] =$row->nmBarang;  
        $data['namaUnit'] =$row->kdBarang;        
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis']; 
            if ($row->chassis_h == $rowchassis['id']) {
                $data['default']['chassis'][$e]['selected'] = "SELECTED";
            }
            $e++;
        }  
        $resultipe= $this->array->data_tipe();
        $e = 0;
        foreach ($resultipe as $rowtipe) {              
            $data['default']['tipe'][$e]['value'] = $rowtipe['id'];
            $data['default']['tipe'][$e]['display'] = $rowtipe['tipe']; 
            if ($row->id_tipe == $rowtipe['id']) {
                $data['default']['tipe'][$e]['selected'] = "SELECTED";
            }
            $e++;
        } 
        $resulvarian = $this->array->data_varian();
        $e = 0;
        foreach ($resulvarian as $rowvarian) {              
            $data['default']['varian'][$e]['value'] = $rowvarian['id'];
            $data['default']['varian'][$e]['display'] = $rowvarian['varian'];
            if ($row->varian_id == $rowvarian['id']) {
                $data['default']['varian'][$e]['selected'] = "SELECTED";
            } 
            $e++;
        }  
        $resulsize = $this->array->data_size();
        $e = 0;
        foreach ($resulsize as $rowsize) {              
            $data['default']['size'][$e]['value'] = $rowsize['id'];
            $data['default']['size'][$e]['display'] = $rowsize['size'];
            if ($row->size_id == $rowsize['id']) {
                $data['default']['size'][$e]['selected'] = "SELECTED";
            }
            $e++;
        } 
        $resulkategori = $this->array->data_kategori();
        $e = 0;
        foreach ($resulkategori as $rowkategori) {              
            $data['default']['kategori'][$e]['value'] = $rowkategori['id'];
            $data['default']['kategori'][$e]['display'] = $rowkategori['kategori']; 
            if ($row->kategori_id == $rowkategori['id']) {
                $data['default']['kategori'][$e]['selected'] = "SELECTED";
            }
            $e++;
        } 
        $resulkategorisize = $this->array->data_kategorisize();
        $e = 0;
        foreach ($resulkategorisize as $rowkategorisize) {              
            $data['default']['ckatageri'][$e]['value'] = $rowkategorisize['id'];
            $data['default']['ckatageri'][$e]['display'] = $rowkategorisize['keterangan']; 
            if ($row->kat_size == $rowkategorisize['id']) {
                $data['default']['ckatageri'][$e]['selected'] = "SELECTED";
            }
            $e++;
        }   
        $data['url_post'] = site_url('Inputbarang/editpost');  
        $data['url_gridchassis'] = site_url('Inputsales/gridchassis');  
        $data['url_getchassis'] = site_url('Inputsales/getchassis');
        $data['url_gridbarang'] = site_url('Inputsales/gridbarang');  
        $data['url_getbarang'] = site_url('Inputsales/getbarang');  
        $data['url_index'] = site_url('Inputbarang'); 
        $data['id'] = $id; 
        $this->load->view('vm_inputbarang/form', $data); 
    }

    public function gridchassis() {
         $id = $this->input->post('id'); 
        echo json_encode(array(
            "data" => $this->table02->getGridDatachassis($id)->result()
        ));
    } 

    function editpost() {
        $user = $this->session->userdata('ses_userName'); 
        $id = $this->input->post('id');
         $kdBarang=$this->input->post('namaUnit');  
           // $namaUnit = $this->input->post('namaUnitshow');  
            $kategori = $this->input->post('kategori');   
            $chassisdetail = $this->input->post('chassisdetail'); 
            $varian = $this->input->post('varian');  
            $size = $this->input->post('size');
            $ckatageri = $this->input->post('ckatageri'); 
            $tipe = $this->input->post('tipe');  
            $row = $this->table03->getGridDatachasiis($chassisdetail)->row();   
            $row2 = $this->array->getGridDataQr($varian)->row();   
            $row3 = $this->array->getGridDataSize($size)->row();   
            $g=$row->chassis; 
            $gg=$row->nmChassis; 
            $var=$row2->varian; 
            $sz=$row3->size;  
            $spasi=" "; 
            $namaUnit=$g.$spasi.$gg.$spasi.$var.$spasi.$sz;
            // var_dump($kdBarang);
            // die();
            $datetime=date("Y-m-d h:i:s");
            $record = array( 
                "kdBarang" => $kdBarang, 
                "nmBarang" => $namaUnit, 
                "kategori_id" => $kategori, 
                "updBy" => $user,
                "chassis_d" => $chassisdetail,
                "varian_id" => $varian,  
                "size_id" => $size, 
                "kat_size" => $ckatageri,
                "id_tipe" => $tipe,    
                "UpdDt" => $datetime, 
                "status" =>0
            ); 
            
        $this->table01->update($id,$record);   
         $valid = 'true';
         $message = 'data update';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Edit barang Table'; 
        $log_trans='edit'.$kdBarang; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("edit", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi untuk delete data */

    public function remove() {
        $id = $this->input->post('id');
        $row = $this->table01->getby_id($id)->row();     
        $kdBarang =$row->kdBarang;  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Hapus barang Table'; 
        $log_trans='Hapus'.$kdBarang; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("hapus", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->table01->delete($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

}
