<?php

/* membuat class dengan nama Magama_model*/
class Mm_inputchassis extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "chassis_detail"; 

    }

     /* mendapatkan semua data dan hasilnya sebuah array */
    public function getAll() {
        return $this->db->get($this->table)->result_array();
    }
 
    function getGridData() {
        $query = "SELECT c.id,c.chassis,COUNT(d.id) total FROM chassis c
                    LEFT OUTER JOIN chassis_detail d ON d.idchassis=c.id
                    GROUP BY c.chassis";
        return $this->db->query($query);      
    }
    function getGridDatachasiis($chassisdetail) {
        $query = "SELECT d.id,d.nmChassis,c.* FROM chassis c
                    INNER JOIN chassis_detail d ON d.idchassis=c.id
                    where d.id='$chassisdetail'";
        return $this->db->query($query);      
    }
    function getGridDatadetail($id) {
        $query = "SELECT d.id,d.nmChassis FROM chassis c
                    INNER JOIN chassis_detail d ON d.idchassis=c.id
                    where d.idchassis='$id'";
        return $this->db->query($query);      
    }

    function getby_id($id) {
        $query = "SELECT * from chassis_detail where id='$id'";
        return $this->db->query($query);   
    }
    function insert($record) {
        $this->db->insert($this->table, $record);
    } 
    function update($id, $record) {
        $this->db->where("id", $id);
        $this->db->update($this->table, $record);
    }  
    function delete($id) {
        $this->db->delete($this->table, array("id" => $id)
        );
    }

}
