<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Inputsize extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_size', 'table01'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('Inputsize/grid'),
            "url_add" => site_url('Inputsize/addSize'),
            "url_edit" => site_url('Inputsize/edit'),
            "url_delete" => site_url('Inputsize/remove'),
        );
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='View Size Table'; 
        $log_trans='SizeTable'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("view", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->load->view('vm_size/home', $data); 
        // $this->load->view('vm_Inputsize/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        echo json_encode(array(
            "data" => $this->table01->getGridData()->result()
        ));
    } 
    public function griddetail() {
         $id = $this->input->post('id'); 
        echo json_encode(array(
            "data" => $this->table01->getGridDatadetail($id)->result()
        ));
    }
    function addSize() { 
        $data['title'] = 'Add - Input Size';   
        $data['size'] = '';            


        $data['url_post'] = site_url('Inputsize/addpost');   
        $data['url_index'] = site_url('Inputsize'); 
        $data['id'] = 0; 
        $this->load->view('vm_size/form', $data); 
    }
 
    public function addpost() { 
        $user = $this->session->userdata('ses_userName'); 
            $size = $this->input->post('size');    
            // var_dump($kdcust);
            // die();
            $record = array(
                "size" => $size,  
                "crtBy" => $user,
                "updBy" => $user 
            );
  
        $this->table01->insert($record); 
         $valid = 'true';
         $message = 'data insert';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  

        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Add Size Table'; 
        $log_trans='Add'.$size; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("add", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi edit ini akan mensetting nilai-nilai di form ketika mengklik tombol edit */

    function edit($id) {
        $row = $this->table01->getby_id($id)->row();   
        $data['size'] = $row->size;         

        $data['url_post'] = site_url('Inputsize/editpost');  
        $data['url_index'] = site_url('Inputsize'); 
        $data['id'] = $id; 
        $this->load->view('vm_size/form', $data); 
    }

    /* fungsi untuk post data ketika melakukan edit data, fungsi ini akan masuk ke database */

    function editpost() {
     
        $user = $this->session->userdata('ses_userName'); 
            $size = $this->input->post('size');  
            $id = $this->input->post('id'); 
            // var_dump($periode);
            // die();

           $datetime=date("Y-m-d h:i:s");
           $record = array( 
                "size" => $size,
                "UpdDt" => $datetime,
                "updBy" => $user 
            );
        $this->table01->update($id,$record);   
         $valid = 'true';
         $message = 'data update';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Edit Size Table'; 
        $log_trans='Edit'.$size; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("edit", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi untuk delete data */

    public function remove() {
        $id = $this->input->post('id');
        $row = $this->table01->getby_id($id)->row();   
        $datasize=$row->size;
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Hapus Size Table'; 
        $log_trans='Hapus'.$datasize; 
        $log_id=$ses_loginId.$log_trans;
        helper_log("hapus", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);

        $this->table01->delete($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

}
