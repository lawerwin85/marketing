<div class="container">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2>  Dashboard</h2> 
            </div>            
            <div class="col-lg-6 col-md-4 col-sm-12 text-right">
                
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card"> 
                <div class="body">                            
                   <div id="containerclick" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>            
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card"> 
                <div class="body">                            
                   <div id="containerclickkategori" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>            
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card">   
                <div class="body"> 
                    <div class="col-lg-2 col-md-2" style="text-align: right">  
                        <select id="tahun" class="form-control tahun" name="tahun"> 
                            <option selected="selected">-- Pilih Tahun --</option>
                           <?php print_r($default['tahun']); foreach ($default['tahun'] as $row) { ?>
                                    
                                    <option data-id="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                            <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                        <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                <?php } ?>
                        </select>  
                    </div>                           
                   <div id="containerFullbuyer" style="min-width: 310px; height: 800px; margin: 0 auto"></div>
                </div>
            </div>
        </div>            
    </div> 
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card">   
                <div class="body"> 
                <div class="col-lg-2 col-md-2" style="text-align: right">  
                </div>                           
                   <div id="containerFullbuyerperiode" style="min-width: 310px; height: 800px; margin: 0 auto"></div>
                </div>
            </div>
        </div>            
    </div> 

 
    </div>
</div>  
<script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"> </script> 
<script src="<?php echo $base; ?>assets/js/product/index.js"></script> 
<script src="<?php echo $base; ?>assets/js/product/barclick.js"></script>
<script src="<?php echo $base; ?>assets/js/product/barclickkategori.js"></script>
<script src="<?php echo $base; ?>assets/js/barchartbuyer.js"></script>
<script src="<?php echo $base; ?>assets/js/barchartbuyerperiode.js"></script> 