 <div class="container">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2> Ganti Password User</h2> 
                    </div>        
                </div>
            </div>
        <form id="basic-form" autocomplete="off" method="post" novalidate>
            <div class="row clearfix"> 
                <div class="col-lg-6 col-md-6">
                    <div class="card"> 
                                <div class="body">  
                                    <div class="form-group">
                                        <label>Password Lama</label> 
                                            <input id="passwordlama" type="password" class="form-control" aria-describedby="basic-addon2"> 
                                        </div> 
                                    <div class="form-group">
                                        <label>Password Baru</label> 
                                        <div class="input-group mb-3">
                                            <input id="passwordbaru" type="password" class="form-control" aria-describedby="basic-addon2"> 
                                            </div>
                                    </div>    
                                </div> 
                                <button type="button" id="submit" class="btn btn-primary">Update</button>
                                <button type="reset" id="cancel" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div> 
            </div>
        </form>
            
        </div>  
 <script type="text/javascript">   
  
    $(document).ready(function ()
    {  
        var buttonsave, buttoncancel, urlpost, urlindex, content;

        buttonsave = $('#submit');
        buttoncancel = $('#cancel');
        urlpost = '<?php echo $url_post; ?>';
        urlindex = '<?php echo $url_index; ?>';
        content = $("#contentdata");
  

        buttonsave.click(
                function ()
                {

                    $.ajax(
                            {
                                type: "POST",
                                url: urlpost,
                                dataType: "json",
                                data: {
                                    id: $("#id").val(),
                                    passwordlama: $("#passwordlama").val(),
                                    passwordbaru: $("#passwordbaru").val(), 
                                },
                                cache: false,
                                success:
                                        function (data, text)
                                        {
                                            if (data.hasil == true) {
                                               // alert(data.msg);
                                                content.fadeOut("slow", "linear");
                                                window.top.location.href = 'index.php#Dash'; 
                                                content.fadeIn("slow");
                                            } else {
                                                alert(data.msg);

                                            }
                                        },
                                error: function (request, status, error) {
                                    alert(request.responseText + " " + status + " " + error);
                                }
                            });
                    return false;

                });

        buttoncancel.click(
                function ()
                {
                    content.fadeOut("slow", "linear");
                    window.top.location.href = 'index.php#Dash'; 
                    content.fadeIn("slow");

                }); 

    }); 
</script> 