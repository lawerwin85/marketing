<div class="row">
    <div class="col-md-4 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> Setting Group</span>
                </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-default btn-sm" onclick="mdl.add_group();">
                        <i class="fa fa-plus"></i> Add </a>
                    <a href="javascript:;" class="btn btn-default btn-sm" onclick="mdl.edit_group();">
                        <i class="fa fa-pencil"></i> Edit </a>
                    <a href="javascript:;" class="btn btn-default btn-sm" onclick="mdl.remove_group();">
                        <i class="fa fa-trash"></i> Delete </a>
                </div>
            </div>

            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group">

                            <div class="list-group" id="group">
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>

    <div class="col-md-8 ">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> Setting Module</span>
                </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-default btn-sm" onclick="mdl.add_module();">
                        <i class="fa fa-plus"></i> Add </a>
                </div>
            </div>

            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="row " id="sub_modul">

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-lg" id="submenu" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
<!--                    <h4 class="modal-title">Modal Title</h4>-->
                </div>
                <div class="modal-body">
                    <div class="portlet box blue ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Customize Submenu </div>
                        </div>
                        <div class="portlet-body form">
                            <form role="form">
                                <div class="form-body" id="submenu_body">

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" onclick="mdl.cancelCustomize();">Close</button>
                    <button type="button" class="btn green" onclick="mdl.saveCustomize();">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>
<script src="<?php echo $base; ?>assets/script/modul.js"></script>
<script type="text/javascript">
    var base_url = '<?php echo $base ?>';
    var url_grid = '<?php echo $url_grid ?>';
    var url_group = '<?php echo $url_group ?>';
    var url_update = '<?php echo $url_update ?>';
    var url_add = '<?php echo $url_add ?>';
    var url_edit = '<?php echo $url_edit ?>';
    var url_remove = '<?php echo $url_remove ?>';
    var url_getchild = '<?php echo $url_getchild ?>';
    var url_upd_menu = '<?php echo $url_upd_menu ?>';
    var url_del_module = '<?php echo $url_del_module ?>';
    var url_add_module = '<?php echo $url_add_module ?>';
    $(document).ready(function () {

        mdl.init();
        try{
            mdl.modul_change();
        }catch(e){console.log(e)}
    });

</script>