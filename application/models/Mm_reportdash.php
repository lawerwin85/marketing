<?php

/* membuat class dengan nama Magama_model*/
class Mm_reportdash extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "barang"; 

    }

     /* mendapatkan semua data dan hasilnya sebuah array */
    public function getAll() {
        return $this->db->get($this->table)->result_array();
    }
 
    function getGridData($category,$tahun,$kat,$varian) {
        if($category=='category'){

        $query = "SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.keterangan,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN categorysize c ON c.id=s.kat_size
                    WHERE tahun='$tahun' AND c.keterangan='$kat' order by s.tgl_sales ASC";
        }
        elseif($category=='tipe'){
            $query = "SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.tipe,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN tipe c ON c.id=s.id_tipe
                    WHERE tahun='$tahun' AND c.tipe='$kat' order by s.tgl_sales ASC"; 
        }
        elseif($category=='QR'){
            $query = " SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.keterangan,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN categorysize c ON c.id=s.kat_size
                    INNER JOIN varian k ON k.id=s.varian_id
                    WHERE tahun='$tahun' AND c.keterangan='$kat' AND k.varian='$varian' ORDER BY s.tgl_sales ASC"; 
        }
        elseif($category=='chassis'){
            $query = " SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    LEFT JOIN barang b ON b.kdBarang=s.kdBarang
                    LEFT JOIN chassis_detail d ON d.id=s.chassis_id
                    LEFT JOIN chassis h ON h.id=d.idchassis 
                    WHERE tahun='$tahun' AND h.chassis='$kat' ORDER BY s.tgl_sales ASC"; 
        }
        elseif($category=='overall'){
            $query = "SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.tipe,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN tipe c ON c.id=s.id_tipe
                    WHERE tahun='$tahun' order by s.tgl_sales ASC"; 
        }
        elseif($category=='cust'){
            $query = "SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.tipe,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN customers f on f.idCust=s.idCust
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN tipe c ON c.id=s.id_tipe
                    WHERE tahun='$tahun' and f.NmCust like '%$kat%' order by s.tgl_sales ASC"; 
        }
        elseif($category=='custall'){
            $query = "SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.tipe,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN customers f on f.idCust=s.idCust
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN tipe c ON c.id=s.id_tipe
                    WHERE  f.NmCust like '%$kat%' order by s.tgl_sales ASC"; 
        }
        elseif($category=='group'){
             $query = "SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.tipe,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN customers f on f.idCust=s.idCust
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN tipe c ON c.id=s.id_tipe
                    WHERE tahun='$tahun' and s.kategori_id='$kat' order by s.tgl_sales ASC"; 
        }
        elseif($category=='Small'){
             $query = "SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.tipe,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN customers f on f.idCust=s.idCust
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN tipe c ON c.id=s.id_tipe
                    WHERE tahun='$tahun' and s.kategori_id='$kat' and s.group_v='$varian' and s.kat_size=2 order by s.tgl_sales ASC"; 
        }
        elseif($category=='Big'){
             $query = "SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.tipe,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN customers f on f.idCust=s.idCust
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN tipe c ON c.id=s.id_tipe
                    WHERE tahun='$tahun' and s.kategori_id='$kat' and s.group_v='$varian' and s.kat_size=1 order by s.tgl_sales ASC"; 
        }
        elseif($category=='Medium'){
             $query = "SELECT b.nmBarang,d.nmChassis,h.chassis,s.*,c.tipe,CONCAT( FORMAT(s.UTotal, 2)) total,CONCAT( FORMAT(s.unit_price, 2)) price FROM sales s 
                    INNER JOIN barang b ON b.kdBarang=s.kdBarang
                    INNER JOIN customers f on f.idCust=s.idCust
                    INNER JOIN chassis_detail d ON d.id=s.chassis_id
                    INNER JOIN chassis h ON h.id=d.idchassis
                    INNER JOIN tipe c ON c.id=s.id_tipe
                    WHERE tahun='$tahun' and s.kategori_id='$kat' and s.group_v='$varian' and s.kat_size=3 order by s.tgl_sales ASC"; 
        }
        return $this->db->query($query);      
    }
      
    function insert($record) {
        $this->db->insert($this->table, $record);
    } 
    function update($id, $record) {
        $this->db->where("id", $id);
        $this->db->update($this->table, $record);
    }  
    function delete($id) {
        $query = "UPDATE barang set status=2 where id='$id'";
        return $this->db->query($query);  
    }

}
