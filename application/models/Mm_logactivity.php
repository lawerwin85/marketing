<?php

/* membuat class dengan nama Magama_model*/
class Mm_logactivity extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "users"; 
        $this->table2 = "user_group_users"; 

    } 
  
    function getGridData($user) {
        $query = "SELECT l.*,t.desc tipe FROM _log l
                    inner join _logtipe t on t.id=l.log_tipe
                    WHERE l.log_user='$user'
                    GROUP BY l.log_id
                    ORDER BY l.log_time ASC";
        return $this->db->query($query);      
    } 

}
