<?php
class Ms_dashboard extends CI_Model {
    
    
    private $table;
    private $table1;

    public function __construct() {
        parent::__construct();
        $this->table = "m_app";
        $this->table1 = "user_group_users";
    }
    
    public function getMenu($param, $param1, $param2) {
        $q = "
            SELECT a.user_group_id,c.seq_no, a.app_id, c.appName, c.url, c.icon, a.allowAdd, a.allowUpdate, a.allowDelete,
                b.users_id,
                c.parent_id
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.parent_id='$param'
            AND a.user_group_id = '$param1'
            AND b.users_id = '$param2' ";
        if($param==100){
            $q .= "ORDER BY c.seq_no asc";
        }
        return $this->db->query($q)->result_array();
    }
     public function getonlyMenu($param, $param1, $param2) {
        $q = "
            SELECT c.url
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.parent_id='$param'
            AND a.user_group_id = '$param1'
            AND b.users_id = '$param2' ORDER by seq_no ASC limit 1"; 
        return $this->db->query($q);
    }
     public function getonlynumrows($param, $param1, $param2) {
        $q = "
            SELECT c.url
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.parent_id='$param'
            AND a.user_group_id = '$param1'
            AND b.users_id = '$param2' ORDER by seq_no ASC"; 
        return $this->db->query($q)->num_rows();
    }
    public function getMenu2($param, $param1, $param2) {
        $q = "
            SELECT a.user_group_id, a.app_id, c.appName, c.url, c.icon, a.allowAdd, a.allowUpdate, a.allowDelete,
                b.users_id,
                c.parent_id
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.parent_id='$param'
            AND a.user_group_id = '$param1'
            AND b.users_id = '$param2' ORDER by seq_no ASC"; 
        return $this->db->query($q)->result_array();
    }
     public function getMenu3($param, $param1, $param2) {
        $q = "
            SELECT a.user_group_id, a.app_id, c.appName, c.url, c.icon, a.allowAdd, a.allowUpdate, a.allowDelete,
                b.users_id,
                c.parent_id
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.parent_id='$param'
            AND a.user_group_id = '$param1'
            AND b.users_id = '$param2'"; 
        return $this->db->query($q)->num_rows();
    }
    public function getMenuApp($idpost, $id) {
        $q = "
            SELECT c.id
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.url='$idpost' and c.parent_id=100  
            AND b.users_id = '$id'"; 
        return $this->db->query($q);
    }
    public function getMenuApp2($idpost, $id) {
        $q = "
            SELECT c.id,c.parent_id
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.url='$idpost' and c.parent_id !=100  
            AND b.users_id = '$id'"; 
        return $this->db->query($q);
    }
    public function getapnumrow($idpost, $id) {
        $q = "
            SELECT c.id,c.parent_id
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.url='$idpost' and c.parent_id !=100  
            AND b.users_id = '$id'"; 
        return $this->db->query($q)->num_rows();
    }
    function getUserGroupId($id) {
        $q = "
            select * from user_group_users where users_id='$id'";
        return $this->db->query($q)->result_array();
    } 
    function getUserGroupId2($id) {
        $this->db->where("users_id", $id);
        return $this->db->get($this->table1);
    }
    function checkData($idpost,$id){
        $q = "
            SELECT c.id
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.url='$idpost' and c.parent_id=100  
            AND b.users_id = '$id'"; 
        return $this->db->query($q)->num_rows();  
    }
     function checkData2($idpost,$id){
        $q = "
            SELECT c.id
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.url='$idpost' and c.parent_id!=100  
            AND b.users_id = '$id'"; 
        return $this->db->query($q)->num_rows();  
    }
    function checkparent($idpost,$id){
        $q = "
           SELECT c.id
            FROM user_group_detail a
            LEFT JOIN user_group_users b ON b.user_group_id=a.user_group_id
            LEFT JOIN m_app c ON c.id = a.app_id
            WHERE c.url='$idpost' and c.parent_id=100  
            AND b.users_id = '$id'"; 
        return $this->db->query($q)->num_rows();  
    }    


}
