 <div class="container">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2> Input User</h2> 
                    </div>        
                </div>
            </div>
        <form id="basic-form" autocomplete="off" method="post" novalidate>
            <div class="row clearfix"> 
                <div class="col-lg-6 col-md-6">
                    <div class="card"> 
                                <div class="body">  
                                    <div class="form-group">
                                        <label>Group User</label>
                                        <select id="group" class="form-control select2" name="group">
                                       <?php print_r($default['group']); foreach ($default['group'] as $row) { ?>
                                                
                                                <option data-id="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                                        <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                                    <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                            <?php } ?>
                                    </select>
                                        </div> 
                                    <div class="form-group">
                                        <label>UserName</label>
                                        <input id="id" type="hidden" value="<?php echo $id; ?>" class="form-control" aria-describedby="basic-addon2">
                                        <div class="input-group mb-3">
                                            <input id="username" type="text" value="<?php echo $username; ?>" class="form-control" aria-describedby="basic-addon2"> 
                                            </div>
                                        </div>   
                                    <div class="form-group">
                                        <label>Password</label> 
                                        <div class="input-group mb-3">
                                            <input id="password" type="password" value="<?php echo $password; ?>" class="form-control" aria-describedby="basic-addon2"> 
                                            </div>
                                        </div>
                                </div> 
                                <button type="submit" id="submit" class="btn btn-primary">Save</button>
                                <button type="reset" id="cancel" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div> 
            </div>
        </form>
            
        </div>  
 <script type="text/javascript">   
  
    $(document).ready(function ()
    { 
            $("#group").select2();
        var buttonsave, buttoncancel, urlpost, urlindex, content;

        buttonsave = $('#submit');
        buttoncancel = $('#cancel');
        urlpost = '<?php echo $url_post; ?>';
        urlindex = '<?php echo $url_index; ?>';
        content = $("#contentdata");
  

        buttonsave.click(
                function ()
                {

                    $.ajax(
                            {
                                type: "POST",
                                url: urlpost,
                                dataType: "json",
                                data: {
                                    id: $("#id").val(),
                                    group: $("#group").val(),
                                    username: $("#username").val(), 
                                    password: $("#password").val(), 
                                },
                                cache: false,
                                success:
                                        function (data, text)
                                        {
                                            if (data.hasil == 'true') {
                                                content.fadeOut("slow", "linear");
                                                window.top.location.href = 'index.php#User'; 
                                                content.fadeIn("slow");
                                            } else {
                                                alert(data.msg);

                                            }
                                        },
                                error: function (request, status, error) {
                                    alert(request.responseText + " " + status + " " + error);
                                }
                            });
                    return false;

                });

        buttoncancel.click(
                function ()
                {
                    content.fadeOut("slow", "linear");
                    window.top.location.href = 'index.php#User'; 
                    content.fadeIn("slow");

                }); 

    }); 
</script> 