<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DashoverAll extends CI_Controller {
   

    function __construct() {
        parent::__construct();
        
        $this->load->model('Mm_dashboardall', 'retrum'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    public function index() {
        $userId = $this->session->userdata('ses_user_id');
//      $row = $this->wil->get_wilayah_by_nik($userId)->row();
//           $user_group_id=$row->user_group_id;
//           if ($user_group_id==6){
        $aktor = $this->session->userdata('ses_aktor');
        $data = array(
                "base" => base_url(),
                "url_grid" => site_url('Dash/grid'), 
                "url_salesGrid" => site_url('Dash/gridsales'),
                "url_salesGridqty" => site_url('Dash/gridsalesqty'), 
                "url_salesGridqtyperiode" => site_url('Dash/gridsalesqtyperiode'), 
            );

        $resultahun= $this->array->data_tahun();
        $e = 0;
        foreach ($resultahun as $rowtahun) {              
            $data['default']['tahun'][$e]['value'] = $rowtahun['tahun'];
            $data['default']['tahun'][$e]['display'] = $rowtahun['tahun']; 
            $e++;
        } 
        $user = $this->session->userdata('ses_userName'); 
         $usergroup= $this->session->userdata('ses_aktor'); 
         $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Dashboard ALL'; 
        $log_trans='ALL'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("dashboard", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);   
        $this->load->view('pages/dashboardall', $data); 

    }
     public function gridsales() {

         $periode = $this->input->post('periode'); 
        echo json_encode(array(
            "data" => $this->retrum->data_sales($periode)->result(),
            "msg" =>'Ok', 
        )); 
    } 
    public function gridsalesqtyperiodeamount() {
 
         $nilaissalesperiode = $this->retrum->data_salesqtyperiodeamount()->result();  
         $totalsales = $this->retrum->data_salesqtyperiodetotal()->result();  

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissalesperiode, 
                "totalsales"=>$totalsales, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    } 
    public function gridsalesqtyperiode() {
 
         $nilaissalesperiode = $this->retrum->data_salesqtyperiode()->result();  
         $totalsales = $this->retrum->data_salesqtyperiodetotal()->result();  

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissalesperiode, 
                "totalsales"=>$totalsales, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    } 
     public function gridsalesqty() {
 
         $nilaissales = $this->retrum->data_salesqty()->result(); 
         $category = $this->retrum->data_salescategory()->result(); 

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales,
                "category"=>$category,
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }     
    public function gridsalesqtyQ() {
 
         $datavarian = $this->input->post('datavarian'); 
         $nilaissales = $this->retrum->data_salesqtyVarian($datavarian)->result();  

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }  
    public function gridsalesqtytipe() {
 
         $nilaissales = $this->retrum->data_salesqtytipe()->result();   
         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    } 
    public function gridsalesqtyfull() {
  
         $nilaissales = $this->retrum->data_salesqtyVarianfull()->result();  

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }  
    public function gridsalesqtychassis() {
  
        // $nilaissales = $this->retrum->data_saleschassis()->result();  

         $nilaissales = $this->retrum->data_saleschassis()->result();  
         $chassis = $this->retrum->data_chassisdetail()->result(); 

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales, 
                "chassis"=>$chassis, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }  

//     INSERT INTO transaksi_chassis (tahun, idchassis,qty,totalV)
// SELECT 2019,c.id,IFNULL(SUM(s.qty), 0),IFNULL(SUM(s.Utotal), 0) FROM chassis c 
// INNER JOIN chassis_detail d ON c.id=d.idchassis  
// LEFT JOIN  sales s ON  d.id=s.chassis_id AND s.tahun=2019
// GROUP BY c.chassis
 

}
