var url2 = "http://localhost:8090/api/purchasing/lpb";
function graphbar(callback){ 
     var param = {
        startDate : "2019-01-01",
        endDate :  "2019-07-17",
        NoClass1 :"[A]",
        NoClass2 : "[ABCDEI]",
        NoClass3 : "%",
        NoClass4 : "%",
        option : 2
      };
        $.ajax({
            url: url2, 
            dataType: 'json', 
            method:'POST',
            data:param,
            success: function(data){    
                var datas=data.data;    
                callback({databar:datas}); 
            } 
        }); 
} 
graphbar(function(res){
    Array.prototype.contains = function(v) {
    for(var i = 0; i < this.length; i++) {
        if(this[i] === v) return true;
    }
    return false;
};

Array.prototype.unique = function() {
    var arr = [];
    for(var i = 0; i < this.length; i++) {
        if(!arr.includes(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr; 
} 
    var list=res.databar; 
    console.log(list);
    var periodebar=[];   
    var NoClass2=[];
    var groups = {}; 
    for (var i = 0; i < list.length; i++) {   
          var periode =list[i].Period;
          var NmClass2=list[i].NmClass2;
          var groupName = list[i].NmClass2;
                var listnilai=list[i].TotalValue;
                    var nilai =Number(listnilai);
         periodebar.push(periode);
         NoClass2.push({name:NmClass2});
         if (!groups[groupName]) {
                    groups[groupName] = [];
                  }
                  groups[groupName].push(nilai);  

    }  
                myArray = [];
                for (var groupName in groups) { 
                  myArray.push({type: 'column',name: groupName, data: groups[groupName]});
                }     
                myArray.push({
                    type: 'spline',
                    showInLegend: false,  
                    data: groups[groupName],
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }
                });
var uniques = periodebar.unique(); 
uniques.sort();  

                 console.log(myArray);
        Highcharts.chart('container', {
        
        title: {
            text: 'Rekap Sales',
            enabled:false
        },
        exporting: {
                    enabled:false
                },
        subtitle: {
            text:null
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: uniques 
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Values '
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:myArray
    });
});   

  // Highcharts.chart('container', {
  //     chart: {
  //         type: 'bar'
  //     },
  //     title: {
  //         text: 'Historic World Population by Region'
  //     },
  //     subtitle: {
  //         text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
  //     },
  //     xAxis: {
  //         categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
  //         title: {
  //             text: null
  //         }
  //     },
  //     yAxis: {
  //         min: 0,
  //         title: {
  //             text: 'Population (millions)',
  //             align: 'high'
  //         },
  //         labels: {
  //             overflow: 'justify'
  //         }
  //     },
  //     tooltip: {
  //         valueSuffix: ' millions'
  //     },
  //     plotOptions: {
  //         bar: {
  //             dataLabels: {
  //                 enabled: true
  //             }
  //         }
  //     },
  //     legend: {
  //         layout: 'vertical',
  //         align: 'right',
  //         verticalAlign: 'top',
  //         x: -40,
  //         y: 80,
  //         floating: true,
  //         borderWidth: 1,
  //         backgroundColor:
  //             Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
  //         shadow: true
  //     },
  //     credits: {
  //         enabled: false
  //     },
  //     series: [{
  //         name: 'Year 1800',
  //         data: [107, 31, 635, 203, 2]
  //     }, {
  //         name: 'Year 1900',
  //         data: [133, 156, 947, 408, 6]
  //     }, {
  //         name: 'Year 2000',
  //         data: [814, 841, 3714, 727, 31]
  //     }, {
  //         name: 'Year 2016',
  //         data: [1216, 1001, 4436, 738, 40]
  //     }]
  // });