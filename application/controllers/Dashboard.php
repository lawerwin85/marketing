<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('Ms_dashboard', 'dash');
        $this->is_logged();
    }

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    public function index() {
        if(isset($_REQUEST)){
         $idpost =$this->input->post('idpost'); 
        }
        $data = array(
            "base" => base_url(),
            "site" => site_url(),  
        ); 
        
        $id = $this->session->userdata('ses_user_id'); 
       
        //var_dump($idpost);
         //die();
        $userGroupId = $this->dash->getUserGroupId($id);
        $x =0;



        foreach ($userGroupId as $s) {
            $app = $this->dash->getMenu(100, $s['user_group_id'], $id);
            $i = 0;
            foreach ($app as $r1) {

                $flag =0;

                $idMenu = $r1['app_id']; 
                if(isset($data['default'])) {
                    for ($y = 0; $y < count($data['default']['main_menu']); $y++) {
                        if ($data['default']['main_menu'][$y]['id'] == $idMenu) {
                            $flag = 1;
                            break;
                        }
                    }
                } 
                if($flag==0) {

                    $data['default']['main_menu'][$i]['id'] = $idMenu;
                    $data['default']['main_menu'][$i]['name'] = $r1['appName'];
                    $data['default']['main_menu'][$i]['url'] = $r1['url']; 
                    $data['default']['main_menu'][$i]['icon'] = $r1['icon'];

                    $app2 = $this->dash->getMenu($idMenu, $s['user_group_id'], $id);
                    $i2 = 0;
                    foreach ($app2 as $r2) {
                        $data['default']['sub_menu'][$idMenu][$i2]['id'] = $r2['app_id'];
                        $data['default']['sub_menu'][$idMenu][$i2]['name'] = $r2['appName'];
                        $data['default']['sub_menu'][$idMenu][$i2]['url'] = $r2['url'];
                        $data['default']['sub_menu'][$idMenu][$i2]['icon'] = $r2['icon'];

                        $i2++;
                    }
                    $i++;
                }
            }

            $x++;
        }    
         
            
        
        $this->load->view('template/index', $data);
    }   
}
