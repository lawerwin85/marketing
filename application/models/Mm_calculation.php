<?php

/* membuat class dengan nama Magama_model*/
class Mm_calculation extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "chassis_detail"; 

    }

     /* mendapatkan semua data dan hasilnya sebuah array */
    public function getAll() {
        return $this->db->get($this->table)->result_array();
    }
 
    function getGridData() {
        $query = "SELECT COUNT(id) total,tahun,sum(qty) qty FROM sales
                    where  status=0
                    GROUP BY tahun";
        return $this->db->query($query);      
    }
    function getGridDatadetail($id) {
        $query = "SELECT v.varian,c.keterangan,s.kat_size,s.varian_id,SUM(s.qty) qty,v.field FROM sales s 
                    INNER JOIN varian v ON v.id=s.varian_id
                    INNER join categorysize c on c.id=s.kat_size
                    WHERE s.tahun='$id' and s.status=0
                    GROUP BY s.varian_id,s.kat_size,v.field";
        return $this->db->query($query);      
    }

    function getby_id($tahun,$ckatageri,$varian) {
        $query = "SELECT v.varian,c.keterangan,s.kat_size,s.varian_id,SUM(s.qty) qty,SUM(s.UTotal) qtyamount,v.field,s.id_tipe FROM sales s 
                    INNER JOIN varian v ON v.id=s.varian_id
                    INNER join categorysize c on c.id=s.kat_size
                    WHERE s.tahun='$tahun' and s.varian_id='$varian' and s.kat_size='$ckatageri' and s.status=0
                    GROUP BY s.varian_id,s.kat_size,v.field";
        return $this->db->query($query);   
    }

    function updatesalesstatus($tahun,$ckatageri,$varian) {
        $query = "UPDATE sales set status=1 
                    WHERE tahun='$tahun' and varian_id='$varian' and kat_size='$ckatageri' and status=0";
        return $this->db->query($query);   
    }
    function insert($record) {
        $this->db->insert($this->table, $record);
    } 
    function update($id, $record) {
        $this->db->where("id", $id);
        $this->db->update($this->table, $record);
    }  
    function delete($id) {
        $this->db->delete($this->table, array("id" => $id)
        );
    }

}
// INSERT INTO transaksi_chassis (tahun, idchassis,qty,totalV) 
// SELECT 2013,c.id,IFNULL(SUM(s.qty), 0),IFNULL(SUM(s.Utotal), 0) FROM chassis c 
// INNER JOIN chassis_detail d ON c.id=d.idchassis 
// LEFT JOIN sales s ON d.id=s.chassis_id AND s.tahun=2013 GROUP BY c.chassis; 


// INSERT INTO transaksi_chassis (tahun, idchassis,qty,totalV) 
// SELECT 2013,c.id,IFNULL(SUM(s.qty), 0),IFNULL(SUM(s.Utotal), 0) FROM chassis c 
// INNER JOIN chassis_detail d ON c.id=d.idchassis 
// LEFT JOIN sales s ON d.id=s.chassis_id AND s.tahun=2013
// where kategori_id in('wb') GROUP BY c.chassis; 