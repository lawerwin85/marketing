<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* class dengan nama Login */

class Login extends CI_Controller {
   
    function __construct() {
        parent::__construct();

        $this->load->model('Login_model', 'login');  
        $this->load->library('session');    
        //$this->load->helper(array('form', 'url','log'));

        $this->load->library('form_validation'); //<---------loaded.
    }

    /* fungsi index yang di load pertama pada saat controller Login di akses */

    public function index() {

        /* setting array key untuk di home.php agar urlnya dinamis, maka 
         */
        $data = array(
            "base" => base_url(),
            "site" => site_url(),
            "url_post" => site_url('login/validationlogin') 
        );

        $this->load->view('login', $data);   
    }

 

    function forgot(){
//        var_dump("masuk sini");
//        die();
        $data = array(
            "base" => base_url(),
            "site" => site_url(),
            "url_post" => site_url('login/validationlogin'),
            "forgot_password" => site_url('login/validationlogin')
        );
        $this->load->view('forgot', $data);
    }

   
 
    function validationlogin() {
        
        $this->load->library('session');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required'); 

        if ($this->form_validation->run() == TRUE) { 
            $user = $this->input->post('username'); 
            $password = $this->input->post('password');  
            $rowlogin = $this->login->getuserdata($user, $password);
          //  $checkdata = $this->login->checkuserdata($user, $pass);
           // $checkdatasiswa = $this->login->cekdataloginsiswa($user, $pass);
             //$passq = md5($user . $pass);
            $checkdata=$rowlogin[0];
             $rowlogins=$rowlogin[1]; 
            // var_dump($rowlogins); 
            // die();
            if ($checkdata == 1) { 
                $session = array(
                        'ses_statuslogin' => true, 
                        'ses_aktor' => $rowlogins->g_desc,
                        'ses_aktorgroup' => $rowlogins->g_name,
                        'ses_loginId' => $rowlogins->loginId, 
                        // 'ses_name' => $rowlogin->nama,  
                        'ses_nik' => $rowlogins->nik, 
                        'ses_user_id' => $rowlogins->userId,
                        'ses_userName' => $rowlogins->userName,
                        'ses_base_url' => base_url()
                    );
                $this->session->set_userdata($session);
                $message = "Login Suskes";
                $valid = true;
                $redir = site_url();

            } else {
                /* membuuat array untuk kebutuhan session */
                $session = array(
                    'ses_statuslogin' => FALSE,
                );
                /* fungsi membuat session */
                $this->session->set_userdata($session);
                $valid = false;
                $redir = site_url("Login");
                $message = "Login Failed, check your username or password";
                    $userName = $user;
                    $userId = $user;
                    $statuslogin = 0;
            }
            $valid = $valid;
            $message = $message;

            $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid,
                "err_username" => null,
                "err_password" => null,
                "redirecto" => $redir
            );
        } else {
            /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
            $jsonmsg = array(
                "msg" => 'Login Data Failed',
                "hasil" => false, 
                "redirecto" => site_url("Login")
            );
        } 
        /* konversi array json, yang akan terkirim ke login.php di folder view */
        echo json_encode($jsonmsg);
    }
 
    function logout() {

        $user = $this->session->userdata('ses_userName'); 
         $usergroup= $this->session->userdata('ses_aktor'); 
         $ses_loginId= $this->session->userdata('ses_loginId');  
        $log_trans='Logout'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("logout", "Keluar aplikasi",$user,$usergroup,$ses_loginId,$log_id,$log_trans);  
        
        $this->session->sess_destroy();
        redirect('login', 'refresh');
    }

}
