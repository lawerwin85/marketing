<?php 
function helper_log($tipe = "", $str = "",$user ="",$usergroup ="",$ses_loginId ="",$log_id ="",$log_trans =""){
    $CI =& get_instance();
 
    if (strtolower($tipe) == "login"){
        $log_tipe   = 1;
    }
    elseif(strtolower($tipe) == "logout")
    {
        $log_tipe   = 5;
    }
    elseif(strtolower($tipe) == "add"){
        $log_tipe   = 2;
    }
    elseif(strtolower($tipe) == "edit"){
        $log_tipe  = 3;
    }
    elseif(strtolower($tipe) == "dashboard"){
        $log_tipe  = 9;
    }
    elseif(strtolower($tipe) == "view"){
        $log_tipe  = 8;
    }
    elseif(strtolower($tipe) == "hapus"){
        $log_tipe  = 7;
    }
    else{
        $log_tipe  = 4;
    }
 
    // paramter
    $param['log_id']      = $log_id;
    $param['log_user']      = $user;
    $param['log_trans']      = $log_trans;
    $param['log_group']      = $usergroup;
    $param['login_id']      = $ses_loginId;
    $param['log_tipe']      = $log_tipe;
    $param['log_desc']      = $str;
 
    //load model log
    $CI->load->model('m_log');
 
    //save to database
    $CI->m_log->save_log($param);
 
}
?>