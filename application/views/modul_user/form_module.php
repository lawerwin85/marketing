<div class="col-md-12">
    <div class="panel panel-white" data-sortable-id="form-stuff-2">
        <div class="panel-heading">

            <h4 class="panel-title">Form <?php echo $title ?></h4>
        </div>
        <div class="panel-body">


            <form id="demo-form2" data-parsley-validate class="form-horizontal">
                <div class="col-sm-6">

                    <input type="hidden" id="vbr" value="<?php echo $id; ?>" />
                    <input type="hidden" id="user_group_id" value="<?php echo $user_group_id; ?>" />

                    <div class="form-group">
                        <label class="control-label col-xs-3" for="app_id">App Name</label>
                        <div class="col-xs-9">
                            <select class="form-control" id="app_id" name="app_id">
                                <?php foreach ($default['app_id'] as $row) { ?>

                                    <option value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>"
                                        <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                        <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                <?php } ?>
                            </select>
                            <span id="err_app_id"></span>
                        </div>
                    </div>
                    <div class="portlet box green ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Customize Submenu </div>
                        </div>
                        <div class="portlet-body form">
                            <form role="form">
                                <div class="form-body" id="submenu_body">

                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="button" id="cancel" class="btn btn-warning">Cancel</button>
                        <button type="button" id ="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<script src="<?php echo $base; ?>assets/script/modul.js"></script>
<script type="text/javascript">
    $(document).ready(function ()
    {
        var buttonsave, buttoncancel, urlpost, urlindex, content;


        buttonsave = $('#submit');
        buttoncancel = $('#cancel');
        urlpost = '<?php echo $url_post; ?>';
        urlindex = '<?php echo $url_index; ?>';
        content = $("#contentdata");


        $('#app_id').change(function() {
            var x = this.value;
            var a = $(this).find("option:selected").text();
            mdl.custom_group_menu(x,a);
        });

        buttonsave.click(
            function () {


                var id = [];
                var add = [];
                var upd = [];
                var del = [];

                id.push($('#app_id').val());
                add.push(0);
                upd.push(0);
                del.push(0);

                for(i=0; i<mdl.subMenu.length; i++){
                    id.push(mdl.subMenu[i].id);
                    add.push(mdl.subMenu[i].allowAdd);
                    upd.push(mdl.subMenu[i].allowUpdate);
                    del.push(mdl.subMenu[i].allowDelete);
                }

                $.ajax({
                    url: urlpost,
                    type: "post",
                    dataType: "json",
                    cache: false,
                    data : {
                        "user_group_id" : $('#user_group_id').val(),
                        "id[]" : id,
                        "add[]" : add,
                        "upd[]" : upd,
                        "del[]" : del
                    },
                    success:function(xx){
                        bootbox.confirm("save success",function (result) {
                            if(result){
                                content.fadeOut("slow", "linear");
                                content.load(urlindex);
                                content.fadeIn("slow");
                            }
                        })
                    }
                });

                return false;

            });

        buttoncancel.click(
            function ()
            {
                content.fadeOut("slow", "linear");
                content.load(urlindex);
                content.fadeIn("slow");

            });




    });


</script>