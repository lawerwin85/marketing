 <div class="container"> 
         <div class="col-lg-12">
                    <div class="card">
                            <?php $category=$_GET['category'];
                                  $tahun=$_GET['tahun'];
                                  $kat=$_GET['kat'];
                                  $varian=$_GET['varian'];
                                  if($varian=='b'){
                                    $t='BOX';
                                  }
                                  elseif($varian=='nb'){
                                    $t='NON BOX';
                                  }
                                  else{
                                    $t=$_GET['varian'];
                                  }
                                  $varian=$_GET['varian'];
                                  if($category=='category'){$title='Category Size';}
                                  elseif($category=='tipe'){$title=$kat;}
                                  elseif($category=='QR'){$title=$varian;}
                                  elseif($category=='chassis'){$title=$varian;}
                                  elseif($category=='overall'){$title='Overall';}
                                  elseif($category=='cust'){$title=$kat;}
                                  elseif($category=='custall'){$title=$kat;}
                                  elseif($category=='group'){$title=$kat;}
                                  elseif($category=='Big'){$title=$t;}
                                  elseif($category=='Small'){$title=$t;}
                                  elseif($category=='Medium'){$title=$t;}
                                   ?>
                        <div class="header">
                            <h2>Report Dashboard<small><?php echo $title ?></small></h2>                            
                        </div>
                        <div class="body">
                            <input type="hidden" id="category" value="<?php echo $category ?>">
                            <input type="hidden" id="tahun" value="<?php echo $tahun ?>">
                            <input type="hidden" id="kat" value="<?php echo $kat ?>">
                            <input type="hidden" id="varian" value="<?php echo $varian ?>">
                            <div class="table-responsive">
                                <table  id="datatable-ajax" class="table table-hover js-basic-example dataTable table-custom">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal Sales</th>
                                            <th>Nama Produksi</th> 
                                            <th>Detail Chassis</th>  
                                            <th>Chassis</th>
                                            <th>Qty</th> 
                                            <th>Price</th> 
                                            <th>Total Price</th>  
                                        </tr>
                                    </thead> 
                                    <tbody>
                                         
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
        </div>
  <script>
   var InputSales = {
        table: undefined
    };
    $(document).ready(function () {
        /* start datatable */
        var category=$("#category").val();
        var tahun=$("#tahun").val();
        var kat=$("#kat").val();
        var varian=$("#varian").val();
        var handleDataTableDefault = function () {
            if ($("#datatable-ajax").length!==0) {
               InputSales.table = $("#datatable-ajax").DataTable({
                    "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
                        var oSettings = this.fnSettings();
                        var iTotalRecords = oSettings.fnRecordsDisplay();
                        $("#total").val(iTotalRecords);
                        $("td:first", nRow).html(iDisplayIndex +1);
                       return nRow;
                    }, 
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'print',
                            text: 'Print all',
                            title: '',
                            exportOptions: {
                                modifier: {
                                    selected: null
                                }
                            }
                        }],
                    "ajax": {
                        "url": '<?php echo $url_grid; ?>',
                        "type": 'POST',
                        "data" : {
                            "category" :category,
                            "tahun" :tahun, 
                            "kat" :kat, 
                            "varian" :varian 
                        },
                    },
                    "columns": [
                        {"data": "id"},
                        {"data": "tgl_sales"},
                        {"data": "nmBarang"}, 
                        {"data": "nmChassis"}, 
                        {"data": "chassis"}, 
                        {"data": "qty"}, 
                        {"data": "price"}, 
                        {"data": "total"}
                    ]
                });
            }
        };
        TableManageDefault = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableDefault();
                }
            };
        }();
        TableManageDefault.init();
    });
    /* end datatable */

 
   

</script>