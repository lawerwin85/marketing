var urlchassis = urlpost+'Dashutlamount/gridsalesqtychassis';
 $(function () { 
        function graphbar(callback){ 
              
                $.ajax({
                    url: urlchassis, 
                    dataType: 'json', 
                    type:'POST',  
                    data: {
                        periode: '2019', 
                    },
                    success: function(data){    
                        var datas=data.nilaissales;  
                        var chassis=data.chassis;  
                        callback({databar2:datas,chassisd:chassis}); 
                    } 
                }); 
        } 
        graphbar(function(res){
            Array.prototype.contains = function(v) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === v) return true;
            }
            return false;
        };

        Array.prototype.unique = function() {
            var arr = [];
            for(var i = 0; i < this.length; i++) {
                if(!arr.includes(this[i])) {
                    arr.push(this[i]);
                }
            }
            return arr; 
        } 
            var list=res.databar2; 
            var listchassis=res.chassisd; 
            var periodebar=[];    
            var groups2 = {};    
            for (var i = 0; i < list.length; i++) {   
                  var periode =list[i].tahun; 
                        var listnilai=list[i].qty; 
                        var nilai =Number(listnilai);
                        var groupName22 = list[i].chassis; 
                        if (!groups2[groupName22]) {
                            groups2[groupName22] = [];
                          }    
                        periodebar.push(periode);   
                          groups2[groupName22].push(nilai);  
            }
                             
                        myArray = [];
                        for (var groupName22 in groups2) {  
                             var data =groups2[groupName22];
                              
                                data.forEach(function(element, index) { 
                                    if (element === 0) { 
                                //    console.log(element);
                                      data[index] = null;
                                    }
                                  }); 
                          myArray.push({type: 'column',name: groupName22, data: groups2[groupName22]});
                            //getshow(data);
                        }   
                       function filter_array(test_array) {
                                let index = -1;
                                const arr_length = test_array ? test_array.length : 0;
                                let resIndex = -1;
                                const result = [];

                                while (++index < arr_length) {
                                    const value = test_array[index];

                                    if (value) {
                                        result[++resIndex] = value;
                                    }
                                }

                                return result;
                            }
 
             myArray.push({
                            type: 'spline',
                            showInLegend: false,  
                            data:data, 
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[3],
                                fillColor: 'white'
                            }
                        });
                     //   console.log(filter_array([groups2]));
                       // function getshow(data){ 
                       //  myArray.push({
                       //      type: 'spline',
                       //      showInLegend: false,  
                       //      data:data, 
                       //      marker: {
                       //          lineWidth: 2,
                       //          lineColor: Highcharts.getOptions().colors[3],
                       //          fillColor: 'white'
                       //      }
                       //  });
                       // }
        var uniques = periodebar.unique(); 
        uniques.sort();  
        Highcharts.setOptions({
            lang: {
                decimalPoint: ',',
                thousandsSep: '.'
            }
        });
 

             // console.log(myArray);
                Highcharts.chart('containerFullchassis', {
                
                title: {
                    text: 'CHASSIS UTILITY BASED ON AMOUNT',
                    enabled:false
                },
                exporting: {
                            enabled:false
                        },
                subtitle: {
                    text:null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: uniques 
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'AMOUNT / K'
                    }
                },
                tooltip: {
                     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td align="right" style="padding:0"><b>{point.y:,.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: { 
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        dataLabels: {
                          enabled: false,
                          formatter: function() {
                            if (this.y > 0) {
                              return this.y;
                            }
                          }
                        },
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                   //alert('Category: ' + this.category + ', value: ' + this.series.name);  
                                  window.open ('#Reportdash?category=chassis&tahun='+this.category+'&kat='+this.series.name+'&varian=all','_blank'); 
                                }
                            }
                        }
                    }
                },
                series:myArray
            });
        });    
})