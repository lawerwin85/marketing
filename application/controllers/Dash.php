<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dash extends CI_Controller {
   

    function __construct() {
        parent::__construct();
        
        $this->load->model('Mm_dashboard', 'retrum'); 
        $this->load->model('Mm_dashboardutl', 'retrum2'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    public function index() {
        $userId = $this->session->userdata('ses_user_id');
  //      $row = $this->wil->get_wilayah_by_nik($userId)->row();
//           $user_group_id=$row->user_group_id;
//           if ($user_group_id==6){
        $aktor = $this->session->userdata('ses_aktor');
        $data = array(
                "base" => base_url(),
                "url_grid" => site_url('Dash/grid'), 
                "url_salesGrid" => site_url('Dash/gridsales'),
                "url_salesGridqty" => site_url('Dash/gridsalesqty'), 
                "url_salesGridqtyperiode" => site_url('Dash/gridsalesqtyperiode'), 
            );

        $resultahun= $this->array->data_tahun();
        $e = 0;
        foreach ($resultahun as $rowtahun) {              
            $data['default']['tahun'][$e]['value'] = $rowtahun['tahun'];
            $data['default']['tahun'][$e]['display'] = $rowtahun['tahun']; 
            $e++;
        };
        $user = $this->session->userdata('ses_userName'); 
         $usergroup= $this->session->userdata('ses_aktor'); 
         $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Dashboard by Wingbox QTY'; 
        $log_trans='WQ'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("dashboard", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->load->view('pages/dashboard', $data); 

    }
     public function gridsales() {

         $periode = $this->input->post('periode'); 
        echo json_encode(array(
            "data" => $this->retrum->data_sales($periode)->result(),
            "msg" =>'Ok', 
        )); 
    } 
    public function gridsalesqtyperiode() {
 
         $nilaissalesperiode = $this->retrum->data_salesqtyperiode()->result();  
         $totalsales = $this->retrum->data_salesqtyperiodetotal()->result();  

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissalesperiode, 
                "totalsales"=>$totalsales, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    } 
     public function gridsalesqty() {
 
         $nilaissales = $this->retrum->data_salesqty()->result(); 
         $category = $this->retrum->data_salescategory()->result(); 

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales,
                "category"=>$category,
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }     
    public function gridsalesqtyQ() {
 
         $datavarian = $this->input->post('datavarian'); 
         $nilaissales = $this->retrum->data_salesqtyVarian($datavarian)->result();  

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }  
    public function gridsalesqtytipe() {
 
         $nilaissales = $this->retrum->data_salesqtytipe()->result();   
         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    } 
    public function gridsalesqtyfull() {
  
         $nilaissales = $this->retrum->data_salesqtyVarianfull()->result();  

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }  
    public function gridsalesqtychassis() {
   
          
          $checkdata = $this->retrum->checkData(); 
            if ($checkdata > 0) {  
                $this->retrum2->droptemp();  
            }  
        $checkdata2 = $this->retrum2->checkData2(); 
            if ($checkdata2 > 0) {   
                $this->retrum2->droptempcalc(); 
            }     
        $row = $this->retrum->getendingyear()->row();
        $rowcount = $this->retrum->getcountchassis()->row();
        $tahun = $row->tahun;
        $countchassis = $rowcount->chassis;
        $nowdate=date("Y"); 
        $blank=$countchassis-5;  
        $limit=5;  
        $firstYear = $tahun; 
        for ($i = $tahun; $i<=$nowdate; $i++)
        {$i; 

             $getdatacalc =$this->retrum2->calculateyear($i);    
             $getdatacalc2 =$this->retrum2->updatecalculateyear($i);
             $getdata =$this->retrum->data_chassisyear($i,$limit);  
             $getdata2 =$this->retrum->data_chassisyearblank($i,$blank);  
        } 
         $nilaissales = $this->retrum->data_saleschassis()->result();  
         //$chassis = $this->retrum->data_saleschassis()->result();
         $chassis = $this->retrum->data_saleschassisup()->result();

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissales"=>$nilaissales, 
                "chassis"=>$chassis, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }  
    public function gridcustperkat() { 
            $tahun = $this->input->post('periode');   
            $kat = $this->input->post('kat'); 
            
            $year=date("Y");
            if ($tahun==0){
                $periode=$year;
            }
            else{
               $periode=$tahun;
            }
         $nilaissales = $this->retrum->data_katsalesCust($periode,$kat)->result();   
         $chassis = $this->retrum->data_katsalesCustup($periode,$kat)->result();

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissalescust"=>$nilaissales, 
                "cust"=>$chassis, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }  
    public function gridsalesqtycust() { 
            $periode = $this->input->post('periode');    
         $nilaissales = $this->retrum->data_salescust($periode)->result();  
         //$chassis = $this->retrum->data_saleschassis()->result();
         $chassis = $this->retrum->data_salescustup($periode)->result();

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissalescust"=>$nilaissales, 
                "cust"=>$chassis, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }    
    public function gridsalesqtycustperiode() {     
         $nilaissales = $this->retrum->data_salescustful()->result();  
         //$chassis = $this->retrum->data_saleschassis()->result();
         $chassis = $this->retrum->data_salescustupfull()->result();

         $jsonmsg = array( 
                "hasil" => 'true', 
                "nilaissalescust"=>$nilaissales, 
                "cust"=>$chassis, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    } 
    public function gridsaleschassis() { 
         $chasis = $this->retrum->chassis()->result();  
         //$chassis = $this->retrum->data_saleschassis()->result();
         $chassisdetail = $this->retrum->chassisdetail()->result();

         $jsonmsg = array( 
                "hasil" => 'true', 
                "chasis"=>$chasis, 
                "chassisdetail"=>$chassisdetail, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }  

    public function gridsaleschassiskategori() { 
         $chasis = $this->retrum->chassis()->result();  
         //$chassis = $this->retrum->data_saleschassis()->result();
         $chassisdetail = $this->retrum->chassisdetailkategori()->result();

         $jsonmsg = array( 
                "hasil" => 'true', 
                "chasis"=>$chasis, 
                "chassisdetail"=>$chassisdetail, 
                "msg" =>'Ok', 
            );
         echo json_encode($jsonmsg); 
    }  
//     INSERT INTO transaksi_chassis (tahun, idchassis,qty,totalV)
// SELECT 2019,c.id,IFNULL(SUM(s.qty), 0),IFNULL(SUM(s.Utotal), 0) FROM chassis c 
// INNER JOIN chassis_detail d ON c.id=d.idchassis  
// LEFT JOIN  sales s ON  d.id=s.chassis_id AND s.tahun=2019
// GROUP BY c.chassis
 

}
