<div class="container">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-12">
                    <h2> Input Group User</h2> 
                </div>        
            </div>
        </div>
        <form id="basic-form" autocomplete="off" method="post" novalidate>
            <div class="row clearfix"> 
                <div class="col-lg-6 col-md-6">
                    <div class="card"> 
                                <div class="body">  
                                    <div class="form-group">
                                        <label>Group User</label>
                                        <input id="id" type="hidden" value="<?php echo $id; ?>" class="form-control" aria-describedby="basic-addon2">
                                        <div class="input-group mb-3">
                                            <input id="group" type="text" value="<?php echo $group; ?>" class="form-control" aria-describedby="basic-addon2"> 
                                            </div>
                                        </div>  
                                </div> 
                                <button type="submit" id="submit" class="btn btn-primary">Save</button>
                                <button type="reset" id="cancel" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div> 
            </div>
        </form>
        <div class="col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>List Group 
                            </h2>                            
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table  id="datatable-ajax" class="table table-hover js-basic-example dataTable table-custom">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th width="10">No</th>
                                            <th>Group</th>  
                                            <th>Action</th>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                         
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
</div>  
 <script type="text/javascript">   
    var addGroup = {
        table: undefined
    };
    $(document).ready(function ()
    {  
        var buttonsave, buttoncancel, urlpost, urlindex, content;

        buttonsave = $('#submit');
        buttoncancel = $('#cancel');
        urlpost = '<?php echo $url_post; ?>';
        urlindex = '<?php echo $url_index; ?>';
        content = $("#contentdata");
  

        buttonsave.click(
                function ()
                {

                    $.ajax(
                            {
                                type: "POST",
                                url: urlpost,
                                dataType: "json",
                                data: {
                                    id: $("#id").val(),
                                    group: $("#group").val(), 
                                },
                                cache: false,
                                success:
                                        function (data, text)
                                        {
                                            if (data.hasil == true) {
                                                content.fadeOut("slow", "linear");
                                                location.reload(true); 
                                                content.fadeIn("slow");
                                            } else {
                                                alert(data.msg);

                                            }
                                        },
                                error: function (request, status, error) {
                                    alert(request.responseText + " " + status + " " + error);
                                }
                            });
                    return false;

                });

        buttoncancel.click(
                function ()
                {
                    content.fadeOut("slow", "linear");
                    window.top.location.href = 'index.php#User'; 
                    content.fadeIn("slow");

                }); 
        var handleDataTableDefault = function () {
            if ($("#datatable-ajax").length!==0) {
               addGroup.table = $("#datatable-ajax").DataTable({
                    "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
                        var oSettings = this.fnSettings();
                        var iTotalRecords = oSettings.fnRecordsDisplay();
                        $("#total").val(iTotalRecords);
                        $("td:first", nRow).html(iDisplayIndex +1);
                       return nRow;
                    },
                    "ajax": {
                        "url": '<?php echo $url_gridgroup; ?>',
                        "type": 'POST',
                    },
                    "columns": [
                        {"data": "id"},
                        {"data": "g_desc"}, 
                        {
                            "data": "id", "width": "100px", "sClass": "left",
                            "bSortable": false,
                            "mRender": function (data, type, row) {
                                var btn = "";  
                                btn = btn + " <button onClick='ParamFunc.editdata(" + row.id + ")' class='btn btn-xs btn-icon btn-circle btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit Data' type='button'><i class='fa fa-edit'></i> </button>";
                                btn = btn + " <button onClick='ParamFunc.deletedata(" + row.id + ")' class='hapus btn btn-xs btn-icon btn-circle btn-danger' data-toggle='tooltip' data-placement='bottom' title='Delete Data' type='button'><i class='fa fa-trash-o'></i> </button>"; 
                                
                                btn = btn + "</div>";
                                return btn;
                            }
                        }
                    ]
                });
            }
        };
        TableManageDefault = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableDefault();
                }
            };
        }();
        TableManageDefault.init();

    }); 

        var ParamFunc = { 
            editdata: function (id) {
                window.top.location.href = 'index.php#User/editgroup/'+id; 
            }, 
            deletedata: function (id) {
                  var jawab = confirm("Apakah anda yakin untuk menghapus !");
                    if (jawab === true) {
            //            kita set hapus false untuk mencegah duplicate request
                        var hapus = false;
                        if (!hapus) {
                            hapus = true;
                            $.post('<?php echo $url_deletegroup ?>', {id: id},
                            function (data) {
                               addGroup.table.ajax.reload();
                            });
                            hapus = false;
                        }
                    } else {
                        return false;
                    }
            }
        }
</script> 