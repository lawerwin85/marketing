<?php

/* membuat class dengan nama Magama_model*/
class Mm_size extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "size"; 

    }

     /* mendapatkan semua data dan hasilnya sebuah array */
    public function getAll() {
        return $this->db->get($this->table)->result_array();
    }
 
    function getGridData() {
        $query = "SELECT * from size";
        return $this->db->query($query);      
    }  
    function getby_id($id) {
        $query = "SELECT * from size where id='$id'";
        return $this->db->query($query);   
    }
    function insert($record) {
        $this->db->insert($this->table, $record);
    } 
    function update($id, $record) {
        $this->db->where("id", $id);
        $this->db->update($this->table, $record);
    }  
    function delete($id) {
        $this->db->delete($this->table, array("id" => $id)
        );
    }

}
