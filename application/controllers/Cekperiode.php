<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Cekperiode extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_Cekperiode', 'table01');  
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_mtahun" => site_url('Cekperiode/m_tahun'),
            "url_mtahunkat" => site_url('Cekperiode/m_tahunkat'),
            "url_mtahuntipe" => site_url('Cekperiode/m_tahuntipe'),
            "url_transaksi_sales" => site_url('Cekperiode/transaksi_sales'),
            "url_transaksi_tipe" => site_url('Cekperiode/transaksi_tipe')
        );

        $year=date("Y");
        $mthaun = $this->table01->getM_tahun()->row();
        $mthaunkat = $this->table01->getM_tahunkat()->row();
        $mthauntipe = $this->table01->getM_tahunTipe()->row();
        $transaksi_salesqty = $this->table01->gettransaksi_salesqty()->row();
        $transaksi_tipe = $this->table01->gettransaksi_tipe()->row();
        $m_tahun=$mthaun->tahun; 
        $m_tahunkat=$mthaunkat->tahun; 
        $m_tahuntipe=$mthauntipe->tahun; 
        $transQty=$transaksi_salesqty->tahun;
        $transtipe=$transaksi_tipe->tahun;
        $data['m_tahun'] =$m_tahun; 
        $data['m_tahunkat'] =$m_tahunkat; 
        $data['m_tahuntipe'] =$m_tahuntipe; 
        $data['transQty'] =$transQty;  
        $data['transtipe'] =$transtipe; 
        if($year==$m_tahun){$datatahun='disabled';}else{$datatahun='';}
        if($year==$m_tahunkat){$datatahunkat='disabled';}else{$datatahunkat='';}
        if($year==$m_tahuntipe){$datatahuntipe='disabled';}else{$datatahuntipe='';}
        if($year==$transQty){$datatransQty='disabled';}else{$datatransQty='';}
        if($year==$transtipe){$datatranstipe='disabled';}else{$datatranstipe='';}
        // if($year==$m_tahun){$datatahun='disabled';}else{$datatahun='';}
        // if($year==$m_tahun){$datatahun='disabled';}else{$datatahun='';}
        // if($year==$m_tahun){$datatahun='disabled';}else{$datatahun='';}
        $data['thisyear'] =$year;  
        $data['buttonm_tahun'] =$datatahun; 
        $data['buttonm_tahunkat'] =$datatahunkat; 
        $data['buttonm_tahuntipe'] =$datatahuntipe;
        $data['button_transQty'] =$datatransQty;
        $data['button_transtipe'] =$datatranstipe;
        $this->load->view('vm_cekperiode/home', $data); 
        // $this->load->view('vm_calculation/confirm_delete', $data);
    }
     public function m_tahun() {
        $id = $this->input->post('id');
        
        $this->table01->updateM_tahun($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Update Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }
     public function m_tahunkat() {
        $id = $this->input->post('id');
        
        $this->table01->updateM_tahunkat($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Update Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }
     public function m_tahuntipe() {
        $id = $this->input->post('id');
        
        $this->table01->updateM_tahuntipe($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Update Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }
     public function transaksi_sales() {
        $id = $this->input->post('id');
        
        $this->table01->update_transaksi_sales($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Update Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }
     public function transaksi_tipe() {
        $id = $this->input->post('id');
        
        $this->table01->update_transaksi_tipe($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Update Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

}
