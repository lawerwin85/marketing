var urlcustperiode = urlpost+'Dash/gridsalesqtycustperiode'; 
 
        function graphbar(callback){  
            
                $.ajax({
                    url: urlcustperiode, 
                    dataType: 'json', 
                    type:'POST',  
                    data: {
                        periode: 2019, 
                    },
                    success: function(data){    
                        var datacust=data.nilaissalescust;  
                        var cust=data.cust;  
                        callback({databarcust:datacust,custd:cust}); 
                    } 
                }); 
        } 
        graphbar(function(res){
            Array.prototype.contains = function(v) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === v) return true;
            }
            return false;
        };

        Array.prototype.unique = function() {
            var arr = [];
            for(var i = 0; i < this.length; i++) {
                if(!arr.includes(this[i])) {
                    arr.push(this[i]);
                }
            }
            return arr; 
        } 
            var listbarcust=res.databarcust; 
            var listcust=res.custd; 
            var periodebar=[];    
            var groups = {};   
 
            for (var i = 0; i < listbarcust.length; i++) {   
                        var listnilai=listbarcust[i].qty;
                        var y =Number(listnilai);
                        var groupName2 = listbarcust[i].chassis;  
                        if (!groups[groupName2]) {
                            groups[groupName2] = [];
                          }   

                        
                          groups[groupName2].push({y:y,name:groupName2});  
            }
                        myArray = [];
                        for (var groupName2 in groups) { 
                          myArray.push({type: 'column',name: groupName2, data: groups[groupName2]});
                             
                         }
             
                        
   
 
                Highcharts.chart('containerFullbuyerperiode', {
                
                title: {
                    text: 'CUSTOMERS TOP 15 All Periode',
                    enabled:false
                },
                exporting: {
                            enabled:false
                        },
                subtitle: {
                    text:null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    type: 'category' 
                },
                yAxis: {
                    min: 0,
                    title: {
                        text:QTY
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: { 
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        pointWidth:22,
                        dataLabels: {
                          enabled: false,
                          formatter: function() {
                            if (this.y > 0) {
                              return this.y;
                            }
                          }
                        },
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                   //alert('Category: ' + this.category + ', value: ' + this.series.name);  
                                  window.open ('#Reportdash?category=custall&tahun='+this.category+'&kat='+this.series.name+'&varian=all','_blank'); 
                                }
                            }
                        }
                    }
                },
                series:myArray
            });
        });     