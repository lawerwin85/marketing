$(function () {
    Highcharts.chart('containerpie', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false, 
        marginLeft:0,
        marginTop:0,
        margin: 0,
        type: 'pie',
        marginTop: -50,
        margin: 0, 
        marginLeft:1,
    },
    title:false,          
    exporting: {
        enabled:false
    },
    tooltip: {
        pointFormat: null
    },
    credits: {
          enabled: false
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                distance:-1,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                filter: {
                    property: 'percentage',
                    operator: '>',
                    value: 4
                },
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Chrome',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Internet Explorer',
            y: 11.84
        }, {
            name: 'Firefox',
            y: 10.85
        }, {
            name: 'Edge',
            y: 4.67
        }, {
            name: 'Safari',
            y: 4.18
        }, {
            name: 'Sogou Explorer',
            y: 1.64
        }, {
            name: 'Opera',
            y: 1.6
        }, {
            name: 'QQ',
            y: 1.2
        }, {
            name: 'Other',
            y: 2.61
        }]
    }]
});
});