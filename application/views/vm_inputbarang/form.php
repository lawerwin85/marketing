 <div class="container">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2> Input Produk</h2> 
                    </div>        
                </div>
            </div>
        <form id="basic-form" autocomplete="off" method="post" novalidate>
            <div class="row clearfix"> 
                <div class="col-lg-6 col-md-6">
                    <div class="card"> 
                        <div class="body">  
                                <div class="form-group">
                                    <label>Chassis</label>
                                    <select id="chassis" class="form-control select2" name="chassis">
                                       <?php print_r($default['chassis']); foreach ($default['chassis'] as $row) { ?>
                                                
                                                <option data-id="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                                        <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                                    <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Type Chassis</label> 
                                    <div class="input-group mb-3">
                                        <input id="id" type="hidden" value="<?php echo $id; ?>" class="form-control" aria-describedby="basic-addon2">
                                        <input id="nmchassisdetail" type="text" value="<?php echo $nmchassisdetail; ?>" class="form-control" aria-describedby="basic-addon2">
                                        <input id="chassisdetail" type="hidden" value="<?php echo $chassisdetail; ?>" class="form-control" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button href="#largeModal" data-toggle="modal" class="btn btn-outline-secondary showmodal" type="button">Get >></button>
                                        </div>
                                    </div> 
                                </div>
                                <input id="namaUnit" type="hidden" value="<?php echo $namaUnit; ?>" class="form-control" aria-describedby="basic-addon2">
                           <!--      <div class="form-group">
                                    <label>Nama Unit</label>
                                    <div class="input-group mb-3">
                                        <input id="namaUnitshow" type="text" value="<?php echo $namaUnitshow; ?>" class="form-control" aria-describedby="basic-addon2">
                                        
                                    </div> 
                                </div>   -->
                                <div class="form-group">
                                    <label>Ukuran katagori</label>
                                    <select id="ckatageri" class="form-control select2" name="ckatageri">
                                       <?php print_r($default['ckatageri']); foreach ($default['ckatageri'] as $row) { ?>
                                                
                                                <option data-id="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                                        <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                                    <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Varian</label>
                                    <select id="varian" class="form-control select2" name="varian">
                                       <?php print_r($default['varian']); foreach ($default['varian'] as $row) { ?>
                                                
                                                <option value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                                        <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                                    <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="card"> 
                        <div class="body">
                            <form id="basic-form" method="post" novalidate>   
                                <div class="form-group">
                                    <label>Size</label>
                                   <select id="size" class="form-control select2" name="size">
                                       <?php print_r($default['size']); foreach ($default['size'] as $row) { ?>
                                                
                                                <option value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                                        <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                                    <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Kategori</label>
                                    <select id="kategori" class="form-control select2" name="kategori">
                                       <?php print_r($default['kategori']); foreach ($default['kategori'] as $row) { ?>
                                                
                                                <option value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                                        <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                                    <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>  
                                <div class="form-group">
                                    <label>Tipe</label>
                                    <select id="tipe"  class="form-control select2" name="kategori">
                                       <?php print_r($default['tipe']); foreach ($default['tipe'] as $row) { ?>
                                                
                                                <option value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                                        <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                                    <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>  
                                <button type="submit" id="submit" class="btn btn-primary">Save</button>
                                <button type="reset" id="cancel" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div> 
            </div>
        </form>
            
        </div>
        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="titlemodal" class="title" id="largeModalLabel">Chassis Detail</h4>
                    </div>
                    <div class="modal-body"> 
                        <table  id="datatable-ajax" style="width: 100%" class="table table-hover js-basic-example dataTable table-custom">
                            <thead class="thead-dark">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Chassis</th> 
                                    <th>Action</th>
                                </tr>
                            </thead> 
                            <tbody>
                                 
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
        <div class="modal fade" id="largeModalunit" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="titlemodal" class="title" id="largeModalLabel">Nama Unit</h4>
                    </div>
                    <div class="modal-body"> 
                        <table  id="datatable-ajax2" style="width: 100%" class="table table-hover js-basic-example dataTable table-custom">
                            <thead class="thead-dark">
                                <tr>
                                    <th>No</th>
                                    <th>Kode Unit</th> 
                                    <th>Nama Unit</th>  
                                    <th>Action</th>
                                </tr>
                            </thead> 
                            <tbody>
                                 
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </div>

 <script type="text/javascript">   
    var InputSales = {
        table: undefined
    };
    $(document).ready(function ()
    {
        $("#tanggal").datepicker({format:'yyyy-mm-dd'});  

            $("#customer").select2();
           
            $("#varian").select2();
            $("#size").select2();
       
            $("#kategori").select2();
            $("#chassis").select2();
        var buttonsave, buttoncancel, urlpost, urlindex, content;

        buttonsave = $('#submit');
        buttoncancel = $('#cancel');
        urlpost = '<?php echo $url_post; ?>';
        urlindex = '<?php echo $url_index; ?>';
        content = $("#contentdata");


$('.showmodal').click(function() {
   //alert('called'); 
    var id = $('#chassis').val(); 
     //var id=document.getElementById('myModal').getAttribute("data-id"); 
      var handleDataTableDefault = function () {
            if ($("#datatable-ajax").length!==0) {
               InputSales.table = $("#datatable-ajax").DataTable({
                    "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
                        var oSettings = this.fnSettings();
                        var iTotalRecords = oSettings.fnRecordsDisplay();
                        $("#total").val(iTotalRecords);
                        $("td:first", nRow).html(iDisplayIndex +1);
                       return nRow;
                    },
                    "ajax": {
                        "url": '<?php echo $url_gridchassis; ?>',
                        "type": 'POST',
                        "data" : {
                            "id" :id 
                        },
                    },    destroy: true,searching: false,paging: false,
                    "columns": [
                        {"data": "id"},
                        {"data": "nmChassis"}, 
                        {
                            "data": "id", "width": "100px", "sClass": "left",
                            "bSortable": false,
                            "mRender": function (data, type, row) {
                                var btn = "";
                               
                                btn = btn + " <button data-dismiss='modal' onClick='ParamFunc.editdata(" + row.id + ")' class='btn btn-xs btn-icon btn-circle btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit Data' type='button'><i class='fa fa-edit'></i> </button>"; 
                                btn = btn + "</div>";
                                return btn;
                            }
                        }
                    ]
                });
            }
        };
        TableManageDefault = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableDefault();
                }
            };
        }();
        TableManageDefault.init();
   ('#largeModal').modal('show'); 
});
$('.showmodalunit').click(function() {
   //alert('called'); 
    var id = $('#chassisdetail').val(); 
     //var id=document.getElementById('myModal').getAttribute("data-id"); 
      var handleDataTableDefault = function () {
            if ($("#datatable-ajax2").length!==0) {
               InputSales.table = $("#datatable-ajax2").DataTable({
                    "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
                        var oSettings = this.fnSettings();
                        var iTotalRecords = oSettings.fnRecordsDisplay();
                        $("#total").val(iTotalRecords);
                        $("td:first", nRow).html(iDisplayIndex +1);
                       return nRow;
                    },
                    "ajax": {
                        "url": '<?php echo $url_gridbarang; ?>',
                        "type": 'POST',
                        "data" : {
                            "chassis_d" :id 
                        },
                    },    destroy: true,searching: false,paging: false,
                    "columns": [
                        {"data": "id"},
                        {"data": "kdBarang"}, 
                        {"data": "nmBarang"},  
                        {
                            "data": "id", "width": "100px", "sClass": "left",
                            "bSortable": false,
                            "mRender": function (data, type, row) {
                                var btn = "";
                               
                                btn = btn + " <button data-dismiss='modal' onClick='ParamFunc.editdatabarang(" + row.id + ")' class='btn btn-xs btn-icon btn-circle btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit Data' type='button'><i class='fa fa-edit'></i> </button>"; 
                                btn = btn + "</div>";
                                return btn;
                            }
                        }
                    ]
                });
            }
        };
        TableManageDefault = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableDefault();
                }
            };
        }();
        TableManageDefault.init();
   ('#largeModal').modal('show'); 
});

        buttonsave.click(
                function ()
                { 
                    $.ajax(
                            {
                                type: "POST",
                                url: urlpost,
                                dataType: "json",
                                data: {
                                    id: $("#id").val(), 
                                    kategori: $("#kategori").val(),
                                    chassis: $("#chassis").val(),
                                    size: $("#size").val(),
                                    varian: $("#varian").val(), 
                                    //namaUnitshow: $("#namaUnitshow").val(),   
                                     namaUnit: $("#namaUnit").val(),  
                                    chassisdetail: $("#chassisdetail").val(),
                                    tipe: $("#tipe").val(),
                                    ckatageri: $("#ckatageri").val()
                                },
                                cache: false,
                                success:
                                        function (data, text)
                                        {
                                            if (data.hasil == 'true') {
                                                content.fadeOut("slow", "linear");
                                                window.top.location.href = 'index.php#Inputbarang'; 
                                                content.fadeIn("slow");
                                            } else {
                                                alert(data.msg);

                                            }
                                        },
                                error: function (request, status, error) {
                                    alert(request.responseText + " " + status + " " + error);
                                }
                            });
                    return false;

                });

        buttoncancel.click(
                function ()
                {
                    content.fadeOut("slow", "linear");
                    window.top.location.href = 'index.php#Inputbarang'; 
                    content.fadeIn("slow");

                }); 

    });
    var ParamFunc = {
        editdata: function (id) {
            $.ajax(
                {
                    type: "POST",
                    url: '<?php echo $url_getchassis; ?>',
                    dataType: "json",
                    data: {
                        id: id, 
                    },
                    cache: false,
                    success:
                            function (data, text)
                            {
                                if (data.hasil == 'true') { 
                                    var nmChassis=data.nmChassis;
                                    $('#nmchassisdetail').val(nmChassis);
                                    $('#chassisdetail').val(id); 
                                } else {
                                    alert('Gagal dapat detail');

                                }
                            } 
                });
        },
        editdatabarang: function (id) {
            $.ajax(
                {
                    type: "POST",
                    url: '<?php echo $url_getbarang; ?>',
                    dataType: "json",
                    data: {
                        id: id, 
                    },
                    cache: false,
                    success:
                            function (data, text)
                            {
                                if (data.hasil == 'true') { 
                                    var nmBarang=data.nmBarang;
                                    var kdBarang=data.kdBarang;
                                    $('#namaUnitshow').val(nmBarang);
                                    $('#namaUnit').val(kdBarang);
                                } else {
                                    alert('Gagal dapat detail');

                                }
                            } 
                });
        }
    }
</script> 