<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Inputsales extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_Inputsales', 'table01'); 
        $this->load->model('Mm_Calculation', 'table03'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('Inputsales/grid'),
            "url_add" => site_url('Inputsales/addSales'),
            "url_edit" => site_url('Inputsales/edit'),
            "url_delete" => site_url('Inputsales/remove'),
        );
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='View Sales Table'; 
        $log_trans='SalesTable'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("view", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->load->view('vm_inputsales/home', $data); 
        // $this->load->view('vm_inputsales/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        echo json_encode(array(
            "data" => $this->table01->getGridData()->result()
        ));
    }

    function addSales() {
        $data['title'] = 'Add - Input Sales'; 
        $data['tanggal'] = ''; 
        $data['nmchassisdetail'] = ''; 
        $data['chassisdetail'] = ''; 
        $data['namaUnitshow'] = ''; 
        $data['namaUnit'] = ''; 
        $data['qty'] = ''; 
        $data['unitprice'] = ''; 
        $data['qty2'] = ''; 
        $data['field'] = '';     
        $data['readset']='';   
        $data['disabled']='';
        $resulcustomers = $this->array->data_cust();
        $e = 0;
        foreach ($resulcustomers as $rowcust) {              
            $data['default']['customer'][$e]['value'] = $rowcust['idCust'];
            $data['default']['customer'][$e]['display'] = $rowcust['NmCust2']; 
            $e++;
        } 
        $resulunit = $this->array->data_barang();
        $e = 0;
        foreach ($resulunit as $rowunit) {              
            $data['default']['namaUnit'][$e]['value'] = $rowunit['kdBarang'];
            $data['default']['namaUnit'][$e]['display'] = $rowunit['nmBarang']; 
            $e++;
        } 
        $resulvarian = $this->array->data_varian();
        $e = 0;
        foreach ($resulvarian as $rowvarian) {              
            $data['default']['varian'][$e]['value'] = $rowvarian['id'];
            $data['default']['varian'][$e]['display'] = $rowvarian['varian']; 
            $e++;
        }  
        $resulsize = $this->array->data_size();
        $e = 0;
        foreach ($resulsize as $rowsize) {              
            $data['default']['size'][$e]['value'] = $rowsize['id'];
            $data['default']['size'][$e]['display'] = $rowsize['size']; 
            $e++;
        } 
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis']; 
            $e++;
        } 
        $resulchassisdetail= $this->array->data_chassisdtl();
        $e = 0;
        foreach ($resulchassisdetail as $rowchassisdtl) {              
            $data['default']['chassisdetail'][$e]['value'] = $rowchassisdtl['id'];
            $data['default']['chassisdetail'][$e]['display'] = $rowchassisdtl['nmChassis']; 
            $e++;
        }
        $resulkategori = $this->array->data_kategori();
        $e = 0;
        foreach ($resulkategori as $rowkategori) {              
            $data['default']['kategori'][$e]['value'] = $rowkategori['id'];
            $data['default']['kategori'][$e]['display'] = $rowkategori['kategori']; 
            $e++;
        } 
        $resultipe= $this->array->data_tipe();
        $e = 0;
        foreach ($resultipe as $rowtipe) {              
            $data['default']['tipe'][$e]['value'] = $rowtipe['id'];
            $data['default']['tipe'][$e]['display'] = $rowtipe['tipe'];  
            $e++;
        } 
        $resulkategorisize = $this->array->data_kategorisize();
        $e = 0;
        foreach ($resulkategorisize as $rowkategorisize) {              
            $data['default']['ckatageri'][$e]['value'] = $rowkategorisize['id'];
            $data['default']['ckatageri'][$e]['display'] = $rowkategorisize['keterangan']; 
            $e++;
        }   
        $data['url_post'] = site_url('Inputsales/addpost'); 
        $data['url_gridchassis'] = site_url('Inputsales/gridchassis');  
        $data['url_getchassis'] = site_url('Inputsales/getchassis');
        $data['url_gridbarang'] = site_url('Inputsales/gridbarang');  
        $data['url_getbarang'] = site_url('Inputsales/getbarang');  
        $data['url_index'] = site_url('Inputsales'); 
        $data['id'] = 0; 

        $this->load->view('vm_inputsales/form', $data); 
    }
    public function gridchassis() {
         $id = $this->input->post('id'); 
        echo json_encode(array(
            "data" => $this->table01->getGridDatachassis($id)->result()
        ));
    }
    public function getchassis() {
            $id = $this->input->post('id'); 
            $row = $this->table01->getchassisby_id($id)->row();  
            $jsonmsg = array( 
                "hasil" => 'true', 
                "nmChassis"=>$row->nmChassis
            );
             echo json_encode($jsonmsg);
        }
    public function gridbarang() {
         $id = $this->input->post('chassis_d'); 
        echo json_encode(array(
            "data" => $this->table01->getGridDatabarang($id)->result()
        ));
    }
    public function getbarang() {
            $id = $this->input->post('id'); 
            $row = $this->table01->getbarangby_id($id)->row();  
            $jsonmsg = array( 
                "hasil" => 'true', 
                "data"=>$row
            );
             echo json_encode($jsonmsg);
        }
    /* fungsi untuk post data ketika melakukan add data, fungsi ini akan masuk ke database */

    public function addpost() {
        $user = $this->session->userdata('ses_userName'); 
         $id = $this->input->post('id'); 
            $kategori = $this->input->post('kategori');  
            $chassis = $this->input->post('chassis'); 
            //$tipe = $this->input->post('tipe'); 
            $size = $this->input->post('size');
            $varian = $this->input->post('varian');
            $unitprice = $this->input->post('unitprice'); 
            $qty = $this->input->post('qty');
            $namaUnit = $this->input->post('namaUnit');
            $customer = $this->input->post('customer');
            $tanggal2 = $this->input->post('tanggal');
            $chassisdetail = $this->input->post('chassisdetail'); 
           // $ckatageri = $this->input->post('ckatageri');

            $rowgroup = $this->table01->getby_idvarian($varian)->row();

            $groupV=$rowgroup->group; 
             
            $row = $this->table01->getby_idsize($size)->row(); 
            $tsize=$row->size;
            $splitsize = explode('L', $tsize); 
            $size2 = $splitsize[0]; 
            // $splitsize = explode('\n',$sizesplit); 
            // $size2 = $splitsize[0];  
            if($varian>=4){
                $tipe=2;
            }
            elseif($varian<4){
                $tipe=1;
            }
            else{
                $tipe=0;
            } 
            if($size2>8000){
                $ckatageri=1;
            }
            elseif($size2>=6000 && $size2<=8000){
                $ckatageri=3;
            }
            elseif($size2=='3M3' && $size2=='4M3'){
                $ckatageri=2;
            }
            elseif($size2=='6M3' && $size2=='5M3'){
                $ckatageri=3;
            }
            elseif($size2=='7M3' && $size2=='8M3'){
                $ckatageri=1;
            }
            else{
                $ckatageri=2;
            }
            $tanggal=$tanggal2.'-30';
            // var_dump($sizesplit);
            // var_dump($size2);
            // die(); 
            $split = explode('-', $tanggal);
            $periode = $split[0].$split[1];  
            $tahun = $split[0];  
            $total=$qty*$unitprice;
            $record = array(
                "tahun" => $tahun,
                "id_tipe" => $tipe,
                "kategori_id" => $kategori,
                "periode" => $periode,
                "chassis_id" => $chassisdetail,
                "size_id" => $size, 
                "varian_id" => $varian,
                "unit_price" => $unitprice, 
                "qty" => $qty, 
                "UTotal" => $total, 
                "kdBarang" => $namaUnit,
                "idCust" => $customer,
                "tgl_sales" => $tanggal,
                "kat_size" => $ckatageri,
                "crtBy" => $user,
                "updBy" => $user,
                "group_v" =>$groupV,
                "status" =>0
            );
        $this->table01->insert($record);
        if($kategori=='wb'){
  
            $row2 = $this->table03->getby_id($tahun,$ckatageri,$varian)->row();  
            $qtyTotal=$row2->field; 
            // $qty=$row2->qty;
            $qtyamount=$total;
            $tipe=$row2->id_tipe;  
            // var_dump($ckatageri); 
            // var_dump($qty);
            // var_dump($total);
            // var_dump($ckatageri);
            // var_dump($varian);
            // die();
            $this->table01->updatetransaksi($tahun,$ckatageri,$qtyTotal,$qty); //ddd
            $this->table01->updatetransaksiamount($tahun,$ckatageri,$qtyTotal,$qtyamount);// eee
            $this->table01->updatetransaksitipe($tahun,$tipe,$qty,$qtyamount);//eee
            // $this->table02->updatetransaksitipe($tahun,$tipe,$qtyamount,$qtyTotal);
           // $this->table01->updatesalesstatus($tahun,$ckatageri,$varian);
        }  
        // $this->table01->updatesalesstatus($tahun,$ckatageri,$varian);
         $valid = 'true';
         $message = 'data insert';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Add Sales Table'; 
        $log_trans='Add'.$periode.'-'.$namaUnit; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("add", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi edit ini akan mensetting nilai-nilai di form ketika mengklik tombol edit */

    function edit($id) {
        $row = $this->table01->getby_id($id)->row();  
        $data['nmchassisdetail'] =$row->nmChassis;  
        $data['chassisdetail'] =$row->chassis_id;  
        $data['namaUnitshow'] =$row->nmBarang;  
        $data['namaUnit'] =$row->kdBarang;    
        $data['qty'] =$row->qty;     
        $data['qty2'] =$row->qty;    
        $data['unitprice'] =$row->unit_price;  
        $variand=$row->varian_id;    
        $rowvariandetail = $this->table01->getby_idvarian($variand)->row();
        $data['field'] =$rowvariandetail->field; 
        $data['readset']="readonly='readonly'";
        $data['disabled']='disabled'; 
        $resulcustomers = $this->array->data_cust();

        $var = $row->tgl_sales;
        $date = str_replace('/', '-', $var);
        $date2 = date('Y-m', strtotime($date));

        $data['tanggal'] =$date2;  
        $e = 0;
        foreach ($resulcustomers as $rowcust) {              
            $data['default']['customer'][$e]['value'] = $rowcust['idCust'];
            $data['default']['customer'][$e]['display'] = $rowcust['NmCust2']; 
            if ($row->idCust == $rowcust['idCust']) {
                $data['default']['customer'][$e]['selected'] = "SELECTED";
            }
            $e++;
        }   
        $resulchassis= $this->array->data_chassis();
        $e = 0;
        foreach ($resulchassis as $rowchassis) {              
            $data['default']['chassis'][$e]['value'] = $rowchassis['id'];
            $data['default']['chassis'][$e]['display'] = $rowchassis['chassis']; 
            if ($row->chassis_h == $rowchassis['id']) {
                $data['default']['chassis'][$e]['selected'] = "SELECTED";
            }
            $e++;
        }  
        $resultipe= $this->array->data_tipe();
        $e = 0;
        foreach ($resultipe as $rowtipe) {              
            $data['default']['tipe'][$e]['value'] = $rowtipe['id'];
            $data['default']['tipe'][$e]['display'] = $rowtipe['tipe']; 
            if ($row->id_tipe == $rowtipe['id']) {
                $data['default']['tipe'][$e]['selected'] = "SELECTED";
            }
            $e++;
        } 
        $resulvarian = $this->array->data_varian();
        $e = 0;
        foreach ($resulvarian as $rowvarian) {              
            $data['default']['varian'][$e]['value'] = $rowvarian['id'];
            $data['default']['varian'][$e]['display'] = $rowvarian['varian'];
            if ($row->varian_id == $rowvarian['id']) {
                $data['default']['varian'][$e]['selected'] = "SELECTED";
            } 
            $e++;
        }  
        $resulsize = $this->array->data_size();
        $e = 0;
        foreach ($resulsize as $rowsize) {              
            $data['default']['size'][$e]['value'] = $rowsize['id'];
            $data['default']['size'][$e]['display'] = $rowsize['size'];
            if ($row->size_id == $rowsize['id']) {
                $data['default']['size'][$e]['selected'] = "SELECTED";
            }
            $e++;
        } 
        $resulkategori = $this->array->data_kategori();
        $e = 0;
        foreach ($resulkategori as $rowkategori) {              
            $data['default']['kategori'][$e]['value'] = $rowkategori['id'];
            $data['default']['kategori'][$e]['display'] = $rowkategori['kategori']; 
            if ($row->kategori_id == $rowkategori['id']) {
                $data['default']['kategori'][$e]['selected'] = "SELECTED";
            }
            $e++;
        } 
        $resulkategorisize = $this->array->data_kategorisize();
        $e = 0;
        foreach ($resulkategorisize as $rowkategorisize) {              
            $data['default']['ckatageri'][$e]['value'] = $rowkategorisize['id'];
            $data['default']['ckatageri'][$e]['display'] = $rowkategorisize['keterangan']; 
            if ($row->kat_size == $rowkategorisize['id']) {
                $data['default']['ckatageri'][$e]['selected'] = "SELECTED";
            }
            $e++;
        }   
        $data['url_post'] = site_url('Inputsales/editpost'); 
        $data['url_gridchassis'] = site_url('Inputsales/gridchassis');  
        $data['url_getchassis'] = site_url('Inputsales/getchassis');
        $data['url_gridbarang'] = site_url('Inputsales/gridbarang');  
        $data['url_getbarang'] = site_url('Inputsales/getbarang');  
        $data['url_index'] = site_url('Inputsales'); 
        $data['id'] = $id; 
        $this->load->view('vm_inputsales/form', $data); 
    }

    /* fungsi untuk post data ketika melakukan edit data, fungsi ini akan masuk ke database */

    function editpost() {
        $user = $this->session->userdata('ses_userName'); 
        $id = $this->input->post('id'); 
            //$tipe = $this->input->post('tipe');  
            $kategori = $this->input->post('kategori');  
            $chassis = $this->input->post('chassis'); 
            $size = $this->input->post('size');
            $varian = $this->input->post('varian');
            $unitprice = $this->input->post('unitprice'); 
            $qty = $this->input->post('qty');
            $qty2 = $this->input->post('qty2');
            $field = $this->input->post('field');
            $namaUnit = $this->input->post('namaUnit');
            $customer = $this->input->post('customer');
            $tanggal2 = $this->input->post('tanggal');
            $chassisdetail = $this->input->post('chassisdetail'); 
            $ckatageri = $this->input->post('ckatageri');
            if($varian>=4){
                $tipe=2;
            }
            else{
                $tipe=1;
            }
            $tanggal=$tanggal2.'-30';
            $split = explode('-', $tanggal);
            $periode = $split[0].$split[1];  
            $total=$qty*$unitprice; 
            $tahun = $split[0];  
            // var_dump($periode);
            // die();
            $datetime=date("Y-m-d h:i:s");
            $record = array(
                "tahun" => $tahun,
                "id_tipe" => $tipe,
                "kategori_id" => $kategori,
                "periode" => $periode,
                "chassis_id" => $chassisdetail,
                "size_id" => $size, 
                "varian_id" => $varian,
                "unit_price" => $unitprice, 
                "qty" => $qty, 
                "UTotal" => $total, 
                "kdBarang" => $namaUnit,
                "idCust" => $customer,
                "tgl_sales" => $tanggal,
                "kat_size" => $ckatageri,  
                "UpdDt" => $datetime, 
                "updBy" => $user,
                "status" =>0
            );
        $this->table01->update($id,$record); 
        $row = $this->table01->getby_idvarian($varian)->row();
        $qtyTotal=$row->field;  
        $this->table01->updatetransaksidetail($tahun,$ckatageri,$qtyTotal,$field,$qty,$qty2);
         $valid = 'true';
         $message = 'data update';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Edit Sales Table'; 
        $log_trans='Edit'.$periode.'-'.$namaUnit; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("edit", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi untuk delete data */

    public function remove() {
        $id = $this->input->post('id');  
        $row = $this->table01->getby_id($id)->row();    
        $periode=$row->periode;        
        $namaUnit=$row->kdBarang;       
        $varian=$row->varian_id;      
        $tahun=$row->tahun;        
        $variand=$row->varian_id;    
        $ckatageri=$row->kat_size;    
        $tipe=$row->id_tipe;    
        $qty=$row->qty;    
        $row = $this->table01->getby_idvarian($varian)->row();
        $qtyTotal=$row->field;  
        $this->table01->delete($id);
        $this->table01->updatetransaksiremove($tahun,$ckatageri,$qtyTotal,$qty);
        $this->table01->updatetransaksitiperemove($tahun,$tipe,$qtyTotal,$qty);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $user = $this->session->userdata('ses_userName'); 
        $usergroup= $this->session->userdata('ses_aktor'); 
        $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='Hapus Sales Table'; 
        $log_trans='Hapus'.$periode.'-'.$namaUnit; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("hapus", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

}
