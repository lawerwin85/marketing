<div class="container">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2>  Dashboard  Wing Box by Amount</h2> 
            </div>            
            <div class="col-lg-6 col-md-4 col-sm-12 text-right">
                
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card"> 
                <div class="body">                            
                   <div id="barbyperiode" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>            
    </div>
    <div class="row clearfix">
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body">                            
                    <div id="containerkat" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body todo_list">
                     <div id="containertipe" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        </div>                
    </div>
    <div class="row clearfix">
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body">                            
                    <div id="containerQ1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body todo_list">
                     <div id="containerQ2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        </div>                
    </div>
    <div class="row clearfix">
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body">                            
                    <div id="containerQ3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body todo_list">
                     <div id="containerQ4" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        </div>                
    </div>
    <div class="row clearfix">
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body">                            
                    <div id="containerQ5" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body todo_list">
                     <div id="containerQ7" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        </div>                
    </div>
    <div class="row clearfix">
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body">                            
                    <div id="containerQ8" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body todo_list">
                     <div id="containerQA" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        </div>                
    </div>
    <div class="row clearfix">
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body">                            
                    <div id="containerCT" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="card"> 
                <div class="body todo_list">
                     <div id="containerwb" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        </div>                
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card">   
                <div class="body"> 
                    <div class="col-lg-2 col-md-2" style="text-align: right">  
                        <select id="tahun" class="form-control tahun" name="tahun"> 
                            <option value="0" selected="selected">-- Pilih Tahun --</option>
                           <?php print_r($default['tahun']); foreach ($default['tahun'] as $row) { ?>
                                    
                                    <option data-id="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" value="<?php echo (isset($row['value'])) ? $row['value'] : ''; ?>" 
                                            <?php echo (isset($row['selected'])) ? $row['selected'] : ''; ?> >
                                        <?php echo (isset($row['display'])) ? $row['display'] : ''; ?></option>
                                <?php } ?>
                        </select> 
                        <input type="hidden" readonly="readonly" id="kategori" name="kategori" value="wb"> 
                    </div>                           
                   <div id="containerKatbuyer" style="min-width: 310px; height: 800px; margin: 0 auto"></div>
                </div>
            </div>
        </div>            
    </div> 
  <!--   <div class="row clearfix">
        <div class="col-lg-12 col-md-12">
            <div class="card"> 
                <div class="body">                            
                   <div id="containerFullchassis" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>            
    </div>  -->
    </div>
</div> 
<script src="<?php echo $base; ?>assets/js/amount/index.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchart.js"></script> 
<script src="<?php echo $base; ?>assets/js/barbuyerkatamount.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartbyperiod.js"></script> 
<script src="<?php echo $base; ?>assets/js/amount/barchart2.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchart3.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barcharttipe.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartQ1.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartQ2.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartQ3.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartQ4.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartQ5.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartQ7.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartQ8.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartQA.js"></script> 
<script src="<?php echo $base; ?>assets/js/amount/barchartCT.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/barchartwb.js"></script>
<!-- <script src="<?php echo $base; ?>assets/js/amount/barchartchassis.js"></script> -->
<!-- <script src="<?php echo $base; ?>assets/js/amount/barchartFull.js"></script>  -->
<script src="<?php echo $base; ?>assets/js/amount/piechart.js"></script>
<script src="<?php echo $base; ?>assets/js/amount/clickchart.js"></script>