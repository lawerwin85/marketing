<?php

/* membuat class dengan nama Magama_model*/
class Mm_Cekperiode extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "chassis_detail"; 

    }
 
    function getM_tahun() { 
        $query = "SELECT IFNULL(MAX(tahun),0) tahun FROM m_tahun ";
        return $this->db->query($query);      
    }
    function getM_tahunkat() {
        $year=date("Y");
        $query = "SELECT IFNULL(MAX(tahun),0) tahun FROM m_tahunkat ";
        return $this->db->query($query);      
    }
    function getM_tahunTipe() {
        $year=date("Y");
        $query = "SELECT IFNULL(MAX(tahun),0) tahun FROM m_tahuntipe ";
        return $this->db->query($query);      
    }
    function gettransaksi_salesqty() {
        $year=date("Y");
        $query = "SELECT IFNULL(MAX(tahun),0) tahun FROM transaksi_sales ";
        return $this->db->query($query);      
    }
    function gettransaksi_tipe() {
        $year=date("Y");
        $query = "SELECT IFNULL(MAX(tahun),0) tahun FROM transaksi_salestipe ";
        return $this->db->query($query);      
    }
    function updateM_tahun($id) {
        $query = "INSERT INTO m_tahun (tahun) values ('$id')";
        return $this->db->query($query);  
    }
    function updateM_tahunkat($id) {
        $query = " INSERT INTO m_tahunkat (tahun, kat_size) 
                    SELECT $id,id FROM categorysize;";
        return $this->db->query($query);  
    }
    function updateM_tahuntipe($id) {
        $query = " INSERT INTO m_tahuntipe (tahun, id_tipe) 
                    SELECT $id,id FROM tipe;";
        return $this->db->query($query);  
    }
    function update_transaksi_sales($id) {
        $query = " INSERT INTO transaksi_sales (tahun, id_category) 
                    SELECT $id,id FROM categorysize;";
        $query2 = " INSERT INTO transaksi_salesamount (tahun, id_category) 
                    SELECT $id,id FROM categorysize;";
                    $this->db->query($query2);  
        return $this->db->query($query);  
    } 
    function update_transaksi_tipe($id) {
        $query = " INSERT INTO transaksi_salestipe (tahun, id_tipe) 
                    SELECT $id,id FROM tipe;";    
        return $this->db->query($query);  
    } 
}
// INSERT INTO transaksi_chassis (tahun, idchassis,qty,totalV) 
// SELECT 2013,c.id,IFNULL(SUM(s.qty), 0),IFNULL(SUM(s.Utotal), 0) FROM chassis c 
// INNER JOIN chassis_detail d ON c.id=d.idchassis 
// LEFT JOIN sales s ON d.id=s.chassis_id AND s.tahun=2013 GROUP BY c.chassis; 


// INSERT INTO transaksi_chassis (tahun, idchassis,qty,totalV) 
// SELECT 2013,c.id,IFNULL(SUM(s.qty), 0),IFNULL(SUM(s.Utotal), 0) FROM chassis c 
// INNER JOIN chassis_detail d ON c.id=d.idchassis 
// LEFT JOIN sales s ON d.id=s.chassis_id AND s.tahun=2013
// where kategori_id in('wb') GROUP BY c.chassis; 