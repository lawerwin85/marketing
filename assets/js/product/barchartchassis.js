var urlchassis = urlpost+'Dash/gridsalesqtychassis';
 $(function () { 
        function graphbar(callback){ 
              
                $.ajax({
                    url: urlchassis, 
                    dataType: 'json', 
                    type:'POST',  
                    data: {
                        periode: '2019', 
                    },
                    success: function(data){    
                        var datas=data.nilaissales;  
                        var chassis=data.chassis;  
                        callback({databar:datas,chassisd:chassis}); 
                    } 
                }); 
        } 
        graphbar(function(res){
            Array.prototype.contains = function(v) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === v) return true;
            }
            return false;
        };

        Array.prototype.unique = function() {
            var arr = [];
            for(var i = 0; i < this.length; i++) {
                if(!arr.includes(this[i])) {
                    arr.push(this[i]);
                }
            }
            return arr; 
        } 
            var list=res.databar; 
            var listchassis=res.chassisd; 
            var periodebar=[];    
            var groups = {}; 
            // for (var i = 0; i < listchassis.length; i++) {    
            //       var nmchassis=listchassis[i].chassis;
            //       var groupName2 = listchassis[i].chassis;  
            //      if (!groups[groupName2]) {
            //                 groups[groupName2] = [];
            //               }  

            // }    
            for (var i = 0; i < list.length; i++) {   
                  var periode =list[i].tahun; 
                        var listnilai=list[i].qty;
                        var nilai =Number(listnilai);
                        var groupName2 = list[i].chassis; 
                        if (!groups[groupName2]) {
                            groups[groupName2] = [];
                          }  

                        periodebar.push(periode);   
                          groups[groupName2].push(nilai); 
            }
                        myArray = [];
                        for (var groupName2 in groups) { 
                          myArray.push({type: 'column',name: groupName2, data: groups[groupName2]});
                      //  console.log(groups[groupName]);
                        }     
                        myArray.push({
                            type: 'spline',
                            showInLegend: false,  
                            data: groups[groupName2],
                            name: curvetrend,
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[3],
                                fillColor: 'white'
                            }
                        });
        var uniques = periodebar.unique(); 
        uniques.sort();  
 
            //console.log(periodebar);
                Highcharts.chart('containerFullchassis', {
                
                title: {
                    text: 'CHASSIS WING BOX BASED ON QTY',
                    enabled:false
                },
                exporting: {
                            enabled:false
                        },
                subtitle: {
                    text:null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: uniques 
                },
                yAxis: {
                    min: 0,
                    title: {
                        text:QTY
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                   //alert('Category: ' + this.category + ', value: ' + this.series.name);  
                                  window.open ('#Reportdash?category=chassis&tahun='+this.category+'&kat='+this.series.name+'&varian=all','_blank'); 
                                }
                            }
                        }
                    }
                },
                series:myArray
            });
        });    
})