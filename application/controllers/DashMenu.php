<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DashMenu extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('Ms_dashboard', 'dash');
        $this->is_logged();
    }

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    public function index() {
        $data = array(
            "base" => base_url(),
            "site" => site_url(),  
        ); 
        $id = $this->session->userdata('ses_user_id'); 
       
        //var_dump($idpost);
         //die();
        $userGroupId = $this->dash->getUserGroupId2($id)->row();
        $groupId=$userGroupId->users_id;
        $idpost = $this->input->post('idpost');
        $checkparent = $this->dash->checkparent($idpost,$id); //cek menu parent_id
         // var_dump($userGroupId);
         //        die();
        if($checkparent > 0){
            $getapp = $this->dash->getMenuApp($idpost,$id)->row(); 
                $idApp=$getapp->id; 
                $app = $this->dash->getMenu2($idApp, $groupId, $id);
                $getonlyMenu = $this->dash->getonlyMenu($idApp, $groupId, $id)->row();
                $getonlynumrows = $this->dash->getonlynumrows($idApp, $groupId, $id);
                if ($getonlynumrows > 1) {
                    $geturl=$getonlyMenu->url; 
                    }
                else{
                        $geturl=0;
                    }
                   
                $app2 = $this->dash->getMenu3($idApp, $groupId, $id);
                $checkdata = $this->dash->checkdata($idpost,$id); 
                $dashboard = array( 
                            "appName" =>0,
                            "app_id" =>0         
                        );
                    if ($checkdata < 1) {
                        $fsonmsg = array(
                            "msg" => 'You not have menu authority', 
                            "status" =>0
                        );
                    }
                    else{
                        if($app2 < 1 ){
                           $fsonmsg = array(
                                "msg" => 'Ini Menu Dashboard', 
                                "status" => 3,
                                "data" => $dashboard,
                                "getonlyMenu" => $geturl 
                            ); 
                        }
                        else{
                            $fsonmsg = array(
                            "msg" => 'Get Data Succces', 
                            "status" => 1,
                            "data" => $app,
                            "getonlyMenu" => $geturl
                            );
                        } 
                    }
        }
        else{
            
           $getapp = $this->dash->getMenuApp2($idpost,$id)->row(); 
                $idApp=$getapp->parent_id; 
                $app = $this->dash->getMenu2($idApp, $groupId, $id);
                $getonlyMenu = $this->dash->getonlyMenu($idApp, $groupId, $id)->row();
                $getonlynumrows = $this->dash->getonlynumrows($idApp, $groupId, $id);
                if ($getonlynumrows > 1) {
                        $geturl=$getonlyMenu->url;
                    }
                else{
                        $geturl=0;
                    }
                $app2 = $this->dash->getMenu3($idApp, $groupId, $id);
                $checkdata = $this->dash->checkData2($idpost,$id); 
               
                $dashboard = array( 
                            "appName" =>0,
                            "app_id" =>0         
                        );
                    if ($checkdata < 1) {
                        $fsonmsg = array(
                            "msg" => 'You not have menu authority', 
                            "status" =>0,
                            "cek" =>$idApp
                        );
                    }
                    else{
                        if($app2 < 1 ){
                           $fsonmsg = array(
                                "msg" => 'Ini Menu Dashboard', 
                                "status" => 3,
                                "data" => $dashboard,
                                "getonlyMenu" => $geturl 
                            ); 
                        }
                        else{
                            $fsonmsg = array(
                            "msg" => 'Get Data Succces', 
                            "status" => 1,
                            "data" => $app,
                            "getonlyMenu" => $geturl
                            );
                        } 
                    }
        }
          
       

          echo json_encode($fsonmsg); 
         
    }  
}
?>