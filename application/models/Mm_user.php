<?php

/* membuat class dengan nama Magama_model*/
class Mm_user extends CI_Model {
    
     /* membuat encapsulasi untuk properties %table */
    private $table;

    public function __construct() {
        parent::__construct();
        $this->table = "users"; 
        $this->table2 = "user_group_users"; 
        $this->table3 = "user_group";

    }

     /* mendapatkan semua data dan hasilnya sebuah array */
    public function getAll() {
        return $this->db->get($this->table)->result_array();
    }
  
    function getGridData($userId) {
        $query = "SELECT b.userName,g.g_name,g.g_desc,b.userId,IFNULL(latestDate,0)latestDate
                FROM users b 
                LEFT JOIN user_group_users u ON u.users_id=b.userId
                LEFT JOIN user_group g ON g.id=u.user_group_id  
                LEFT JOIN(SELECT log_user,MAX(log_time) AS latestDate
                FROM _log
                GROUP BY login_id) l ON l.log_user=b.userName  
                group by b.userId
                order by b.userid ASC";
        return $this->db->query($query);      
    } 
  
    function getGridDatagroup() {
        $query = "SELECT * from user_group order by id ASC";
        return $this->db->query($query);      
    } 
    function getby_id($id) {
        $query = "SELECT b.userName,b.userPassword,g.id idgroup,g.g_desc,b.userId 
                FROM users b 
                LEFT JOIN user_group_users u ON u.users_id=b.userId
                LEFT JOIN user_group g ON g.id=u.user_group_id 
                WHERE b.userId = '$id'";
        return $this->db->query($query);   
    }  
    function getby_idgroup($id) {
        $query = "SELECT * from user_group where id = '$id'";
        return $this->db->query($query);   
    }   

    function cek_namegroup($groupsmall){
        $this->db->where("g_name",$groupsmall);
        $result= $this->db->get($this->table3)->num_rows();  
        return $result;
    }
    function insert($record) {
        $this->db->insert($this->table, $record); 
        return $this->db->insert_id();
    }  
    function insertgroup($recordgrup) {
        $this->db->insert($this->table2, $recordgrup);
    } 
    function insertgroupuser($record) {
        $this->db->insert($this->table3, $record);
    } 
    function updategroup($id,$group,$userid) {
        $query = "UPDATE user_group_users set user_group_id=$group ,Createby='$userid',CreateTime=Now() where users_id=$id";
        return $this->db->query($query);   
    }  
    function update($id, $record) {
        $this->db->where("userId", $id);
        $this->db->update($this->table, $record);
    }
    function updategroupuser($id, $record) {
        $this->db->where("id", $id);
        $this->db->update($this->table3, $record);
    }  
    function delete($id) {
        $this->db->delete($this->table, array("userId" => $id)
        );
    }
    function deletegroup($id) {
        $this->db->delete($this->table3, array("id" => $id)
        );
    }

}
