var urlfull = urlpost+'Dashutlamount/gridsalesqtyfull';
 $(function () { 
        function graphbar(callback){ 
              
                $.ajax({
                    url: urlfull, 
                    dataType: 'json', 
                    type:'POST', 
                    data: {
                        datavarian: 'qtyTotalFull', 
                    },
                    success: function(data){    
                        var datas=data.nilaissales;    
                        callback({databar:datas}); 
                    } 
                }); 
        } 
        graphbar(function(res){
            Array.prototype.contains = function(v) {
            for(var i = 0; i < this.length; i++) {
                if(this[i] === v) return true;
            }
            return false;
        };

        Array.prototype.unique = function() {
            var arr = [];
            for(var i = 0; i < this.length; i++) {
                if(!arr.includes(this[i])) {
                    arr.push(this[i]);
                }
            }
            return arr; 
        } 
            var list=res.databar; 
            // console.log(list);
            var varianbar=[];  
            var groups = {}; 
            for (var i = 0; i < list.length; i++) {   
                  var varian =list[i].tahun; 
                  var groupName = list[i].tahun;
                        var listnilaiQ1=Number(list[i].qtyTotalQ1); 
                        var listnilaiQ2=Number(list[i].qtyTotalQ2);
                        var listnilaiQ3=Number(list[i].qtyTotalQ3);
                        var listnilaiQ4=Number(list[i].qtyTotalQ4);
                        var listnilaiQ5=Number(list[i].qtyTotalQ5);
                        var listnilaiQ7=Number(list[i].qtyTotalQ7);
                        var listnilaiQ8=Number(list[i].qtyTotalQ8);
                        var listnilaiQA=Number(list[i].qtyTotalQA);
                        var listnilaiCT=Number(list[i].qtyTotalCT);
                        var listnilaiwb=Number(list[i].qtyTotalwb);
                 varianbar.push(varian); 
                 if (!groups[groupName]) {
                            groups[groupName] = [];
                          }
                          groups[groupName].push(listnilaiQ1);  
                          groups[groupName].push(listnilaiQ2);  
                          groups[groupName].push(listnilaiQ3);  
                          groups[groupName].push(listnilaiQ4);  
                          groups[groupName].push(listnilaiQ5);  
                          groups[groupName].push(listnilaiQ7);  
                          groups[groupName].push(listnilaiQ8);  
                          groups[groupName].push(listnilaiQA);  
                          groups[groupName].push(listnilaiCT);  
                          groups[groupName].push(listnilaiwb);  
            }  
                        myArray = [];
                        for (var groupName in groups) { 
                          myArray.push({type: 'column',name: groupName, data: groups[groupName]});
                      //  console.log(groups[groupName]);
                        }     
                        myArray.push({
                            type: 'spline',
                            showInLegend: false,  
                            data: groups[groupName],
                            name: curvetrend,
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[3],
                                fillColor: 'white'
                            }
                        });
        var uniques = varianbar.unique(); 
        uniques.sort();  

                        console.log(myArray);
                Highcharts.chart('containerFull', {
                
                title: {
                    text: 'VARIANT PRODUCT WING BOX BASED ON QTY ',
                    enabled:false
                },
                exporting: {
                            enabled:false
                        },
                subtitle: {
                    text:null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: uniques 
                },
                yAxis: {
                    min: 0,
                    title: {
                        text:QTY
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series:myArray
            });
        });    
})