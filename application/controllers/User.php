<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller {
  
    function __construct() {
        parent::__construct();

        $this->load->model('Mm_user', 'table01'); 
        $this->load->model('Mm_array', 'array'); 
        $this->is_logged();
    }

    /* fungsi pengecekan user login */

    function is_logged() {
        $this->load->library('session');
        if ($this->session->userdata('ses_statuslogin') != TRUE) {
            redirect('Login', 'refresh');
        }
    }

    /* fungsi index yang di load pertama pada saat controller bagian di akses */

    public function index() {

        $data = array(
            "base" => base_url(),
            "url_grid" => site_url('User/grid'),
            "url_add" => site_url('User/addUser'),
            "url_edit" => site_url('User/edit'),
            "url_delete" => site_url('User/remove'),
            "url_group" => site_url('User/addgroup'),
        );
        $user = $this->session->userdata('ses_userName'); 
         $usergroup= $this->session->userdata('ses_aktor'); 
         $ses_loginId= $this->session->userdata('ses_loginId'); 
        $dash='View User Table'; 
        $log_trans='UserTable'; 
        $log_id=$ses_loginId.$log_trans;

        helper_log("view", $dash,$user,$usergroup,$ses_loginId,$log_id,$log_trans);
        $this->load->view('vm_user/home', $data); 
        // $this->load->view('vm_User/confirm_delete', $data);
    }

    /* fungsi untuk mendapatkan data dan menampilkan di tabel pada file home.php */

    public function grid() {
        $userId = $this->session->userdata('ses_userName');
        echo json_encode(array(
            "data" => $this->table01->getGridData($userId)->result()
        ));
    }  
    public function gridgroup() { 
        echo json_encode(array(
            "data" => $this->table01->getGridDatagroup()->result()
        ));
    }  
    function addUser() { 
        $data['title'] = 'Add - Input User';  
        $resulgroup = $this->array->data_group();
        $e = 0;
        foreach ($resulgroup as $rowgroup) {              
            $data['default']['group'][$e]['value'] = $rowgroup['id'];
            $data['default']['group'][$e]['display'] = $rowgroup['g_desc']; 
            $e++;
        }        
        $data['username'] = '';      
        $data['password'] = '';       
        $data['url_post'] = site_url('User/addpost');   
        $data['url_index'] = site_url('User'); 
        $data['id'] = 0; 

        $this->load->view('vm_user/form', $data); 
    }
    function addgroup() { 
        $data['title'] = 'Add - Input Group User';   
        $data['group'] = '';     
        $data['url_post'] = site_url('User/addpostgroup');   
        $data['url_gridgroup'] = site_url('User/gridgroup'); 
        $data['url_deletegroup'] = site_url('User/removegroup');
        $data['url_index'] = site_url('User'); 
        $data['id'] = 0; 

        $this->load->view('vm_user/formgroup', $data); 
    }
    public function addpostgroup() { 
            $userName= $this->session->userdata('ses_userName');
            $group = $this->input->post('group');   
            // var_dump($kdcust);
            // die();

            $ganti=strtolower($group);
            $groupsmall = str_replace(" ", "_", $ganti); 
            $checkdata = $this->table01->cek_namegroup($groupsmall);
            // var_dump($checkdata);  
            if($checkdata > 0){
                 $valid =false; 
                 $message = 'Nama Ini sudah digunakan';
            }
            else{ 
                 $valid =true; 
                 $message = 'Data disimpan';
                    $record = array(
                        "g_name" => $groupsmall,
                        "g_desc" => $group,  
                        "CreateBy" => $userName,
                        "UpdateBy" => $userName 
                    );
          
                $this->table01->insertgroupuser($record);  
            } 
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }
 
    public function addpost() { 
            $userId = $this->session->userdata('ses_user_id');
            $group = $this->input->post('group');  
            $username = $this->input->post('username'); 
            $password = $this->input->post('password'); 
            $pass = md5($username . $password);
            // var_dump($kdcust);
            // die();
            $record = array(
                "userName" => $username,
                "userPassword" => $pass,  
                "CreateBy" => $userId,
                "UpdateBy" => $userId 
            );
  
        $id=$this->table01->insert($record); 
            $recordgrup = array(
                "users_id" => $id,
                "user_group_id" => $group,  
                "CreateBy" => $userId 
            ); 
        $this->table01->insertgroup($recordgrup); 
         $valid = 'true';
         $message = 'data insert';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    function edit($id) {
        $row = $this->table01->getby_id($id)->row();   
        $resulgroup = $this->array->data_group();
        $e = 0;
        foreach ($resulgroup as $rowgroup) {              
            $data['default']['group'][$e]['value'] = $rowgroup['id'];
            $data['default']['group'][$e]['display'] = $rowgroup['g_desc'];  
            if ($row->idgroup == $rowgroup['id']) {
                $data['default']['group'][$e]['selected'] = "SELECTED";
            } 
            $e++;
        }         
        $data['username'] = $row->userName ;      
        $data['password'] = $row->userPassword;    

        $data['url_post'] = site_url('User/editpost');  
        $data['url_index'] = site_url('User'); 
        $data['id'] = $id; 
        $this->load->view('vm_user/form', $data); 
    }
    /* fungsi edit ini akan mensetting nilai-nilai di form ketika mengklik tombol edit */

    function editgroup($id) {
        $row = $this->table01->getby_idgroup($id)->row();    
        $data['group'] = $row->g_desc ;          

        $data['url_post'] = site_url('User/editpostgroup');  
        $data['url_gridgroup'] = site_url('User/gridgroup');  
        $data['url_deletegroup'] = site_url('User/removegroup');  
        $data['url_index'] = site_url('User'); 
        $data['id'] = $id; 
        $this->load->view('vm_user/formgroup', $data); 
    }

    /* fungsi untuk post data ketika melakukan edit data, fungsi ini akan masuk ke database */

    function editpostgroup() { 
            $userName = $this->session->userdata('ses_userName');
            $id = $this->input->post('id');   
            $group = $this->input->post('group');   
            $datetime=date("Y-m-d h:i:sa");
            $ganti=strtolower($group);
            $groupsmall = str_replace(" ", "_", $ganti);
            $checkdata = $this->table01->cek_namegroup($groupsmall);  
            if($checkdata > 0){
                 $valid =false; 
                 $message = 'Nama Ini sudah digunakan';
            }
            else{ 
                 $valid =true; 
                 $message = 'Data diUpadate';
                    
                    $record = array(
                        "g_name" => $groupsmall,
                        "g_desc" => $group, 
                        "UpdateBy" => $userName,  
                        "UpdateTime" => $datetime 
                    );  
                $this->table01->updategroupuser($id,$record); 
            }    
            $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }
    function editpost() { 
            $userId = $this->session->userdata('ses_user_id');
            $id = $this->input->post('id');  
            $row = $this->table01->getby_id($id)->row();  
            $pass= $row->userPassword;
            $group = $this->input->post('group');  
            $username = $this->input->post('username'); 
            $password = $this->input->post('password'); 
            if($pass==$password){
                $postpass=$password;
            }
            else{
                 $postpass=md5($username . $password);
             }   
            //  var_dump($password);
            //  var_dump($pass);
            //  var_dump($postpass);
            // die();
            $datetime=date("Y-m-d h:i:sa");
            $record = array(
                "userName" => $username,
                "userPassword" => $postpass, 
                "UpdateBy" => $userId,  
                "UpdateTime" => $datetime 
            );  
        $this->table01->updategroup($id,$group,$userId);   
        $this->table01->update($id,$record);   
         $valid = 'true';
         $message = 'data update';
         $jsonmsg = array(
                "msg" => $message,
                "hasil" => $valid, 
            );  
        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }

    /* fungsi untuk delete data */

    public function remove() {
        $id = $this->input->post('id');
        $this->table01->delete($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }
    public function removegroup() {
        $id = $this->input->post('id');
        $this->table01->deletegroup($id);
        /* membuat array, yang akan dikonversi menjadi json untuk kebutuhan ajax */
        $jsonmsg = array(
            "msg" => 'Delete Data Succces',
            "hasil" => true
        );

        /* konversi array json, yang akan terkirim ke form.php */
        echo json_encode($jsonmsg);
    }


}
